<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\City;
use App\Models\Color;
use App\Models\Contacts;
use App\Models\ShopCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;

class SharedComponents
{
    protected const PAGINATION = 9;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        View::share('colors', Color::get()->translate(session('locale')));
        View::share('banks', City::get());
        View::share('contacts', Contacts::get());
        View::share('categories', ShopCategory::with('products')->get()->translate(session('locale')));
        return $next($request);
    }
}
