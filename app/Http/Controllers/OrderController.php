<?php

namespace App\Http\Controllers;

use App\Models\Banks;
use App\Models\City;
use App\Models\Contacts;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Promocode;
use App\Models\ShopCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    public function orderPage(Request $request)
    {
        // $data = json_decode($request->product, true);
        // $cart = array_reduce($data['cart'], function($carry, $product) {
        //     if (array_key_exists('image', $product)) {
        //         $image = json_decode($product['image'], true);
        //         $product['image'] = $image;
        //     }
        //     $carry[] = $product;
        //     return $carry;
        // }, []);
        $params = [
            // 'cartNew'       => $cart,
            // 'promocode'     => $data['promocode'],
            // 'totalQuantity' => $data['totalQuantity'],
            // 'totalSum'      => $data['totalSum'],
            'cities'        => City::with('show')->get(),
        ];
        return view('checkout', $params);
    }

    public function cartClear(Request $request) {
        session()->put('cart', []);
        session()->save();
        return response ([], 200);
    }

    public function orderSubmit(Request $request)
    {
        if ($request->type_of_form == 'fiz') {
            $valData = $request->validate([
                'name' => 'required',
                'flat' => 'required',
                'apartment' => 'required',
                'house' => 'required',
                'phone' => 'required',
                'address' => 'required',
                'country_city' => 'required',
                'email' => 'required',
//            'mail_index'=>'required',
//            'order_number'=>'required'
                'payment_type' => 'required',
                'delivery_type' => 'required',
            ]);

            $basket = session()->get('cart');
            $totalQuantity = 0;
            $totalSum = 0;

            $promocode = null;
            if ($request->has('promocode')) {
                $promocode = Promocode::where('promocode', $request->promocode)->first();
            }
            foreach ($basket as $id => $quantity) {
                $product = \App\Models\Product::with('similar')->find($id);
                $totalQuantity += $quantity;
                if (Auth::check()) {
                    $totalSum += $quantity * $product->partners_price;
                } else {
                    $totalSum += $quantity * $product->price;
                }
            }

//        if ($totalSum < 50000) {
//            return redirect()->back()->withErrors('Заказ оформляется с 50000');
//        }

            $order = Order::create([
                'name' => $request->name,
                'phone' => $request->phone,
                'user_id' => $request->has('user_id') ? $request->user_id : null,
                'email' => $request->has('email') ? $request->email : null,
//            'surname'=> $request->surname,
                'payment_type' => $request->payment_type,
                'delivery_type' => $request->delivery_type,
                'payment_status_id' => 1,
//            'delivery_id' => 1,
//            'delivery_status_id' => 1,
                'order_status_id' => 1,
                'address' => $request->address,
                'country_city' => $request->country_city,
//            'mail_index' => $request->has('mail_index') ? $request->mail_index : null,
                'total_price' => $promocode != null ? $totalSum - (($promocode->product->partners_price * $promocode->percent / 100) * $totalSum) : $totalSum, //вот эту хуйню ебучую закинуть повыше прописать и сделать с ретёрном
                //минимальная стоимость заказа и доставки должна быть регулируемой в админке,
                'total_quantity' => $totalQuantity,
                'house' => $request->house,
                'flat' => $request->flat,
                'apartment' => $request->apartment
            ]);
            $orderDetails = collect([]);
            foreach ($basket as $id => $quantity) {
                $product = \App\Models\Product::find($id);
                $orderDetails->push(OrderDetail::create([
                    'unit_price' => $product->price,
                    'unit_quantity' => $quantity,
                    'order_id' => $order->id,
                    'product_name' => $product->product_name,
                    'product_id' => $product->id,
                    'image' => $product->image
                ]));
            }
        } else {
            $valData = $request->validate([
                'company_name' => 'required',
                'bin' => 'required',
                'bank' => 'required',
                'iic' => 'required',
                'phone' => 'required',
                'address' => 'required',
                'legal_address' => 'required',
                'country_city' => 'required',
                'email' => 'required',
//            'mail_index'=>'required',
//            'order_number'=>'required'
                'payment_type' => 'required',
                'delivery_type' => 'required',
            ]);

            $basket = session()->get('cart');
            $totalQuantity = 0;
            $totalSum = 0;
            $promocode = null;
            if ($request->has('promocode')) {
                $promocode = Promocode::where('promocode', $request->promocode)->first();
            }

            foreach ($basket as $id => $quantity) {
                $product = \App\Models\Product::with('similar')->find($id);
                $totalQuantity += $quantity;
                $totalSum += $quantity * $product->price;
            }
//        if ($totalSum < 50000) {
//            return redirect()->back()->withErrors('Заказ оформляется с 50000');
//        }

            $order = Order::create([
                'company_name' => $request->company_name,
                'phone' => $request->phone,
//                'user_id' => $request->has('user_id') ? $request->user_id : null,
                'email' => $request->has('email') ? $request->email : null,
//            'surname'=> $request->surname,
                'payment_type' => $request->payment_type,
                'delivery_type' => $request->delivery_type,
                'payment_status_id' => 1,
//            'delivery_id' => 1,
//            'delivery_status_id' => 1,
                'order_status_id' => 1,
                'address' => $request->address,
                'country_city' => $request->country_city,
//            'mail_index' => $request->has('mail_index') ? $request->mail_index : null,
                'total_price' => $promocode != null ? $totalSum - (($promocode->percent / 100) * $totalSum) : $totalSum, //вот эту хуйню ебучую закинуть повыше прописать и сделать с ретёрном
                //минимальная стоимость заказа и доставки должна быть регулируемой в админке,
                'total_quantity' => $totalQuantity,
                'bank' => $request->bank,
                'bin' => $request->bin,
                'vat_payer' => $request->vat_payer
            ]);
            $orderDetails = collect([]);
            foreach ($basket as $id => $quantity) {
                $product = \App\Models\Product::find($id);
                $orderDetails->push(OrderDetail::create([
                    'unit_price' => $product->price,
                    'unit_quantity' => $quantity,
                    'order_id' => $order->id,
                    'product_name' => $product->product_name,
                    'product_id' => $product->id,
                    'image' => $product->image
                ]));
            }
        }

        session()->put('cart', []);
        session()->save();
        return redirect('/page');
    }
}

