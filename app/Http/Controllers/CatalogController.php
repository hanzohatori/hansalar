<?php

namespace App\Http\Controllers;

use App\Domains\Contracts\FilterContract;
use App\Models\Banks;
use App\Models\City;
use App\Models\Color;
use App\Models\Contacts;
use App\Models\Filter;
use App\Models\Product;
use App\Models\ProductFilters;
use App\Models\ShopCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use TCG\Voyager\Models\Category;

class CatalogController extends Controller
{
    public function filter(Request $request)
    {
        $products = Product::with('category')->where(function ($query) use ($request) {
            if ($request->has('price')) {
                $query->whereBetween('price', $request->price);
            }
            if ($request->has('category')) {
                $query->whereBetween('category', $request->category);
            }
            if ($request->has('search')) {
                $query->where('name', 'LIKE', '%' . $request->search . '%');
            }
        })->orderBy('id', 'desc')->get();
        return response(['products' => $products], 200);
    }

    public function category(Request $request)
    {
        if ($request->has('category')) {
            $category = \App\Models\ShopCategory::where('parent_id', $request->category)->get();
            return response(['categories' => $category], 200);
        }
    }

    public function mainApiCatalogue(Request $request)
    {
        $list = $request->all();
        $data = (array)json_decode($request->getContent());
        $products = Product::with('productFilters', 'colour')->where(function ($query) use ($request) {
            if ($request->has('category')) {
                $query->where('shop_category_id', $request->category);
            }
        });
        if (isset($data['search'])) {
            $products->where('product_name', 'LIKE', '%' . ($data['search']) . '%');
        }
        if (isset($data['is_new'])) {
            $products = $products->orderBy('created_at', 'desc');
        }
        if (!Auth::check()) {
            if (isset($data['price_asc'])) {
                $products = $products->orderBy('current_price', 'asc');
            }
            if (isset($data['price_desc'])) {
                $products = $products->orderBy('current_price', 'desc');
            }
            if (isset($data['price_from']) && isset($data['price_from']) != '') {
                $products = $products->where('current_price', '<=', (array)$data['price_from']);
            }
            if (isset($data['price_to']) && isset($data['price_to']) != '') {
                $products = $products->where('current_price', '>=', (array)$data['price_to']);
            }
        } else {
            if (isset($data['price_asc'])) {
                $products = $products->orderBy('current_price_for_auth', 'asc');
            }
            if (isset($data['price_desc'])) {
                $products = $products->orderBy('current_price_for_auth', 'desc');
            }
            if (isset($data['price_from']) && isset($data['price_from']) != '') {
                $products = $products->where('current_price_for_auth', '<=', (array)$data['price_from']);
            }
            if (isset($data['price_to']) && isset($data['price_to']) != '') {
                $products = $products->where('current_price_for_auth', '>=', (array)$data['price_to']);
            }
        }

        if (isset($data['is_popular'])) {
            $products = $products->where('is_popular', 1);
        }
        if (isset($data["filter"])) {
            foreach ($data["filter"] as $idFilter => $filter) {
                $products->whereHas('productFilters', function ($query) use ($filter) {
                    if (!is_array($filter)) {
                        $filter = [$filter];
                    }
                    $query->wherein('title', $filter);
                });
            }
        }

        $products = $products->paginate(9);

        foreach ($products as $key => $product) {
            $products[$key]['image'] = json_decode($products[$key]['image']);
        }

        if(!Auth::check()){
            $max = Product::orderBy('current_price', 'desc')->first()->current_price;
            $min = Product::orderBy('current_price', 'asc')->first()->current_price;
        }else{
            $max = Product::orderBy('current_price_for_auth', 'desc')->first()->current_price_for_auth;
            $min = Product::orderBy('current_price_for_auth', 'asc')->first()->current_price_for_auth;
        }

        return response(['products' => $products, 'min' => $min, 'max' => $max]);
    }

    public function catalogue(Request $request)
    {
        $products = Product::with('productFilters')->paginate(9);
        $page = $request->has('page') ? $request->page : 1;
        $cart = session()->get('cart');
        $params = [];
        $totalSum = 0;
        $totalQuantity = 0;


        $_filters = [];
        $list = $request->all();
        $products = Product::with('productFilters')->where(function ($query) use ($request) {
        });

        if (isset($list["filter"])) {
            foreach ($list["filter"] as $idFilter => $filter) {
                $products->whereHas('productFilters', function ($query) use ($filter) {
                    if (!is_array($filter)) {
                        $filter = [$filter];
                    }
                    $query->wherein('title', $filter);
                });
            }
        }
        $products = $products->paginate(9);
        $_productsAll = Product::with('productFilters')->get();
        $_products = $_productsAll->toArray();

        $filter = [];

        foreach ($_products as &$product) {
            foreach ($product['product_filters'] as &$filters) {

                $filterTitle = Filter::where(FilterContract::ID, $filters['filter_id'])->first();
                if (!empty($filterTitle)) {
                    $filterTitle = $filterTitle->toArray();
                    if (!array_key_exists($filterTitle[FilterContract::TITLE], $filter)) {
                        $filter[$filterTitle[FilterContract::TITLE]] = [];
                    }
                    $filter[$filterTitle[FilterContract::TITLE]][] = [$filterTitle['title'], $filters['title']];
                    $filter[$filterTitle[FilterContract::TITLE]] = (array_unique($filter[$filterTitle[FilterContract::TITLE]], SORT_REGULAR));
                };
            }
        }
        $choosed_values = [];
        if ($request->has('filter'))
            $choosed_values = $request->filter;

        if(!Auth::check()){
            $max = Product::orderBy('current_price', 'desc')->first()->current_price;
            $min = Product::orderBy('current_price', 'asc')->first()->current_price;
        }else{
            $max = Product::orderBy('current_price_for_auth', 'desc')->first()->current_price_for_auth;
            $min = Product::orderBy('current_price_for_auth', 'asc')->first()->current_price_for_auth;
        }

        if (!$cart) {
            return view('catalog',
                compact(
                    'cart',
                    'products', 'page', 'products', 'params',
                    'filter', 'choosed_values','max','min'
                )
            );
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                if (Auth::check()) {
                    $totalSum += $quantity * $cartProduct->partners_price;
                } elseif (!Auth::check() && $cartProduct->discount_price != null) {
                    $totalSum += $quantity * $cartProduct->discount_price;
                } else {
                    $totalSum += $quantity * $cartProduct->price;
                }
                $totalQuantity += $cartProduct->quantity;
            }
        }
        return view('catalog',
            compact(
                'cartNew', 'cart', 'totalSum',
                'totalQuantity',
                'products',
                'page', 'params', 'filter',
                'choosed_values',
                'max', 'min'
            )
        );
    }

    public function shopCategory($id, Request $request)
    {
        $currentUrl = $request->url();
        $_filters = [];
        $category_name = ShopCategory::findOrFail($id)->title;
        $list = $request->all();

        $products = Product::with('productFilters')->where('shop_category_id', $id)->where(function ($query) use ($request, $id) {

        });
        if (isset($list["filter"])) {
            foreach ($list["filter"] as $idFilter => $filter) {
                $products->whereHas('productFilters', function ($query) use ($filter) {
                    if (!is_array($filter)) {
                        $filter = [$filter];
                    }
                    $query->wherein('title', $filter);
                });
            }
        }
        $products = $products->paginate(9);
        $_productsAll = Product::with('productFilters')->where('shop_category_id', $id)->get();
        $_products = $_productsAll->toArray();

        $filter = [];

        foreach ($_products as $product) {
            foreach ($product['product_filters'] as $filters) {
                $filterTitle = Filter::where(FilterContract::ID, $filters['filter_id'])->first();
                if (!empty($filterTitle)) {
                    $filterTitle = $filterTitle->toArray();
                    if (!array_key_exists($filterTitle[FilterContract::TITLE], $filter)) {
                        $filter[$filterTitle[FilterContract::TITLE]] = [];
                    }
                    $filter[$filterTitle[FilterContract::TITLE]][] = [$filterTitle['title'], $filters['title']];
                    $filter[$filterTitle[FilterContract::TITLE]] = (array_unique($filter[$filterTitle[FilterContract::TITLE]], SORT_REGULAR));
                };
            }
        }
        $choosed_values = [];
        if ($request->has('filter'))
            $choosed_values = $request->filter;


        if(!Auth::check()){
            $max = Product::where('shop_category_id', $id)->orderBy('current_price', 'desc')->first()->current_price;
            $min = Product::where('shop_category_id', $id)->orderBy('current_price', 'asc')->first()->current_price;
        }else{
            $max = Product::where('shop_category_id', $id)->orderBy('current_price_for_auth', 'desc')->first()->current_price_for_auth;
            $min = Product::where('shop_category_id', $id)->orderBy('current_price_for_auth', 'asc')->first()->current_price_for_auth;
        }

        $cart = session()->get('cart');
        $params = [];
        $totalSum = 0;
        $totalQuantity = 0;
        $page = $request->has('page') ? $request->page : 1;
        if (!$cart) {
            return view('catalog',
                compact(
                    'cart', 'id',
                    'products', 'page', 'products',
                    'params', 'currentUrl',
                    'filter', 'choosed_values', 'max', 'min', 'category_name'
                )
            );
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                if (Auth::check()) {
                    $totalSum += $quantity * $cartProduct->partners_price;
                } elseif (!Auth::check() && $cartProduct->discount_price != null) {
                    $totalSum += $quantity * $cartProduct->discount_price;
                } else {
                    $totalSum += $quantity * $cartProduct->price;
                }
                $totalQuantity += $cartProduct->quantity;
            }
        }
        return view('catalog',
            compact(
                'cartNew', 'cart', 'totalSum',
                'totalQuantity',
                'products', 'page',
                'id', 'params', 'currentUrl',
                'filter', 'choosed_values', 'totalQuantity',
                'min', 'max', 'category_name'
            )
        );
    }
}
//select * from `products` where (`shop_category_id` = ?) and exists (select * from `product_filters` where `products`.`id` = `product_filters`.`product_id` and `title` in (?, ?)) and `products`.`deleted_at` is null
//select * from `products` where (`shop_category_id` = ?) and `current_price_for_auth` <= ? and `current_price_for_auth` >= ? and `products`.`deleted_at` is null
