<?php

namespace App\Http\Controllers;

use App\Domains\Contracts\FilterContract;
use App\Models\Product;
use App\Models\ProductFilters;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PaymentcardController extends Controller {
    
    public function showPage (Request $request) {
        $products = Product::with('productFilters')->paginate(9);
        $page = $request->has('page') ? $request->page : 1;
        $cart = session()->get('cart');
        $params = [];
        $totalSum = 0;
        $totalQuantity = 0;

        return view('paymentcard', compact(
            'cart', 'totalSum',
            'totalQuantity',
            'products',
            'page', 'params',
        ));
    }
}