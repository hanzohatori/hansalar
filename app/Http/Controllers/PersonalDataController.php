<?php

namespace App\Http\Controllers;

use App\Models\Banks;
use App\Models\City;
use App\Models\Contacts;
use App\Models\Product;
use App\Models\Questions;
use App\Models\ShopCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use TCG\Voyager\Models\User;

class PersonalDataController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $bankso = Banks::get();
        $products = Product::get();
        $products = $products->translate(session('locale'));
        $totalSum = 0;
        $totalQuantity = 0;
        $cart = session()->get('cart');
        if (!$cart) {
            return view('personal-data', compact('cart', 'products', 'user', 'bankso'));
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                $totalSum += $quantity * $cartProduct->partners_price;
                $totalQuantity += $cartProduct->quantity;
            }
        }

        return view('personal-data', compact('cart', 'products', 'user', 'cartNew', 'totalSum', 'bankso', 'totalQuantity'));
    }

    public function updateUser(Request $request)
    {
        $bankso = Banks::get();
        $user = Auth::user();
        if ($request->has('name') && $request->name != '') {
            $user['name'] = $request->name;
        }

        if ($request->has('surname') && $request->surname != '') {
            $user->surname = $request->surname;
        }

        if ($request->has('phone') && $request->phone != '') {
            $user['phone'] = $request->phone;
        }

        if ($request->has('legal_address') && $request->legal_address != '') {
            $user['legal_address'] = $request->legal_address;
        }

        if ($request->has('bank') && $request->bank != '') {
            $user['bank'] = $request->bank;
        }

        if ($request->has('iic') && $request->iic != '') {
            $user['iic'] = $request->iic;
        }

        if ($request->has('bin') && $request->bin != '') {
            $user['bin'] = $request->bin;
        }

        if ($request->has('legal_entity') && $request->legal_entity != '') {
            $user['legal_entity'] = $request->legal_entity;
        }

        if ($request->has('is_female')) {
            $user['is_male'] = 0;
//            $user->avatar = setting('site.female');
        }

        if ($request->has('email') && $request->email != '') {
            $user['email'] = $request->email;
        }

        session()->put('user', $user);
        session()->save();
        $user->save();

        return back();
    }

    public function send(Request $request)
    {
        $question = Questions::create([
            'user_id' => $request->has('user_id') ? $request->user_id : null,
            'question' => $request->question
        ]);
        return redirect()->back()->with('message', 'Ваш вопрос был отправлен Администратору');
    }
}
