<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Banks;
use App\Models\Color;
use App\Models\Filter;
use App\Models\Product;
use App\Models\Contacts;
use App\Models\ShopCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Domains\Contracts\FilterContract;

class OutletController extends Controller
{
    public function index(Request $request)
    {
        $products = Product::with('productFilters')->paginate(9);
        $page = $request->has('page') ? $request->page : 1;
        $cart = session()->get('cart');
        $params = [];
        $totalSum = 0;
        $totalQuantity = 0;


        $_filters = [];
        $list = $request->all();

        $products = Product::with('productFilters')->where(function ($query) use ($request) {
            if ($request->has('color')) {
                $query->whereIn('color_id', $request->color);
            }
//            if (isset($products->discount_price)) {
//                if ($request->has('price_from') && $request->price_from != '') {
//                    $query->where('discount_price', '<=', $request->price_from);
//                }
//
//                if ($request->has('price_to') && $request->price_to != '') {
//                    $query->where('discount_price', '>=', $request->price_to);
//                }
//            }else{
            if ($request->has('price_from') && $request->price_from != '') {
                $query->where('price', '<=', $request->price_from);
            }

            if ($request->has('price_to') && $request->price_to != '') {
                $query->where('price', '>=', $request->price_to);
            }
//            }

            if ($request->has('sort_type') && $request->has('sort_value')) {
                $query->orderBy($request->sort_type, $request->sort_value);
            }
            if ($request->has('price_asc')) {
                $query->orderBy('price', 'asc');
            }

            if ($request->has('price_desc')) {
                $query->orderBy('price', 'desc');
            }
            if ($request->has('is_new')) {
                $query->orderBy('created_at', 'desc');
            }

            if($request->has('is_popular')){
                $query->where('is_popular',1);
            }
        });
        if ($request->has('is_new')) {
            $products = $products->orderBy('created_at', 'desc');
        }
        if ($request->has('price_desc')) {

            $products = $products->orderBy('price', 'desc');
        }

        if ($request->has('price_asc')) {

            $products = $products->orderBy('price', 'asc');
        }
        if ($request->has('is_popular')) {
            $products = $products->where('is_popular', 1);
        }
        if (isset($list["filter"])) {
            foreach ($list["filter"] as $idFilter => $filter) {
                $products->whereHas('productFilters', function ($query) use ($filter) {
                    if (!is_array($filter)) {
                        $filter = [$filter];
                    }
                    $query->wherein('title', $filter);
                });
            }
        }
        $products = $products->where('loan', 1)->paginate(9);
        $_productsAll = Product::with('productFilters')->where('loan', 1)->get();
        $_products = $_productsAll->toArray();

        $filter = [];

        foreach ($_products as &$product) {
            foreach ($product['product_filters'] as &$filters) {

                $filterTitle = Filter::where(FilterContract::ID, $filters['filter_id'])->first();
                if (!empty($filterTitle)) {
                    $filterTitle = $filterTitle->toArray();
                    if (!array_key_exists($filterTitle[FilterContract::TITLE], $filter)) {
                        $filter[$filterTitle[FilterContract::TITLE]] = [];
                    }
                    $filter[$filterTitle[FilterContract::TITLE]][] = [$filterTitle['title'], $filters['title']];
                    $filter[$filterTitle[FilterContract::TITLE]] = (array_unique($filter[$filterTitle[FilterContract::TITLE]], SORT_REGULAR));
                };
            }
        }
        $choosed_values = [];
        if ($request->has('filter'))
            $choosed_values = $request->filter;

        if(!Auth::check()){
            $max = Product::orderBy('current_price', 'desc')->first()->current_price;
            $min = Product::orderBy('current_price', 'asc')->first()->current_price;
        }else{
            $max = Product::orderBy('current_price_for_auth', 'desc')->first()->current_price_for_auth;
            $min = Product::orderBy('current_price_for_auth', 'asc')->first()->current_price_for_auth;
        }

        if (!$cart) {
            return view('outlet',
                compact(
                    'cart',
                    'products','page', 'products',
                    'params', 'filter',
                    'choosed_values', 'max', 'min'
                )
            );
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                if(Auth::check()){
                    $totalSum += $quantity * $cartProduct->partners_price;
                }elseif(!Auth::check() && $cartProduct->discount_price != null){
                    $totalSum += $quantity * $cartProduct->discount_price;
                }else{
                    $totalSum += $quantity * $cartProduct->price;
                }
                $totalQuantity += $cartProduct->quantity;
            }
        }
        return view('outlet',
            compact(
                'cartNew', 'cart', 'totalSum',
                'totalQuantity', 'products',
                'page', 'params',
                'filter', 'choosed_values', 'max', 'min'
            )
        );
    }

    public function outletCategory($id, Request $request)
    {
        $currentUrl = $request->url();
        $category_name = ShopCategory::findOrFail($id)->title;
        $_filters = [];
        $list = $request->all();

        $products = Product::with('productFilters')->where('shop_category_id',$id)->where(function ($query) use ($request, $id) {
            if ($request->has('color')) {
                $query->whereIn('color_id', $request->color);
            }
//            if (isset($products->discount_price)) {
//                if ($request->has('price_from') && $request->price_from != '') {
//                    $query->where('discount_price', '<=', $request->price_from);
//                }
//
//                if ($request->has('price_to') && $request->price_to != '') {
//                    $query->where('discount_price', '>=', $request->price_to);
//                }
//            }else{
            if ($request->has('price_from') && $request->price_from != '') {
                $query->where('price', '<=', $request->price_from);
            }

            if ($request->has('price_to') && $request->price_to != '') {
                $query->where('price', '>=', $request->price_to);
            }
//            }

            if ($request->has('sort_type') && $request->has('sort_value')) {
                $query->orderBy($request->sort_type, $request->sort_value);
            }
            if ($request->has('price_asc')) {
                $query->orderBy('price', 'asc');
            }

            if ($request->has('price_desc')) {
                $query->orderBy('price', 'desc');
            }
        });
        if ($request->has('new')) {
            $products = $products->orderBy('created_at', 'desc');
        }
        if ($request->has('price_desc')) {

            $products = $products->orderBy('price', 'desc');
        }

        if ($request->has('price_asc')) {

            $products = $products->orderBy('price', 'asc');
        }

        if (isset($list["filter"])) {
            foreach ($list["filter"] as $idFilter => $filter) {
                $products->whereHas('productFilters', function ($query) use ($filter) {
                    if (!is_array($filter)) {
                        $filter = [$filter];
                    }
                    $query->wherein('title', $filter);
                });
            }
        }
        $products = $products->where('loan', 1)->paginate(9);
        $_productsAll = Product::with('productFilters')->where('loan', 1)->where('shop_category_id', $id)->get();
        $_products = $_productsAll->toArray();

        $filter = [];

        foreach ($_products as &$product) {
            foreach ($product['product_filters'] as &$filters) {
                $filterTitle = Filter::where(FilterContract::ID, $filters['filter_id'])->first();
                if (!empty($filterTitle)) {
                    $filterTitle = $filterTitle->toArray();
                    if (!array_key_exists($filterTitle[FilterContract::TITLE], $filter)) {
                        $filter[$filterTitle[FilterContract::TITLE]] = [];
                    }
                    $filter[$filterTitle[FilterContract::TITLE]][] = [$filterTitle['title'], $filters['title']];
                    $filter[$filterTitle[FilterContract::TITLE]] = (array_unique($filter[$filterTitle[FilterContract::TITLE]], SORT_REGULAR));
                };
            }
        }


//        foreach ($filter as $k=>$item) {
//            $filter[$k] = array_unique($item,SORT_STRING );
//        }

        $cart = session()->get('cart');
        $params = [];
        $totalSum = 0;
        $totalQuantity = 0;
        $page = $request->has('page') ? $request->page : 1;

        $choosed_values = [];
        if ($request->has('filter'))
            $choosed_values = $request->filter;

        if(!Auth::check()){
            $max = Product::where('shop_category_id', $id)->orderBy('current_price', 'desc')->first()->current_price;
            $min = Product::where('shop_category_id', $id)->orderBy('current_price', 'asc')->first()->current_price;
        }else{
            $max = Product::where('shop_category_id', $id)->orderBy('current_price_for_auth', 'desc')->first()->current_price_for_auth;
            $min = Product::where('shop_category_id', $id)->orderBy('current_price_for_auth', 'asc')->first()->current_price_for_auth;
        }

        if (!$cart) {
            // check if color has products, if it has return color
            return view('outlet',
                compact(
                    'cart', 'id', 'page', 'products',
                    'params', 'currentUrl',
                    'filter', 'choosed_values', 'min', 'max', 'category_name'
                )
            );
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                if(Auth::check()){
                    $totalSum += $quantity * $cartProduct->partners_price;
                }elseif(!Auth::check() && $cartProduct->discount_price != null){
                    $totalSum += $quantity * $cartProduct->discount_price;
                }else{
                    $totalSum += $quantity * $cartProduct->price;
                }
                $totalQuantity += $cartProduct->quantity;
            }
        }
        // $filter['Цвет дш'] = collect(collect($filter)['Цвет дш'])
        //     ->pluck(1)
        //     ->map(fn($color_title) => DB::table('colors')->where('color', '=', $color_title)->first())
        //     ->pluck('id')
        //     ->filter(fn($color_id) => DB::table('products')->where('color_id', $color_id)->count() > 0)
        //     ->map(fn($color_id) => DB::table('colors')->where('id', '=', $color_id)->first())
        //     ->pluck('color')
        //     ->map(fn($color_title) => ['Цвет дш', $color_title])
        //     ->toArray();
        return view('outlet',
            compact(
                'cartNew', 'cart', 'totalSum',
                'totalQuantity',
                'products', 'page',
                'id', 'params', 'currentUrl',
                'filter', 'choosed_values', 'totalQuantity',
                'max', 'min', 'category_name'
            )
        );
    }

    public function mainApiCatalogue(Request $request)
    {

        $list = $request->all();
        $data = (array)json_decode($request->getContent());
        $products = Product::with('productFilters', 'colour')->where(function ($query) use ($request) {
            if ($request->has('category')) {
                $query->where('shop_category_id', $request->category);
            }
        });


        if (isset($data['search'])) {
            $products->where('product_name', 'LIKE', '%' . ($data['search']) . '%');
        }
        if (isset($data['is_new'])) {
            $products = $products->orderBy('created_at', 'desc');
        }
        if (!Auth::check()) {
            if (isset($data['price_asc'])) {
                $products = $products->orderBy('current_price', 'asc');
            }
            if (isset($data['price_desc'])) {
                $products = $products->orderBy('current_price', 'desc');
            }
            if (isset($data['price_from']) && isset($data['price_form']) != '') {
                $products = $products->where('current_price', '<=', (array) $data['price_from']);
            }
            if (isset($data['price_to']) && isset($data['price_to']) != '') {
                $products = $products->where('current_price', '>=', (array) $data['price_to']);
            }
        } else {
            if (isset($data['price_asc'])) {
                $products = $products->orderBy('current_price_for_auth', 'asc');
            }
            if (isset($data['price_desc'])) {
                $products = $products->orderBy('current_price_for_auth', 'desc');
            }
            if (isset($data['price_from']) && isset($data['price_form']) != '') {
                $products = $products->where('current_price_for_auth', '<=', (array) $data['price_from']);
            }
            if (isset($data['price_to']) && isset($data['price_to']) != '') {
                $products = $products->where('current_price_for_auth', '>=', (array) $data['price_to']);
            }
        }

        if (isset($data['is_popular'])) {
            $products = $products->where('is_popular', 1);
        }
        if (isset($list["filter"])) {
            foreach ($list["filter"] as $idFilter => $filter) {
                $products->whereHas('productFilters', function ($query) use ($filter) {
                    if (!is_array($filter)) {
                        $filter = [$filter];
                    }
                    $query->wherein('title', $filter);
                });
            }
        }


        $products = $products->where(function($query) {
            $query->whereNotNull('discount_price');
            $query->orWhere('loan', 1);
        });
        $products = $products->paginate(9);


        foreach ($products as $key => $product) {
            $products[$key]['image'] = json_decode($products[$key]['image']);
        }

        if(!Auth::check()){

            $max = Product::where(function($query) {
                $query->whereNotNull('discount_price');
                $query->orWhere('loan', 1);
            });

            $max = Product::orderBy('current_price', 'desc')->first()->current_price;
            $min = Product::orderBy('current_price', 'asc')->first()->current_price;
        }else{
            $max = Product::orderBy('current_price_for_auth', 'desc')->first()->current_price_for_auth;
            $min = Product::orderBy('current_price_for_auth', 'asc')->first()->current_price_for_auth;
        }


        return response(['products' => $products, 'min' => $min, 'max' => $max]);
    }
}
