<?php

namespace App\Http\Controllers;

use App\Models\Banks;
use App\Models\City;
use App\Models\Contacts;
use App\Models\Product;
use App\Models\ShopCategory;
use App\Models\Order;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class OrderHistoryController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $products = Product::get();
        $products = $products->translate(session('locale'));
        $totalQuantity = 0;
        $totalSum = 0;
        $cart = session()->get('cart');
        $order = Order::with('order_details')->where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();

        if (!$cart) {
            return view('order-history', compact('cart', 'products', 'user', 'order'));
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                $totalSum += $quantity * $cartProduct->partners_price;
                $totalQuantity += $cartProduct->quantity;
            }
        }

        return view('order-history', compact('cart', 'user', 'products', 'cartNew', 'totalSum', 'order', 'totalQuantity'));
    }
}
