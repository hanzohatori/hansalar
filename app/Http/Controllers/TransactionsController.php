<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Contacts;
use App\Models\Order;
use App\Models\Product;
use App\Models\ShopCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TransactionsController extends Controller
{
    public function index()
    {
        $user = Auth::user();
        $products = Product::get();
        $products = $products->translate(session('locale'));
        $totalSum = 0;
        $totalQuantity = 0;
        $order = Order::with('order_details')->where('user_id', Auth::user()->id)->orderBy('created_at', 'desc')->get();
        $cart = session()->get('cart');
        if (!$cart) {
            return view('transactions', compact('cart', 'products', 'user', 'order'));
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                $totalSum += $quantity * $cartProduct->partners_price;
                $totalQuantity += $cartProduct->quantity;
            }
        }
        return view('transactions', compact('cart', 'products', 'user', 'cartNew', 'totalSum', 'order', 'totalQuantity'));
    }
}
