<?php

namespace App\Http\Controllers;

use App\Http\Requests\PartnerRequest;
use App\Models\Banks;
use App\Models\City;
use App\Models\Partners\PartnerDescription;
use App\Models\Partners\Advantage;
use App\Models\Contacts;
use App\Models\Partners\Cursive;
use App\Models\Partner;
use App\Models\Product;
use App\Models\ShopCategory;

use App\Models\Partners\Steps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PartnerController extends Controller
{
    public function partner()
    {
        $products = Product::get();
        $products = $products->translate(session('locale'));
        $advantages = Advantage::get();
        $advantages = $advantages->translate(session('locale'));
        $description = PartnerDescription::first();
        $description = $description->translate(session('locale'));
        $cursives = Cursive::first();
        $cursives = $cursives->translate(session('locale'));
        $steps = Steps::get();
        $totalSum = 0;
        $totalQuantity = 0;
        $cart = session()->get('cart');
        if (!$cart) {
            return view('partners',
                compact('cart',
                    'products',
                    'advantages', 'description',
                    'steps', 'cursives'
                )
            );
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                if (Auth::check()) {
                    $totalSum += $quantity * $cartProduct->partners_price;
                } elseif (!Auth::check() && $cartProduct->discount_price != null) {
                    $totalSum += $quantity * $cartProduct->discount_price;
                } else {
                    $totalSum += $quantity * $cartProduct->price;
                }
                $totalQuantity += $cartProduct->quantity;
            }
        }
        return view('partners',
            compact('cart', 'products', 'cartNew',
                'totalSum',
                'advantages', 'description',
                'steps', 'cursives','totalQuantity'
            )
        );
    }

    public function register(Partner $partner, PartnerRequest $request)
    {
        $partner->create($request->all());
        return redirect()->back()->with('message', 'Ваша заявка принята в обработку');
    }
}
