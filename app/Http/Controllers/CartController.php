<?php

namespace App\Http\Controllers;

use App\Models\Banks;
use App\Models\City;
use App\Models\Contacts;
use App\Models\Product;
use App\Models\Promocode;
use App\Models\ShopCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use TCG\Voyager\Models\Category;

class CartController extends Controller
{
    public function cart()
    {
        return view('basket');
    }

    public function viewCart(Product $product,Request $request)
    {
        $promocode = null;
        if ($request->has('promocode')){
            $promocode = Promocode::where('promocode',$request->promocode)->first();
            session()->put('promocode',$promocode);
            session()->save();
        }
        $cart = session()->get('cart');
        $totalSum = 0;
        $totalQuantity = 0;
        if (!$cart) {
            return view('basket',
                compact('cart', 'product', 'totalSum',
                    'totalQuantity',
                    'promocode'
                )
            );
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::with('colour')->find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                if(Auth::check()){
                    $totalSum += $quantity * $cartProduct->partners_price;
                }elseif(!Auth::check() && $cartProduct->discount_price != null){
                    $totalSum += $quantity * $cartProduct->discount_price;
                }else{
                    $totalSum += $quantity * $cartProduct->price;
                }
                $totalQuantity += $cartProduct->quantity;
            }
        }
        return view('basket',
            compact('cartNew',
                'cart', 'totalSum',
                'totalQuantity',
                'promocode',
            )
        );
    }

    public function addCart(Request $request, Product $product)
    {
        $cart = session()->get('cart');
        if (isset($cart[$product->id])) {
            $cart[$product->id] = $cart[$product->id] + 1;
        } else {
            $cart[$product->id] = 1;
        }
        session()->forget('cart');
        session()->put('cart', $cart);
        $totalSum = 0;
        $totalQuantity = 0;
        $cartNew = [];
        foreach ($cart as $productId => $quantity) {
            $cartProduct = Product::with('colour')->find($productId);
            $cartProduct->quantity = $quantity;
            $cartNew[] = $cartProduct;
            if(Auth::check()){
                $totalSum += $quantity * $cartProduct->partners_price;
            }elseif(!Auth::check() && $cartProduct->discount_price != null){
                $totalSum += $quantity * $cartProduct->discount_price;
            }else{
                $totalSum += $quantity * $cartProduct->price;
            }
            $totalQuantity += $cartProduct->quantity;
        }

        return response(['message' => 'Товар добавлен в корзину', 'cart' => $cartNew, 'totalQuantity' => $totalQuantity, 'totalSum' => $totalSum], 200);
    }

    public function addManyCart(Request $request, Product $product)
    {
        /*dump(session()->all());
         dump(session()->get('cart'));
         $cart = session()->get('cart');
         if (isset($cart[$product->id])) {
             $cart[$product->id] = $cart[$product->id] + $count;
         } else {
             $cart[$product->id] = $count;
         }

         session()->forget('cart');
         session()->put('cart', $cart);
         dump(session()->get('cart'));
         dump(session()->all());
         $totalSum = 0;
         $totalQuantity = 0;
         $cartNew = [];
         foreach ($cart as $productId => $quantity) {
             $cartProduct = Product::with('colour')->find($productId);
             $cartProduct->quantity = $quantity;
             $cartNew[] = $cartProduct;
             if(Auth::check()) {
                 $totalSum += $quantity * $cartProduct->partners_price;
             }elseif(!Auth::check() && $cartProduct->discount_price != null){
                 $totalSum += $quantity * $cartProduct->discount_price;
             }else{
                 $totalSum += $quantity * $cartProduct->price;
             }
             $totalQuantity += $cartProduct->quantity;
         } */
         $product = Product::with('colour')->findOrFail($product->id)->toJson();
        return response(['current_product' => $product], 200);
    }

    public function removeCart(Product $product)
    {
        $cart = session()->get('cart');
        if (isset($cart[$product->id])) {
            if ($cart[$product->id] > 1) {
                $cart[$product->id] = $cart[$product->id] - 1;
            } else {
                unset($cart[$product->id]);
            }
        }
        session()->forget('cart');
        session()->put('cart', $cart);
        $totalSum = 0;
        $totalQuantity = 0;
        $cartNew = [];
        foreach ($cart as $productId => $quantity) {
            $cartProduct = Product::with('colour')->find($productId);
            $cartProduct->quantity = $quantity;
            $cartNew[] = $cartProduct;
            if(Auth::check()){
                $totalSum += $quantity * $cartProduct->partners_price;
            }elseif(!Auth::check() && $cartProduct->discount_price != null){
                $totalSum += $quantity * $cartProduct->discount_price;
            }else{
                $totalSum += $quantity * $cartProduct->price;
            }
            $totalQuantity += $cartProduct->quantity;
        }
        return response(['message' => 'Убран 1 товар из корзины', 'cart' => $cartNew, 'totalQuantity' => $totalQuantity, 'totalSum' => $totalSum], 200);
    }

    public function deleteCart(Product $product)
    {
        $cart = session()->get('cart');
        unset($cart[$product->id]);
        session()->forget('cart');
        session()->put('cart', $cart);
        $totalSum = 0;
        $totalQuantity = 0;
        $cartNew = [];
        foreach ($cart as $productId => $quantity) {
            $cartProduct = Product::with('colour')->find($productId);
            $cartProduct->quantity = $quantity;
            $cartNew[] = $cartProduct;
            if(Auth::check()){
                $totalSum += $quantity * $cartProduct->partners_price;
            }elseif(!Auth::check() && $cartProduct->discount_price != null){
                $totalSum += $quantity * $cartProduct->discount_price;
            }else{
                $totalSum += $quantity * $cartProduct->price;
            }
            $totalQuantity += $cartProduct->quantity;
        }
        return response(['message' => 'Товар удален из корзины', 'cart' => $cartNew, 'totalQuantity' => $totalQuantity, 'totalSum' => $totalSum], 200);
    }

    public function getCart(Request $request) {
        $cart = session()->get('cart');
        $promocode = null;
        if ($request->has('promocode')){
            $promocode = Promocode::where('promocode',$request->promocode)->first();
            session()->put('promocode',$promocode);
            session()->save();
        }
        $totalSum = 0;
        $totalQuantity = 0;
        $cartNew = [];
        foreach ($cart as $productId => $quantity) {
            $cartProduct = Product::with('colour')->find($productId);
            $cartProduct->quantity = $quantity;
            $cartNew[] = $cartProduct;
            if(Auth::check()){
                $totalSum += $quantity * $cartProduct->partners_price;
            }elseif(!Auth::check() && $cartProduct->discount_price != null){
                $totalSum += $quantity * $cartProduct->discount_price;
            }else{
                $totalSum += $quantity * $cartProduct->price;
            }
            $totalQuantity += $cartProduct->quantity;
        }
        return response(['cart' => $cartNew, 'totalQuantity' => $totalQuantity, 'totalSum' => $totalSum, 'promocode' => $promocode], 200);
    }
}
