<?php

namespace App\Http\Controllers;

use App\Models\Banks;
use App\Models\City;
use App\Models\Contacts;
use App\Models\Product;
use App\Models\ShopCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function index()
    {
        $products = Product::get();
        $products = $products->translate(session('locale'));
        $totalSum = 0;
        $totalQuantity = 0;
        $cart = session()->get('cart');
        if (!$cart) {
            return view('change-password', compact('cart', 'products'));
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                if (Auth::check()) {
                    $totalSum += $quantity * $cartProduct->partners_price;
                } else {
                    $totalSum += $quantity * $cartProduct->price;
                }
                $totalQuantity += $cartProduct->quantity;
            }
        }
        
        return view('change-password', compact('cart', 'products', 'cartNew', 'totalSum', 'totalQuantity'));
    }

    public function reset(Request $request)
    {
        $valData = $request->validate([
            'old_password' => 'required',
            'password' => 'required|confirmed',
        ]);
        if (Hash::check($valData['old_password'], Auth::user()->password)) {
            $request->user()->update([
                'password' => Hash::make($valData['password'])
            ]);
        }
        return redirect()->back();
    }
}
