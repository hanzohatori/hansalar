<?php

namespace App\Http\Controllers;

use App\Models\Order;
use App\Models\Product;
use App\Models\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    public function AddWishlist($id,Request $request){
        $wishlist = Wishlist::where('user_id',$request->user_id)->where('product_id',$id)->first();
        $product = Product::find($id);
        if (!$product){
            return redirect()->back()->with('message','Данного продукта нет в наличии');
        }
        if(!$wishlist){
            Wishlist::create([
                'user_id' => Auth::user()->id,
                'product_id' => $id
            ]);
        }
        return redirect()->back()->with('message','Продукт добавлен в Избранные!');
    }


    public function cabinet(){
        $wishlist = Wishlist::with('product')->where('user_id',Auth::user()->id)->get();
        $order = Order::with('order_details')->where('user_id', Auth::user()->id)->orderBy('created_at','desc')->get();
        if(is_null($wishlist)){
            return response('В Избранных отсутствуют продукты', 404);
        }else{
        }
        $user = Auth::user();
        return view('account',compact('wishlist', 'order','user'));
    }

    public function RemoveWishlist($id, Request $request){
        $wishlist = Wishlist::with('product')->where('user_id',Auth::user()->id)->find($id);
        if ($wishlist){
            $wishlist->delete();
        }
        return redirect()->back();
    }
}
