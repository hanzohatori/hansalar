<?php

namespace App\Http\Controllers;

use App\Models\Contacts;
use App\Models\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class AdminController extends Controller
{
    public function contacts()
    {
        $contacts = Contacts::get();
        return view('layouts.footer', compact('contacts'));
    }

    public function locale($locale)
    {
        session(['locale'=>$locale]);
        App::setLocale($locale);
        $currentLocale = App::getLocale();
        return redirect()
            ->back(); // GET
    }

    public function city(Request $request, City $city)
    {
        session()->put('city', $city->city);
        return redirect()
            ->back(); // GET
    }
}
