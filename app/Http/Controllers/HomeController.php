<?php

namespace App\Http\Controllers;

use App\Models\Banks;
use App\Models\City;
use App\Models\Contacts;
use App\Models\Product;
use App\Models\ShopCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $bankso = Banks::get();
        $products = Product::get();
        $products = $products->translate(session('locale'));
        $totalSum = 0;
        $totalQuantity = 0;
        $cart = session()->get('cart');
        if (!$cart) {
            return view('personal-data',
                compact(
                    'cart',
                    'products', 'user', 'bankso'
                )
            );
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                $totalSum += $quantity * $cartProduct->price;
                if(Auth::check()){
                    $totalSum += $quantity * $cartProduct->partners_price;
                }elseif(!Auth::check() && $cartProduct->discount_price != null){
                    $totalSum += $quantity * $cartProduct->discount_price;
                }else{
                    $totalSum += $quantity * $cartProduct->price;
                }
                $totalQuantity += $cartProduct->quantity;
            }
        }
        return view('personal-data',
            compact(
                'cart', 'user', 'products',
                'cartNew', 'totalSum',
                'bankso', 'totalQuantity'
            )
        );
    }
}
