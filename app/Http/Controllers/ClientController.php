<?php

namespace App\Http\Controllers;

use App\Domains\Contracts\FilterContract;
use App\Models\Banks;
use App\Models\City;
use App\Models\Clients\BankCards;
use App\Models\Clients\ClientPaymentDesc;
use App\Models\Clients\ClientsCursive;
use App\Models\Clients\ClientsDelivery;
use App\Models\Clients\InternetBanking;
use App\Models\Clients\RetProd;
use App\Models\Clients\SubDescription;
use App\Models\Color;
use App\Models\Contacts;
use App\Models\Filter;
use App\Models\Product;
use App\Models\ShopCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ClientController extends Controller
{
    public function client()
    {
        $bankCards = BankCards::get();
        $descriptions = ClientPaymentDesc::first();
        $descriptions = $descriptions->translate(session('locale'));
        $internets = InternetBanking::get();
        $cursives = ClientsCursive::first();
        $cursives = $cursives->translate(session('locale'));
        $delivers = ClientsDelivery::get();
        $delivers = $delivers->translate(session('locale'));
        $cities = City::with('show')->get();
        $products = Product::get();
        $products = $products->translate(session('locale'));
        $steps = RetProd::get();
        $steps = $steps->translate(session('locale'));
        $subs = SubDescription::get();
        $subs = $subs->translate(session('locale'));
        $totalQuantity = 0;
        $totalSum = 0;
        $cart = session()->get('cart');
        if (!$cart) {
            return view('client',
                compact(
                    'cart',
                    'products',
                    'bankCards', 'internets',
                    'cursives', 'delivers',
                    'steps', 'subs',
                    'cities', 'descriptions',
                )
            );
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                if (Auth::check()) {
                    $totalSum += $quantity * $cartProduct->partners_price;
                } elseif (!Auth::check() && $cartProduct->discount_price != null) {
                    $totalSum += $quantity * $cartProduct->discount_price;
                } else {
                    $totalSum += $quantity * $cartProduct->price;
                }
                $totalQuantity += $cartProduct->quantity;
            }
        }

        return view('client',
            compact(
                'cart', 'products', 'cartNew',
                'totalSum',
                'bankCards', 'internets',
                'cursives', 'delivers',
                'steps', 'subs',
                'cities', 'descriptions', 'totalQuantity'
            )
        );
    }


    public function clientCategory($id, Request $request)
    {
        $currentUrl = $request->url();
        $_filters = [];
        $category_name = ShopCategory::findOrFail($id)->title;
        $list = $request->all();

        $products = Product::with('productFilters')->where('shop_category_id', $id)->where(function ($query) use ($request, $id) {
            if ($request->has('color')) {
                $query->whereIn('color_id', $request->color);
            }

            if ($request->has('price_from') && $request->price_from != '') {
                $query->where('price', '<=', $request->price_from);
            }

            if ($request->has('price_to') && $request->price_to != '') {
                $query->where('price', '>=', $request->price_to);
            }


            if ($request->has('sort_type') && $request->has('sort_value')) {
                $query->orderBy($request->sort_type, $request->sort_value);
            }
            if ($request->has('price_asc')) {
                $query->orderBy('price', 'asc');
            }

            if ($request->has('price_desc')) {
                $query->orderBy('price', 'desc');
            }
        });
        if ($request->has('new')) {
            $products = $products->orderBy('created_at', 'desc');
        }
        if ($request->has('price_desc')) {

            $products = $products->orderBy('price', 'desc');
        }

        if ($request->has('price_asc')) {

            $products = $products->orderBy('price', 'asc');
        }

        if (isset($list["filter"])) {
            foreach ($list["filter"] as $idFilter => $filter) {
                $products->whereHas('productFilters', function ($query) use ($filter) {
                    if (!is_array($filter)) {
                        $filter = [$filter];
                    }
                    $query->wherein('title', $filter);
                });
            }
        }
        $products = $products->paginate(9);
        $_productsAll = Product::with('productFilters')->where('shop_category_id', $id)->get();
        $_products = $_productsAll->toArray();

        $filter = [];

        foreach ($_products as &$product) {
            foreach ($product['product_filters'] as &$filters) {

                $filterTitle = Filter::where(FilterContract::ID, $filters['filter_id'])->first();
                if (!empty($filterTitle)) {
                    $filterTitle = $filterTitle->toArray();
                    if (!array_key_exists($filterTitle[FilterContract::TITLE], $filter)) {
                        $filter[$filterTitle[FilterContract::TITLE]] = [];
                    }
                    $filter[$filterTitle[FilterContract::TITLE]][] = [$filterTitle['title'], $filters['title']];
                    $filter[$filterTitle[FilterContract::TITLE]] = (array_unique($filter[$filterTitle[FilterContract::TITLE]], SORT_REGULAR));
                };
            }
        }

        $cart = session()->get('cart');
        $params = [];
        $totalSum = 0;
        $totalQuantity = 0;
        $page = $request->has('page') ? $request->page : 1;

        $choosed_values = [];
        if ($request->has('filter'))
            $choosed_values = $request->filter;


        $max = Product::where('shop_category_id', $id)->orderBy('current_price', 'desc')->first()->current_price;
        $min = Product::where('shop_category_id', $id)->orderBy('current_price', 'asc')->first()->current_price;


        if (!$cart) {
            return view('catalog_clients',
                compact(
                    'cart', 'id',
                    'products', 'page', 'products',
                    'params', 'currentUrl',
                    'filter', 'choosed_values', 'max', 'min', 'category_name'
                )
            );
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                if (Auth::check()) {
                    $totalSum += $quantity * $cartProduct->partners_price;
                } else {
                    $totalSum += $quantity * $cartProduct->price;
                }
                $totalQuantity += $cartProduct->quantity;
            }
        }
        return view('catalog_clients',
            compact(
                'cartNew', 'cart', 'totalSum',
                'totalQuantity',
                'products', 'page',
                'id', 'params', 'currentUrl',
                'filter', 'choosed_values',
                'totalQuantity',
                'max', 'min', 'category_name'
            )
        );
    }

    public function clientsShop(Request $request)
    {
        $page = $request->has('page') ? $request->page : 1;
        $cart = session()->get('cart');
        $params = [];
        $totalSum = 0;
        $totalQuantity = 0;


        $_filters = [];
        $list = $request->all();

        $products = Product::with('productFilters')->where(function ($query) use ($request) {
            if ($request->has('color')) {
                $query->whereIn('color_id', $request->color);
            }

            if ($request->has('price_from') && $request->price_from != '') {
                $query->where('price', '<=', $request->price_from);
            }

            if ($request->has('price_to') && $request->price_to != '') {
                $query->where('price', '>=', $request->price_to);
            }

            if ($request->has('sort_type') && $request->has('sort_value')) {
                $query->orderBy($request->sort_type, $request->sort_value);
            }
            if ($request->has('price_asc')) {
                $query->orderBy('price', 'asc');
            }

            if ($request->has('price_desc')) {
                $query->orderBy('price', 'desc');
            }
        });
        if ($request->has('new')) {
            $products = $products->orderBy('created_at', 'desc');
        }
        if ($request->has('price_desc')) {

            $products = $products->orderBy('price', 'desc');
        }

        if ($request->has('price_asc')) {

            $products = $products->orderBy('price', 'asc');
        }

        if (isset($list["filter"])) {
            foreach ($list["filter"] as $idFilter => $filter) {
                $products->whereHas('productFilters', function ($query) use ($filter) {
                    if (!is_array($filter)) {
                        $filter = [$filter];
                    }
                    $query->wherein('title', $filter);
                });
            }
        }
        $products = $products->paginate(9);
        $_productsAll = Product::with('productFilters')->get();
        $_products = $_productsAll->toArray();

        $filter = [];

        foreach ($_products as &$product) {
            foreach ($product['product_filters'] as &$filters) {

                $filterTitle = Filter::where(FilterContract::ID, $filters['filter_id'])->first();
                if (!empty($filterTitle)) {
                    $filterTitle = $filterTitle->toArray();
                    if (!array_key_exists($filterTitle[FilterContract::TITLE], $filter)) {
                        $filter[$filterTitle[FilterContract::TITLE]] = [];
                    }
                    $filter[$filterTitle[FilterContract::TITLE]][] = [$filterTitle['title'], $filters['title']];
                    $filter[$filterTitle[FilterContract::TITLE]] = (array_unique($filter[$filterTitle[FilterContract::TITLE]], SORT_REGULAR));
                };
            }
        }

        $choosed_values = [];
        if ($request->has('filter'))
            $choosed_values = $request->filter;

        $max = Product::orderBy('current_price', 'desc')->first()->current_price;
        $min = Product::orderBy('current_price', 'asc')->first()->current_price;

        if (!$cart) {
            return view('catalog_clients',
                compact(
                    'cart',
                    'products', 'page', 'products',
                    'params', 'filter',
                    'choosed_values',
                    'min', 'max'
                )
            );
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                if (Auth::check()) {
                    $totalSum += $quantity * $cartProduct->partners_price;
                } else {
                    $totalSum += $quantity * $cartProduct->price;
                }
                $totalQuantity += $cartProduct->quantity;
            }
        }
        return view('catalog_clients',
            compact(
                'cartNew', 'cart', 'totalSum',
                'totalQuantity',
                'products',
                'page', 'params',
                'filter', 'choosed_values',
                'totalQuantity',
                'min', 'max'
            )
        );
    }

    public function filter(Request $request, $id = null) //Страницу каталогов показывает здесь
    {
        $title = 'Каталог';
        $cart = session()->get('cart');
        $category_name = ShopCategory::findOrFail($id)->title;
        $products = Product::where(function ($query) use ($request, $id) {
            if ($request->has('category')) {
                $query->whereIn('shop_category_id', $request->category);
            }

            if ($request->has('color')) {
                $query->whereIn('color_id', $request->color);
            }

            if ($request->has('price_from') && $request->price_from != '') {
                $query->where('price', '<=', $request->price_from);
            }

            if ($request->has('price_to') && $request->price_to != '') {
                $query->where('price', '>=', $request->price_to);
            }

            if ($request->has('sort_type') && $request->has('sort_value')) {
                $query->orderBy($request->sort_type, $request->sort_value);
            }
            if ($request->has('price_asc')) {
                $query->orderBy('price', 'asc');
            }

            if ($request->has('price_desc')) {
                $query->orderBy('price', 'desc');
            }
        });
        if ($request->has('new')) {
            $products = $products->orderBy('created_at', 'desc');
        }
        if ($request->has('price_desc')) {

            $products = $products->orderBy('price', 'desc');
        }

        if ($request->has('price_asc')) {

            $products = $products->orderBy('price', 'asc');
        }

        $products = $products->paginate(9);
        $currentUrl = $request->fullUrl();

        $params = $request->query();
        $requests = $request->getContent();
        return view(
            'catalog_clients', compact(
                'products', 'requests', 'title', 'currentUrl', 'params',
                'cart', 'category_name'
            )
        );
    }

    public function mainApiCatalogue(Request $request)
    {
        $list = $request->all();
        $products = Product::with('productFilters', 'colour')->where(function ($query) use ($request) {
            if ($request->has('color')) {
                $query->whereIn('color_id', $request->color);
            }
            if ($request->has('category')) {
                $query->where('shop_category_id', $request->category);
            }
            if ($request->has('price_from') && $request->price_from != '') {
                $query->where('price', '<=', $request->price_from);
            }

            if ($request->has('price_to') && $request->price_to != '') {
                $query->where('price', '>=', $request->price_to);
            }


            if ($request->has('sort_type') && $request->has('sort_value')) {
                $query->orderBy($request->sort_type, $request->sort_value);
            }
            if ($request->has('price_asc')) {
                $query->orderBy('price', 'asc');
            }

            if ($request->has('price_desc')) {
                $query->orderBy('price', 'desc');
            }
        });
        if ($request->has('new')) {
            $products = $products->orderBy('created_at', 'desc');
        }
        if ($request->has('price_desc')) {
            $products = $products->orderBy('price', 'desc');
        }

        if ($request->has('price_asc')) {

            $products = $products->orderBy('price', 'asc');
        }

        if (isset($list["filter"])) {
            foreach ($list["filter"] as $idFilter => $filter) {
                $products->whereHas('productFilters', function ($query) use ($filter) {
                    if (!is_array($filter)) {
                        $filter = [$filter];
                    }
                    $query->wherein('title', $filter);
                });
            }
        }
        $products = $products->paginate(9);

        foreach ($products as $key => $product) {
            $products[$key]['image'] = json_decode($products[$key]['image']);
        }


        $max = Product::orderBy('current_price', 'desc')->first()->current_price;
        $min = Product::orderBy('current_price', 'asc')->first()->current_price;

        return response(['products' => $products, 'min' => $min, 'max' => $max]);
    }
}
