<?php

namespace App\Http\Controllers;

use App\Models\Banks;
use App\Models\City;
use App\Models\Color;
use App\Models\Contacts;
use App\Models\MainDescriptions;
use App\Models\Product;
use App\Models\ShopCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    public function blocker()
    {
        return view('blocker');
    }

    public function method(Request $request, $data, $query)
    {
        foreach ($data as $key => $value) {
            if ($request->has($key)) {
                $query->whereIn($value['key'], $value['param']);
            }
        }
    }

    public function search(Request $request)
    {
        $search = $request->input('product_name');
        $products = Product::where('product_name', 'like', '%' . $search . '%')->with('category')->paginate(4);
        $page = $request->has('page') ? $request->page : 1;
//        $products = $products->translate(session('locale'));
        $title = $request->input('product_name');
        $params = [];
        $products = Product::where('product_name', 'like', '%' . $search . '%')->where(function ($query) use ($request) {


//            $data = [
//                'category' => [
//                    'key' => 'category',
//                    'value' => $request->category,
//                ]
//            ];
//            $this->method($request, $data, $query);
            if ($request->has('category')) {
                $query->whereIn('shop_category_id', $request->category);
            }

            if ($request->has('color')) {
                $query->whereIn('color_id', $request->color);
            }

            if ($request->has('price_from') && $request->price_from != '') {
                $query->where('price', '<=', $request->price_from);
            }

            if ($request->has('price_to') && $request->price_to != '') {
                $query->where('price', '>=', $request->price_to);
            }
            if ($request->has('sort_type') && $request->has('sort_value')) {
                $query->orderBy($request->sort_type, $request->sort_value);
            }
            if ($request->has('price_asc')) {
                $query->orderBy('price', 'asc');
            }

            if ($request->has('price_desc')) {
                $query->orderBy('price', 'desc');
            }
        });

        if ($request->has('new')) {
            $products = $products->orderBy('created_at', 'desc');
        }
        if ($request->has('price_desc')) {

            $products = $products->orderBy('price', 'desc');
        }

        if ($request->has('price_asc')) {

            $products = $products->orderBy('price', 'asc');
        }

        $products = $products->paginate(4);
        $currentUrl = $request->fullUrl();

        $params = $request->query();
        $filter = [];

        array_unique($filter);
        return view('catalog', compact(
            'products', 'page',
            'title', 'params', 'filter',
        ));
    }

    public function index()
    {
        $products = Product::with('colour')->inRandomOrder()->paginate(8);
        $products = $products->translate(session('locale'));
        $descriptions = MainDescriptions::first();
        $descriptions = $descriptions->translate(session('locale'));
        $totalSum = 0;
        $totalQuantity = 0;

        $cart = session()->get('cart');
        if (!$cart) {
            return view('index', compact('cart', 'products', 'descriptions'));
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                if (Auth::check()) {
                    $totalSum += $quantity * $cartProduct->partners_price;
                } elseif (!Auth::check() && $cartProduct->discount_price != null) {
                    $totalSum += $quantity * $cartProduct->discount_price;
                } else {
                    $totalSum += $quantity * $cartProduct->price;
                }
                $totalQuantity += $cartProduct->quantity;
            }
        }

        return view('index',
            compact('cart', 'products', 'cartNew',
                'totalSum',
                'descriptions',
                'totalQuantity'
            )
        );
    }

    public function about($id)
    {
        $products = Product::with('similar', 'category', 'colour')->find($id);
        $products = $products->translate(session('locale'));
//        $cart = session()->get('cart');
        $totalSum = 0;
        $totalQuantity = 0;
        $cart = session()->get('cart');
        if (!$cart) {
            return view('product_inner', compact('products'));
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                if (Auth::check()) {
                    $totalSum += $quantity * $cartProduct->partners_price;
                } elseif (!Auth::check() && $cartProduct->discount_price != null) {
                    $totalSum += $quantity * $cartProduct->discount_price;
                } else {
                    $totalSum += $quantity * $cartProduct->price;
                }
                $totalQuantity += $cartProduct->quantity;
            }
        }
        if ($products) {
            return view('product_inner', compact(
                'totalQuantity', 'products',
                'cartNew', 'totalSum',
                'totalQuantity'
            ));
        } else {
            return redirect()->back();
        }
    }

    public function info($id)
    {
        $products = Product::with('similar', 'category', 'colour')->find($id);
        $products = $products->translate(session('locale'));
//        $cart = session()->get('cart');
        $totalSum = 0;
        $totalQuantity = 0;
        $cart = session()->get('cart');
        if (!$cart) {
          return response(['product' => $products], 200);
        } else {
            $cartNew = [];
            foreach ($cart as $productId => $quantity) {
                $cartProduct = Product::find($productId);
                $cartProduct->quantity = $quantity;
                $cartNew[] = $cartProduct;
                if (Auth::check()) {
                    $totalSum += $quantity * $cartProduct->partners_price;
                } elseif (!Auth::check() && $cartProduct->discount_price != null) {
                    $totalSum += $quantity * $cartProduct->discount_price;
                } else {
                    $totalSum += $quantity * $cartProduct->price;
                }
                $totalQuantity += $cartProduct->quantity;
            }
        }
        if ($products) {
            return response(['product' => $products], 200);
        } else {
            return response(['message' => 'Cannot find the product'], 403);
        }
    }

    public function filter(Request $request, $id = null) //Страницу каталогов показывает здесь
    {
        $banks = City::get();
        $contacts = Contacts::get();
        $colors = Color::get();
        $colors = $colors->translate(session('locale'));
        $categories = ShopCategory::get();
        $categories = $categories->translate(session('locale'));
        $title = 'Каталог';
        $cart = session()->get('cart');
        $products = Product::where(function ($query) use ($request, $id) {
            if ($request->has('category')) {
                $query->whereIn('shop_category_id', $request->category);
            }

            if ($request->has('color')) {
                $query->whereIn('color_id', $request->color);
            }
            if ($request->has('price_from') && $request->price_from != '') {
                $query->where('price', '<=', $request->price_from);
            }

            if ($request->has('price_to') && $request->price_to != '') {
                $query->where('price', '>=', $request->price_to);
            }

            if ($request->has('sort_type') && $request->has('sort_value')) {
                $query->orderBy($request->sort_type, $request->sort_value);
            }
            if ($request->has('price_asc')) {
                $query->orderBy('price', 'asc');
            }

            if ($request->has('price_desc')) {
                $query->orderBy('price', 'desc');
            }
        });
        if ($request->has('new')) {
            $products = $products->orderBy('created_at', 'desc');
        }
        if ($request->has('price_desc')) {

            $products = $products->orderBy('price', 'desc');
        }

        if ($request->has('price_asc')) {

            $products = $products->orderBy('price', 'asc');
        }

        $products = $products->paginate(4);
        $currentUrl = $request->fullUrl();

        $params = $request->query();
        $filter = [];

        array_unique($filter);

        $requests = $request->getContent();
        return view('catalog', compact(
            'products', 'requests', 'categories', 'title', 'colors', 'contacts', 'banks', 'currentUrl', 'params', 'cart', 'filter'));
    }
}
