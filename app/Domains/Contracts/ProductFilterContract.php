<?php


namespace App\Domains\Contracts;


class ProductFilterContract implements MainContract
{
    const TABLE =   'product_filters';

    const FILLABLE  =   [
        self::PRODUCT_ID,
        self::FILTER_ID,
        self::TITLE,
        self::IS_SHOW,
        self::SLUG
    ];
}
