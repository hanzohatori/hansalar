<?php


namespace App\Domains\Contracts;


class FilterContract implements MainContract
{
    const TABLE =   'filters';
    const FILLABLE  =   [
        self::CATEGORY_ID,
        self::TITLE,
        self::IS_SHOW,
        self::SLUG
    ];
}
