<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;
use App\Domains\Contracts\ProductFilterContract;

class Product extends Model
{
    use HasFactory, SoftDeletes, Translatable;

    public function category()
    {
        return $this->belongsTo('App\Models\ShopCategory', 'shop_category_id', 'id');
    }

    public function colour()
    {
        return $this->belongsTo('App\Models\Color', 'color_id', 'id');
    }

    public function similar()
    {
        return $this->belongsToMany(
            \App\Models\Product::class,
            'similar_products',
            'product_id',
            'similar_id'
        );
    }

    protected  $fillable = ['current_price', 'current_price_for_auth'];
    protected $translatable = ['product_name', 'description', 'about_product_title', 'about_product_description', 'guarantee', 'delivery', 'origin', 'shop_category_type'];

    public function productFilters()
    {
        return $this->hasMany(\App\Models\ProductFilters::class,ProductFilterContract::PRODUCT_ID,'id');
    }

    public function cities()
    {
        return $this->belongsToMany(City::class,'products_cities');
    }
}
