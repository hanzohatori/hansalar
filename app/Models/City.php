<?php

namespace App\Models;

use App\Models\Partners\PartnersAddress;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory;

    public function show()
    {
        return $this->belongsToMany(PartnersAddress::class,'city_address');
    }
}
