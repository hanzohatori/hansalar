<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable=[
        'name',
        'surname',
        'phone',
        'address',
        'payment_type',
        'order',
        'email',
//        'payment_status_id',
//        'order_status_id',
        'delivery_id',
        'delivery_status_id',
        'user_id',
        'country_city',
        'mail_index',
        'delivery_type',
        'delivery_time',
        'total_price',
        'total_quantity',
//        'delivery_price',
        'order_number',
        'house',
        'apartment',
        'flat',
        'promocode',
        'company_name',
        'legal_address',
        'bank',
        'iic',
        'bin',
        'vat_payer'
    ];

    public function order_details()
    {
        return $this->hasMany('App\Models\OrderDetail');
    }

    public function order_statuses()
    {
        return $this->hasMany('App\Models\OrderStatus');
    }
}
