<?php

namespace App\Models\Partners;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Cursive extends Model
{
    use HasFactory, Translatable;
    protected $translatable = ['content'];
}
