<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Domains\Contracts\FilterContract;

class Filter extends Model
{
    use HasFactory;

    protected $fillable =   FilterContract::FILLABLE;

    public function category()
    {
        return $this->belongsTo(\App\Models\ShopCategory::class,'id',FilterContract::CATEGORY_ID);
    }
}
