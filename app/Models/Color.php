<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;

class Color extends Model
{
    use HasFactory, SoftDeletes, Translatable;
    public function color()
    {
        return $this->hasMany('App\Models\Product','id','color_id');
    }

    protected $translatable = ['color'];
}
