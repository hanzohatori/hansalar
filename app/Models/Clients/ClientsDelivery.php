<?php

namespace App\Models\Clients;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class ClientsDelivery extends Model
{
    use HasFactory, Translatable;
    protected $translatable = ['content'];
}
