<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use TCG\Voyager\Traits\Translatable;
use App\Domains\Contracts\FilterContract;

class ShopCategory extends Model
{
    use HasFactory, SoftDeletes, Translatable;
    protected $translatable = ['title'];

    public function parents()
    {
        return $this->belongsTo('App\Models\ShopCategory','id','parent_id');
    }
    public function children()
    {
        return $this->hasMany('App\Models\ShopCategory','parent_id','id');
    }

    public function products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function outletProducts()
    {
        return $this->hasMany('App\Models\Product')->where(function($query){
            $query->where('discount_price','!=',0);
            $query->orWhere('loan',1);
        });
    }

    public function filters()
    {
        return $this->hasMany(\App\Models\filter::class,FilterContract::CATEGORY_ID,'id');
    }

}
