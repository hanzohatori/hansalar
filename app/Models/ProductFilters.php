<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Domains\Contracts\ProductFilterContract;

class ProductFilters extends Model
{
    use HasFactory;

    protected $fillable =   ProductFilterContract::FILLABLE;

    public function product()
    {
        return $this->belongsTo(\App\Models\Product::class);
    }
}
