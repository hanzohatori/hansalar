<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use HasFactory, SoftDeletes;
    protected $fillable=[
        'order_id','product_name','unit_quantity','unit_price','product_id','image'
    ];

    public function product()
    {
        return $this->belongsTo('App\Models\Product');
    }

}
