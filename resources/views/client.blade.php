@include('layouts.header')

<section class="client catalog">
    <div class="container">
        <!-- <div class="breadscrumb">
      <ul>
        <li><a href="javascript:void(0);">Главная</a></li>
        <li><a>&nbsp;/&nbsp;</a></li>
        <li><a href="javascript:void(0);">Карточка товара</a></li>
      </ul>
    </div> -->
    <div class="catalog__title">
        <h3 class="catalog__title_h3">@lang('main.clients')</h3>
    </div>
    <div class="client__wrapper">
        <div class="client__wrapper__payment">
            <h2>@lang('main.ways')</h2>
            <div class="client__wrapper__payment__block">
                <div class="client__wrapper__payment__item">
                    <h3>@lang('main.bank_cards')</h3>
                    <div class="client__wrapper__payment__item__wrap">
                        @foreach($bankCards as $bank)
                            <div class="client__wrapper__payment__item__img">
                                <img src="{{ asset('storage/'.$bank->image) }}" alt="">
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="client__wrapper__payment__item">
                    <h3>Интернет банкинг</h3>
                    <div class="client__wrapper__payment__item__wrap">
                        @foreach($internets as $internet)
                            <div class="client__wrapper__payment__item__img">
                                <img src="{{ asset('storage/'.$internet->image) }}" alt="">
                            </div>
                        @endforeach
                        {{--            <div class="client__wrapper__payment__item__img">--}}
                        {{--              <img src="./assets/img/sber.svg" alt="">--}}
                        {{--            </div>--}}
                        {{--            <div class="client__wrapper__payment__item__img">--}}
                        {{--              <img src="./assets/img/alpha.svg" alt="">--}}
                        {{--            </div>--}}
                        {{--            <div class="client__wrapper__payment__item__img">--}}
                        {{--              <img src="./assets/img/applepay.svg" alt="">--}}
                        {{--            </div>--}}
                    </div>
                </div>
                <div class="client__wrapper__payment__item">
                    <h3>@lang('main.cash')</h3>
                    <div class="client__wrapper__payment__item__wrap">
                        <p>{!! $descriptions->description !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="client__wrapper__delivery">
            <h2>@lang('main.delivery')</h2>
            @foreach($delivers as $one)
                <p>{{ $one->content }}</p>
            @endforeach
            {{--            <p>Бессознательное интегрирует ассоцианизм, таким образом осуществляется своего рода связь с темнотой--}}
            {{--                бессознательного. Душа доступна. Воспитание, как справедливо считает Ф.Энгельс, стабильно.</p>--}}
            {{--            <p>Онтогенез речи, в представлении Морено, гомогенно понимает социальный ассоцианизм. Автоматизм выбирает--}}
            {{--                ролевой код. Скиннер выдвинул концепцию "оперантного", подкрепляемого научения, в которой импульс--}}
            {{--                сложен. Самонаблюдение устойчиво выбирает конфликтный гендер. Как было показано выше, сновидение--}}
            {{--                мгновенно понимает понимающий гомеостаз.</p>--}}
            <div class="client__wrapper__delivery__wrap">
          <span>
              {{ $cursives->content }}
          </span>
                {{--        <span>Бессознательное понимает эскапизм. Чем больше люди узнают друг друга, тем больше ретардация параллельна. Поведенческая терапия вызывает конформизм. Перцепция притягивает аутотренинг, как и предсказывает теория о бесполезном знании.</span>--}}
            </div>
        </div>
        <div class="client__wrapper__map">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4" style="padding-left: 0px;">
                        <h2>@lang('main.pickup')</h2>
                        <p>@lang('main.clients_points')</p>
                        <div class="client__wrapper__map__select">
                            <select name="" id="select_city">
                                @foreach($cities as $one)
                                    <option @if($loop->index == 0) selected
                                            @endif  value="{{$one->id}}">{{$one->city}} </option>
                                @endforeach
                            </select>
                        </div>
                        @foreach($cities as $one)
                            @foreach($one->show as $addressed)

                                <div class="client__wrapper__map__geo city_id-{{$one->id}}" id="">
                                    <div class="client__wrapper__map__geo_img">
                                        <img src="./assets/img/geo_red.svg" alt="">
                                    </div>
                                    <div class="client__wrapper__map__geo_descr">
                                        <p>{{$addressed->address}}</p>
                                        <p>Пн-Пт 9:00-20:00, Сб-Вс 10:00-18:00</p>
                                    </div>
                                </div>
                            @endforeach
                        @endforeach
                        {{--                        <div class="client__wrapper__map__geo">--}}
                        {{--                            <div class="client__wrapper__map__geo_img">--}}
                        {{--                                <img src="./assets/img/geo_red.svg" alt="">--}}
                        {{--                            </div>--}}
                        {{--                            <div class="client__wrapper__map__geo_descr">--}}
                        {{--                                <p>г. Алматы, пр. Райымбека, 221Ж (остановка КазНИВИ)</p>--}}
                        {{--                                <p>Пн-Пт 9:00-20:00, Сб-Вс 10:00-18:00</p>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}
                    </div>
                    <div class="col-md-8">
                        <iframe id="choice_city" class="client__wrapper__map_content"
                                src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d23254.116471175857!2d76.88546020980223!3d43.235395738579165!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2skz!4v1609147305673!5m2!1sru!2skz"
                                frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="client__wrapper__return">
            <h2>@lang('main.return')</h2>
            <div class="client__wrapper__return__stage__wrap">
                @foreach($steps as $one)
                    <div class="client__wrapper__return__stage">
                        <h3>{{ $one->number }}</h3>
                        <span>{{ $one->content }}</span>
                    </div>
                @endforeach
                {{--                    <div class="client__wrapper__return__stage">--}}
                {{--                        <h3>02</h3>--}}
                {{--                        <span>Очевидно, что газ вторично радиоактивен. Атом</span>--}}
                {{--                    </div>--}}
                {{--                    <div class="client__wrapper__return__stage">--}}
                {{--                        <h3>03</h3>--}}
                {{--                        <span>Очевидно, что газ вторично радиоактивен. Атом</span>--}}
                {{--                    </div>--}}
                {{--                    <div class="client__wrapper__return__stage">--}}
                {{--                        <h3>04</h3>--}}
                {{--                        <span>Очевидно, что газ вторично радиоактивен. Атом</span>--}}
                {{--                    </div>--}}

            </div>
            @foreach($subs as $one)
                <p>{{ $one->content }}</p>
            @endforeach
            {{--            <p>Онтогенез речи, в представлении Морено, гомогенно понимает социальный ассоцианизм. Автоматизм выбирает--}}
            {{--                ролевой код. Скиннер выдвинул концепцию "оперантного", подкрепляемого научения, в которой импульс--}}
            {{--                сложен.</p>--}}
            {{--            <p>Самонаблюдение устойчиво выбирает конфликтный гендер. Как было показано выше, сновидение мгновенно--}}
            {{--                понимает понимающий гомеостаз.</p>--}}
        </div>
    </div>
    </div>
    <style>
        .client__wrapper__map__geo {
            display: none;
        }

        .client__wrapper__map__geo.active {
            display: block;
        }
    </style>
    <script>
        let selectMain = document.getElementById('select_city')
        let mainEnter = selectMain.value
        selectMain.addEventListener('change', (event) => {
            let activeTabs = document.querySelectorAll(`.client__wrapper__map__geo.active`)
            for (let i = 0; i < activeTabs.length; i++) {
                activeTabs[i].classList.remove('active')

            }
            let mainTab = document.getElementsByClassName(`city_id-${event.target.value}`)
            for (let i = 0; i < mainTab.length; i++) {
                mainTab[i].classList.add('active')

            }

        })


        document.addEventListener("DOMContentLoaded", function () {
            let mainTab = document.getElementsByClassName(`city_id-${selectMain.value}`)
            for (let i = 0; i < mainTab.length; i++) {
                mainTab[i].classList.add('active')

            }
        });


    </script>
</section>
@include('layouts.footer')
