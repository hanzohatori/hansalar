@include('layouts.header')
<section class="catalog">
    <div class="container">
        <form action="{{ '/catalogue' }}" method="post">
            <div class="catalog__title" style="flex-direction: column;align-items: baseline;">
            @if (url()->current() === route('clients_shop'))
                <h3 class="catalog__title_h3">Каталог</h3>
            @else
                <div class="catalog__title" style="margin-bottom: 10px">
                    <a  class="catalog__title_h3" href="{{ route('clients_shop') }}">Каталог</a> <span>></span>
                    <h3 class="catalog__title_h3">{{ $category_name }}</h3>
                </div>
            @endif
                
                <div class="catalog__title_filter" style="margin-right: 0px;">
                    <div class="catalog__title_filter" style="margin-right: 30px;">
                        <div class="filters__pricies">
                            <label class="filter__pricies--item">
                                <input 
                                    id="price_asc"
                                    type="radio"
                                    name="price_asc" 
                                    class="filter__pricies--input">
                                    <span class="radio-icon"></span>
                                    <h3>@lang('main.price_up')</h3>
                            </label>
                            <label class="filter__pricies--item">
                                <input 
                                    id="price_desc"
                                    type="radio"
                                    name="price_desc" 
                                    class="filter__pricies--input">
                                    <span class="radio-icon"></span>
                                    <h3>@lang('main.price_down')</h3>
                            </label>
                        </div>
                        {{-- <div class="catalog__title_filter__price">
                            <label class="catalog_label">
                                <h3>@lang('main.price_up')</h3>
                                <input
                                    id="price_asc"
                                    type="radio"
                                    name="price_asc">
                                <span class="checkmark"></span>
                            </label>
                            <label class="catalog_label">
                                <h3>@lang('main.price_down')</h3>
                                <input
                                    id="price_desc"
                                    type="radio"
                                    name="price_desc">
                                <span class="checkmark"></span>
                            </label>
                        </div> --}}
                        <div class="filters__pricies">
                            <label class="filter__pricies--item"> 
                                <h3>@lang('main.new')</h3>
                                <input 
                                    id="is_new"
                                    type="radio"
                                    name="is_new" 
                                    class="filter__pricies--input"
                                    {{-- @if(array_key_exists('is_new',$params)) checked @endif --}}>
                                <span class="checkbox-icon"></span></label>
                        </div>
                        <div class="filters__pricies">
                            <label class="filter__pricies--item"> 
                                <h3>@lang('main.popular')</h3>
                                <input 
                                    id="is_popular"
                                    type="radio"
                                    name="is_popular" 
                                    class="filter__pricies--input"
                                    {{-- @if(array_key_exists('is_popular',$params)) checked
                                    @endif --}}>
                                <span class="checkbox-icon"></span></label>
                        </div>
                        {{--<div class="catalog__title_filter__news">
                            <label class="catalog_checkbox catalog_checkbox__label">
                                <h3>@lang('main.new')</h3>
                                <input type="checkbox" name="is_new" id="is_new"
                                       @if(array_key_exists('is_new',$params)) checked @endif>
                                <span class="checkmark_checkbox"></span>
                            </label>
                        </div>
                        <div class="catalog__title_filter__populate">
                            <label class="catalog_checkbox">
                                <h3>@lang('main.popular')</h3>
                                <input type="checkbox" @if(array_key_exists('is_popular',$params)) checked
                                       @endif name="is_popular" id="is_popular">
                                <span class="checkmark_checkbox"></span>
                            </label>
                            {{--                         <button type="submit" class="categories_filter__btn"--}}
                            {{--                                style="color: #fff; height: 30px; font-size: 10px; margin-top: 11px; margin-right: -12px; padding: 0px;">--}}
                            {{--                            @lang('main.use')--}}
                            {{--                        </button> --}}
                        </div>
                    </div>
                </div>
    
            </div>
            <div class="container">
                <button class="filter_btn" type="button" data-toggle="collapse" data-target="#collapseExample"
                    aria-expanded="false"
                    aria-controls="collapseExample">
                @lang('main.cat_filter')
            </button>
            </div>
    
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 filter__wrap_collapse" id="collapseExample">
                        <div class="categories_filter">
                            <div class="categories_filter__links">
                                <h3 class="categories_filter__links__title">@lang('main.category')</h3>
                                <ul class="categories_filter__links__list" style="border: none">
                                    @foreach($categories as $category)
                                        <li>
                                            <a href="{{route('clientCategory',$category->id)}}">
                                                <h3>{{ $category->title }}</h3>
                                                <span>{{ count($category->products) }}</span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
    
    
                        <div class="categories_filter filter_wrap">
                            <div class="categories_filter__links">
                                <div class="categoriest_filter_reset_wrap">
                                    <a href=""
                                       {{-- <a href="{{ route('catalogue') }}" onclick="resetFilter()" --}}
                                       class="categories_filter__reset_btn">
                                        @lang('main.discard')
                                    </a>
                                </div>
    
                                @foreach($filter as $key=>$value)
                                    <div class="categoriest_catalogue" data-count={{$loop->iteration}}>
    
                                        <h3 class="categories_filter__links__title categories_title">{{$key}}</h3>
                                        <ul class="categories_filter__links__list">
                                            @foreach($value as &$item)
                                                <li>
                                                    <label class="catalog_checkbox catalog_checkbox__label">
                                                        <h3 class="catalog_checkbox__label_h3">{{$item[1]}}
                                                        </h3>
                                                        <input type="checkbox" name="filter[{{$item[0]}}][]"
                                                               onclick="filtering(this,'{{$key}}','{{$item[1]}}')"
                                                               checker="0"
                                                               class="catalogue_input"
    
                                                               value="{{$item[1]}}">
                                                        <span class="checkmark_checkbox"></span>
                                                    </label>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endforeach
                            </div>
                        </div>
    
    
                        <div class="categories_filter">
                            <div class="categories_filter__links">
                                {{--                            <h3 class="categories_filter__links__title">@lang('main.color')</h3>--}}
                                <ul class="categories_filter__links__list">
                                    <li>
                                        <a href="javascript:void(0);" class="catalog__filter_a">
                                            <h4>@lang('main.show_more') <img src="/assets/img/filter_bottom_red.svg" alt=""></h4>
                                        </a>
                                    </li>
                                </ul>
                                <script>
                                    var minRangeValue = {{ $min }};
                                    var maxRangeValue = {{ $max }};
                                </script>
                                <h3 class="categories_filter__links__title"
                                    style="padding-bottom: 0px; margin: 0px;">@lang('main.price')</h3>
                                <div class="categories_filter__range" min="0" max="2000000">
                                    <input class="catergories_filter__range_input" type="text" onchange="debounce()()">
                                    <script>
                                        let timer;
                                        function debounce(timeout = 300){
                                            return () => {
                                                clearTimeout(timer);
                                                timer = setTimeout(() => { mainFilterings() }, timeout);
                                            };
                                        }
                                    </script>
                                    {{--                                <input class="catergories_filter__range_input" type="text">--}}
                                    <div class="categories_filter__range_block">
                                        <div class="categories_filter__range_block__item">
                                            <h3>Min</h3>
                                            <input type="number"
                                                   class="pricefrom"
                                                   name="price_from"
                                                   id="price_from"
                                                   min="{{ $min  }}"
                                                   value="{{ $min }}"
                                                   max="{{ $max }}"
                                                   onchange="mainFilterings()">
                                        </div>
                                        <span>-</span>
                                        <div class="categories_filter__range_block__item categories_filter__range_block__itemMax">
                                            <h3>Max</h3>
                                            <input type="number" class="priceto" name="price_to" id="price_to" min="{{ $min  }}"
                                                   max="{{ $max }}"
                                                   value="{{ $max }}"
                                                   onchange="mainFilterings()">
                                        </div>
                                    </div>
                                </div>
    
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-9">
                        <input type="hidden" name="user" id="user_data"
                               value="{{\Illuminate\Support\Facades\Auth::user()}}">
                        <div class="recommendation_product__descr" id="main_product_panel"
                             style="justify-content: flex-start; margin-left: 20px;">
                        </div>
                    </div>
                </div>
            </div>
        </form>
    
        <div class="catalog__pagination">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-9" style="padding: 0px">
                        <div class="catalog__pagination_descr">
                            {{ $products->links('pagination') }}
                            <div class="catalog__pagination_descr__result"></div>
                        </div>
                    </div>
                </div>
            </div>
            <script>
    
            </script>
        </div>
    
        <a id="btnup" href="#" class="catalog__btn--up">
            <svg width="11" height="11" viewBox="0 0 11 11" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path d="M10 5.5L1 5.5M10 5.5L6 1.5M10 5.5L6 9.5" stroke="#D50032" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round"/>
                </svg>
                
        </a>
    </div>
</section>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script>
    let categoryElement = document.getElementById('category_id')
    if (categoryElement) {
        var category_id = categoryElement.value
    }
    let price_asc = document.getElementById('price_asc')
    let price_desc = document.getElementById('price_desc')
    let is_new = document.getElementById('is_new')
    let is_popular = document.getElementById('is_popular')
    let search_input = document.querySelector('.search_input')
    let links = document.querySelectorAll('.link_page')
    let user_id = document.getElementById('user_id')
    let currentPage = 0;
    let nextPageUrl = ''
    let prevPageUrl = ''
    let url = `/api/clients/products?${category_id !== '' ? `category=${category_id}` : ''}${user_id.value !== '' ? `user=${user_id.value}` : ''}`;
    let filterBody = {}
    let filter = new Object()

    document.addEventListener('DOMContentLoaded', async function () {
        function getParameterByName(name, url = window.location.href) {
            name = name.replace(/[\[\]]/g, '\\$&');
            var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
                results = regex.exec(url);
            if (!results) return null;
            if (!results[2]) return '';
            return decodeURIComponent(results[2].replace(/\+/g, ' '));
        }

        if (window.location.pathname == '/clients/products') {
            let mainPanel = document.getElementById('main_product_panel')
            mainPanel.innerHTML = `
        <div class="loader_wrap"><div class='loader'></div></div><style>.loader {
            border: 16px solid #ccc; /* Light grey */
            border-top: 16px solid #D50032; /* Blue */
            border-radius: 50%;
            display:flex;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            }
            .loader_wrap{
                display:flex;

                justify-content:center;
                align-items:center;
                width:100%;
                height:200px;

            }
            @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
            }</style>`
            let response = await fetch(url, {
                headers: {'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content },
                method:"POST",
                body:JSON.stringify(filterBody)
            })
            let body = await response.json()
            prevPageUrl = body.products.prev_page_url
            nextPageUrl = body.products.next_page_url
            currentPage = body.products.current_page

            let params = getParameterByName('product_name')
            if (params) {
                search(params)
            } else {
                renderHtmlProducts(body.products.data)
            }
        }
        if (window.location.pathname == `/clients/products/categories/${category_id}`) {
            let mainPanel = document.getElementById('main_product_panel')
            mainPanel.innerHTML = `
        <div class="loader_wrap"><div class='loader'></div></div><style>.loader {
            border: 16px solid #ccc; /* Light grey */
            border-top: 16px solid #D50032; /* Blue */
            border-radius: 50%;
            display:flex;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            }
            .loader_wrap{
                display:flex;

                justify-content:center;
                align-items:center;
                width:100%;
                height:200px;

            }
            @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
            }</style>`
            let response = await fetch(url, {
                headers: {'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content },
                method:"POST",
                body:JSON.stringify(filterBody)
            })
            let body = await response.json()
            let params = getParameterByName('product_name')
            prevPageUrl = body.products.prev_page_url
            nextPageUrl = body.products.next_page_url
            currentPage = body.products.current_page
            if (params) {
                search(params)
            } else {
                renderHtmlProducts(body.products.data)
            }
        }
    })

    async function resetFilter() {
        var inputs = document.getElementsByClassName('checkmark_checkbox');
        for (var i = 0; i < inputs.length; i++) {
            inputs[i].removeChild(inputs[i].firstChild)
        }
        let mainPanel = document.getElementById('main_product_panel')
        mainPanel.innerHTML = `
        <div class="loader_wrap"><div class='loader'></div></div><style>.loader {
            border: 16px solid #ccc; /* Light grey */
            border-top: 16px solid #D50032; /* Blue */
            border-radius: 50%;
            display:flex;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            }
            .loader_wrap{
                display:flex;

                justify-content:center;
                align-items:center;
                width:100%;
                height:200px;

            }
            @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
            }</style>`
        let response = await fetch(url, {
            headers: {'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content },
            method:"POST",
            body:JSON.stringify(filterBody)
        })
        let body = await response.json()
        prevPageUrl = body.products.prev_page_url
        nextPageUrl = body.products.next_page_url
        currentPage = body.products.current_page

        renderHtmlProducts(body.products.data)
    }

    async function mainFilterings() {
        filterBody.price_to = document.getElementById('price_to').value
        filterBody.price_from = document.getElementById('price_from').value
        let mainPanel = document.getElementById('main_product_panel')
        mainPanel.innerHTML = `
        <div class="loader_wrap"><div class='loader'></div></div><style>.loader {
            border: 16px solid #ccc; /* Light grey */
            border-top: 16px solid #D50032; /* Blue */
            border-radius: 50%;
            display:flex;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            }
            .loader_wrap{
                display:flex;

                justify-content:center;
                align-items:center;
                width:100%;
                height:200px;

            }
            @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
            }</style>`
        let response = await fetch(url, {
            headers: {'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content },
            method:"POST",
            body:JSON.stringify(filterBody)
        })
        let body = await response.json()
        prevPageUrl = body.products.prev_page_url
        nextPageUrl = body.products.next_page_url
        currentPage = body.products.current_page

        renderHtmlProducts(body.products.data)
    }

    async function filtering(element, filter_name, filter_value) {
        let checker = element.getAttribute('checker')
        if (!filter.hasOwnProperty(filter_name)) {
            filter[filter_name] = []
            filter[filter_name].push(filter_value)
            element.setAttribute('checker', 1)
        } else {
            if (checker == 1) {
                element.setAttribute('checker', 0)
                filter[filter_name] = filter[filter_name].filter(item=>item !== filter_value)
                if(!filter[filter_name].length){
                    delete filter[filter_name]
                }
            } else {
                element.setAttribute('checker', 1)
                filter[filter_name].push(filter_value)
            }

        }

        filterBody.filter = filter
        let mainPanel = document.getElementById('main_product_panel')
        mainPanel.innerHTML = `
        <div class="loader_wrap"><div class='loader'></div></div><style>.loader {
            border: 16px solid #ccc; /* Light grey */
            border-top: 16px solid #D50032; /* Blue */
            border-radius: 50%;
            display:flex;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            }
            .loader_wrap{
                display:flex;

                justify-content:center;
                align-items:center;
                width:100%;
                height:200px;

            }
            @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
            }</style>`
        let response = await fetch(url,{
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content
            },
            method: "POST",
            body:JSON.stringify(filterBody)
        })
        let body = await response.json()

        prevPageUrl = body.products.prev_page_url
        nextPageUrl = body.products.next_page_url
        currentPage = body.products.current_page

        renderHtmlProducts(body.products.data)
    }

    async function getByCategory(category) {
        filterBody.category = category

        let mainPanel = document.getElementById('main_product_panel')
        mainPanel.innerHTML = `
        <div class="loader_wrap"><div class='loader'></div></div><style>.loader {
            border: 16px solid #ccc; /* Light grey */
            border-top: 16px solid #D50032; /* Blue */
            border-radius: 50%;
            display:flex;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            }
            .loader_wrap{
                display:flex;

                justify-content:center;
                align-items:center;
                width:100%;
                height:200px;

            }
            @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
            }</style>`
        let response = await fetch(url, {
            headers: {'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content },
            method:"POST",
            body:JSON.stringify(filterBody)
        })
        let body = await response.json()

        prevPageUrl = body.products.prev_page_url
        nextPageUrl = body.products.next_page_url
        currentPage = body.products.current_page

        renderHtmlProducts(body.products.data)
    }

    async function filterByParamName(price_to) {
        filterBody.price_to = price_to
        let mainPanel = document.getElementById('main_product_panel')
        mainPanel.innerHTML = `
        <div class="loader_wrap"><div class='loader'></div></div><style>.loader {
            border: 16px solid #ccc; /* Light grey */
            border-top: 16px solid #D50032; /* Blue */
            border-radius: 50%;
            display:flex;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            }
            .loader_wrap{
                display:flex;

                justify-content:center;
                align-items:center;
                width:100%;
                height:200px;

            }
            @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
            }</style>`
        let response = await fetch(url, {
            headers: {'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content },
            method:"POST",
            body:JSON.stringify(filterBody)
        })
        let body = await response.json()
        prevPageUrl = body.products.prev_page_url
        nextPageUrl = body.products.next_page_url
        currentPage = body.products.current_page

        renderHtmlProducts(body.products.data)
    }

    search_input.addEventListener('keypress', (e) => {
        if (e.key == 'Enter') {
            let url = window.location.pathname
            if (url == '/clients/products') {
                search(search_input.value)
                e.preventDefault()
            } else {
                let query = new URLSearchParams(filterBody).toString()

                window.location.replace(`/clients/products?${query}`)
            }
        }
    })

    function renderHtmlProducts(products) {
        let userData = document.getElementById('user_data');
        let mainPanel = document.getElementById('main_product_panel');
        let products_array = []; 

        if (mainPanel) {
            mainPanel.innerHTML = '';
            

            products.forEach(product => {
                mainPanel.innerHTML += `
                        <div class="recommendation_product__descr__item" style="flex: 0 0 30%;">
                                    <a href="/product/about/${product.id}">
                                    ${ product.discount_price !== null ? "<div class='recommendation_product__descr__item_icon'> <img src='/assets/img/discount_icon.svg' alt=''></div>" : ''}
                                    ${product.loan === "1" ? '<div class="recommendation_product__descr__item_icon_2"><img src="/assets/img/ras.svg" alt=""></div>' : ''}
                        <div class="recommendation_product__descr__item_img" style="max-width: 201px;">
                        <img src="/storage/${product.image[0]}" alt="" style="height: 181px">
                        </div>
                        <div class="recommendation_product__descr__item_text recommendation_product__normalHeight">
                        <h3 class="recommendation_product__descr__item_text__title">${product.product_name}</h3>
                        <div class="recommendation_product__descr__item_text__price">
                        ${product.discount_price !== null ? `<h3 class="now_price">${product.discount_price} ₸</h3><h3 class="old_price">${product.price} ₸</h3>` : ''}
                        ${product.discount_price === null ? ` <h3 class="now_price">${product.price} ₸</h3>` : ''}
                        </div>
                        <div class="recommendation_product__descr__item_text__status">
                            ${product.stock > 0 ? `<h3>В наличии</h3>` : `<h3>Нет в наличии</h3>`}
                        </div>
                    </div>
                </a>

                <div>
                    <a class="recommendation_product__descr__item_btn" href="#">
                                            <img class="cart_noactive" src="/assets/img/cart_red.svg" alt="">
                                            <img class="cart_active" src="/assets/img/cart_white.svg" alt="">
                                            <span>В корзину</span>
                    </a>
                </div>
            </div>
        `;
        products_array.push(product);
            });
            let cards_nodeList = document.querySelectorAll('.recommendation_product__descr__item_btn');

            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000,
                timerProgressBar: true
            });

            cards_nodeList.forEach((el, id) => {
                    el.addEventListener('click', async (e) => {
                        e.preventDefault();
                        addOneFromCart(e, products[id].id);
                        await fetch(e.currentTarget.href);

                    Toast.fire({
                        icon:  'success',
                        title: 'Товар добавлен в корзину'
                    });
                });
            });
        }
    }

    price_asc.addEventListener('click', async () => {
        filterBody.price_asc = '1'

        if (filterBody.hasOwnProperty('price_desc')) {
            delete filterBody.price_desc
        }
        let mainPanel = document.getElementById('main_product_panel')
        mainPanel.innerHTML = `
        <div class="loader_wrap"><div class='loader'></div></div><style>.loader {
        border: 16px solid #ccc; /* Light grey */
        border-top: 16px solid #D50032; /* Blue */
        border-radius: 50%;
        display:flex;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
        }
        .loader_wrap{
            display:flex;

            justify-content:center;
            align-items:center;
            width:100%;
            height:200px;

        }
        @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
        }</style>`
        let response = await fetch(url,{
            headers: {'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content },
            method:"POST",
            body:JSON.stringify(filterBody)
        })
        let body = await response.json()
        prevPageUrl = body.products.prev_page_url
        nextPageUrl = body.products.next_page_url
        currentPage = body.products.current_page

        renderHtmlProducts(body.products.data)
    })

    price_desc.addEventListener('click', async () => {
        filterBody.price_desc = '1'
        if (filterBody.hasOwnProperty('price_asc')) {
            delete filterBody.price_asc
        }
        let mainPanel = document.getElementById('main_product_panel')
        mainPanel.innerHTML = `
        <div class="loader_wrap"><div class='loader'></div></div><style>.loader {
        border: 16px solid #ccc; /* Light grey */
        border-top: 16px solid #D50032; /* Blue */
        border-radius: 50%;
        display:flex;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
        }
        .loader_wrap{
            display:flex;

            justify-content:center;
            align-items:center;
            width:100%;
            height:200px;

        }
        @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
        }</style>`
        let response = await fetch(url,{
            headers: {'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content },
            method:"POST",
            body:JSON.stringify(filterBody)
        })
        let body = await response.json()
        prevPageUrl = body.products.prev_page_url
        nextPageUrl = body.products.next_page_url
        currentPage = body.products.current_page

        renderHtmlProducts(body.products.data)
    })

    is_popular.addEventListener('click', async () => {

        filterBody.is_popular = '1'
        

        let mainPanel = document.getElementById('main_product_panel')
        mainPanel.innerHTML = `
        <div class="loader_wrap"><div class='loader'></div></div><style>.loader {
        border: 16px solid #ccc; /* Light grey */
        border-top: 16px solid #D50032; /* Blue */
        border-radius: 50%;
        display:flex;
        width: 120px;
        height: 120px;
        animation: spin 2s linear infinite;
        }
        .loader_wrap{
            display:flex;

            justify-content:center;
            align-items:center;
            width:100%;
            height:200px;

        }
        @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
        }</style>`
        let response = await fetch(url,{
            headers: {'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content },
            method:"POST",
            body:JSON.stringify(filterBody)
        })
        let body = await response.json()
        prevPageUrl = body.products.prev_page_url
        nextPageUrl = body.products.next_page_url
        currentPage = body.products.current_page

        renderHtmlProducts(body.products.data);
        delete filterBody.is_popular;
    })

    is_new.addEventListener('click', async () => {

            filterBody.is_new = '1';

        let mainPanel = document.getElementById('main_product_panel')
        mainPanel.innerHTML = `
        <div class="loader_wrap"><div class='loader'></div></div><style>.loader {
            border: 16px solid #ccc; /* Light grey */
            border-top: 16px solid #D50032; /* Blue */
            border-radius: 50%;
            display:flex;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            }
            .loader_wrap{
                display:flex;

                justify-content:center;
                align-items:center;
                width:100%;
                height:200px;

            }
            @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
            }</style>`
        let response = await fetch(url,{
            headers: {'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content },
            method:"POST",
            body:JSON.stringify(filterBody)
        })
        let body = await response.json()
        prevPageUrl = body.products.prev_page_url
        nextPageUrl = body.products.next_page_url
        currentPage = body.products.current_page

        renderHtmlProducts(body.products.data);
        delete filterBody.is_new;
    })

    async function search(val) {
        filterBody.search = val;
        let mainPanel = document.getElementById('main_product_panel')
        mainPanel.innerHTML = `
        <div class="loader_wrap"><div class='loader'></div></div><style>.loader {
            border: 16px solid #ccc; /* Light grey */
            border-top: 16px solid #D50032; /* Blue */
            border-radius: 50%;
            display:flex;
            width: 120px;
            height: 120px;
            animation: spin 2s linear infinite;
            }
            .loader_wrap{
                display:flex;

                justify-content:center;
                align-items:center;
                width:100%;
                height:200px;

            }
            @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
            }</style>`
        let response = await fetch(url, {
            headers: {'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content },
            method:"POST",
            body:JSON.stringify(filterBody)
        })
        let body = await response.json()
        prevPageUrl = body.products.prev_page_url
        nextPageUrl = body.products.next_page_url
        currentPage = body.products.current_page

        renderHtmlProducts(body.products.data)
    }

    async function paginate() {
        links.forEach(item => {
            item.addEventListener('click', async (e) => {
                links.forEach(i => i.classList.remove('active_paginate'))
                e.preventDefault()
                filterBody.page = item.getAttribute('data-id');
                let query = new URLSearchParams(filterBody).toString()
                window.scrollTo(0, 0);
                item.classList.add('active_paginate')
                let mainPanel = document.getElementById('main_product_panel')
                mainPanel.innerHTML = `
                <div class="loader_wrap"><div class='loader'></div></div><style>.loader {
                border: 16px solid #ccc; /* Light grey */
                border-top: 16px solid #D50032; /* Blue */
                border-radius: 50%;
                display:flex;
                width: 120px;
                height: 120px;
                animation: spin 2s linear infinite;
                }
                .loader_wrap{
                    display:flex;

                    justify-content:center;
                    align-items:center;
                    width:100%;
                    height:200px;

                }
                @keyframes spin {
                0% { transform: rotate(0deg); }
                100% { transform: rotate(360deg); }
                }</style>`
                let response = await fetch(url,{
                    headers: {'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content },
                    method:"POST",
                    body:JSON.stringify(filterBody)
                })
                let body = await response.json()
                prevPageUrl = body.products.prev_page_url
                nextPageUrl = body.products.next_page_url
                currentPage = body.products.current_page

                renderHtmlProducts(body.products.data)
            })
        })
    }
    paginate()

    let nextArrow = document.getElementById('next')
    if (nextArrow) {
        nextArrow.addEventListener('click', async function (e) {
            window.scrollTo(0, 0);

            let response = await fetch(nextPageUrl,{
                headers: {'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
            })
            let body = await response.json()
            prevPageUrl = body.products.prev_page_url
            nextPageUrl = body.products.next_page_url
            currentPage = body.products.current_page
            links.forEach(i => {
                if (i.getAttribute('data-id') == currentPage) {
                    i.classList.add('active_paginate')
                } else {
                    i.classList.remove('active_paginate')
                }
            })
            renderHtmlProducts(body.products.data)

        })
    }

    let prevArrow = document.getElementById('prev')
    if (prevArrow) {
        prevArrow.addEventListener('click', async function (e) {
            window.scrollTo(0, 0);


            let response = await fetch(prevPageUrl,{
                headers: {'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content }
            })
            let body = await response.json()
            prevPageUrl = body.products.prev_page_url
            nextPageUrl = body.products.next_page_url
            currentPage = body.products.current_page
            links.forEach(i => {
                if (i.getAttribute('data-id') == currentPage) {
                    i.classList.add('active_paginate')
                } else {
                    i.classList.remove('active_paginate')
                }
            })
            renderHtmlProducts(body.products.data)
        })
    }

</script>
@include('layouts.footer')

