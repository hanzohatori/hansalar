@if ($paginator->hasPages())
    <nav class="catalog__pagination_descr__links">
        <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($paginator->onFirstPage())
                <li class="disabled prev" aria-disabled="true" aria-label="@lang('pagination.previous')" id="prev">
                    <span aria-hidden="true"><img class="prev_pagination" src="/assets/img/pagination_next.svg" alt=""></span>
                </li>
            @else
                <li class="prev" id="prev">
                    <a href="{{ $paginator->previousPageUrl() }}" style="cursor: pointer;" rel="prev" aria-label="@lang('pagination.previous')"><img class="prev_pagination" src="/assets/img/pagination_next.svg" alt=""></a>
                </li>
            @endif

            {{-- Pagination Elements --}}
            @foreach ($elements as $element)
                {{-- "Three Dots" Separator --}}
                @if (is_string($element))
                    <li class="disabled" aria-disabled="true"><span>{{ $element  }}</span></li>
                @endif

                {{-- Array Of Links --}}
                @if (is_array($element))
                    @foreach ($element as $page => $url)
                        @if ($page == $paginator->currentPage())
                            <li class="link_page active_paginate" aria-current="page">{{ $page }}</li>
                        @else
                            <li class="link_page" style="cursor: pointer;" data-id="{{ $page }}">{{ $page }}</li>
                        @endif
                    @endforeach
                @endif
            @endforeach

            {{-- Next Page Link --}}
            @if ($paginator->hasMorePages())
                <li class="next" id="next">
                    <a href="{{ $paginator->nextPageUrl() }}" rel="next" style="cursor: pointer;" aria-label="@lang('pagination.next')"><img src="/assets/img/pagination_next.svg" alt=""></a>
                </li>
            @else
                <li class="disabled next" aria-disabled="true"  aria-label="@lang('pagination.next')">
                    <span aria-hidden="true"><img src="/assets/img/pagination_next.svg" alt=""></span>
                </li>
            @endif
        </ul>
    </nav>
@endif
