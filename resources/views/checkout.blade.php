@include('layouts.header')
<section class="basket checkout checkout_wrapper__paddingNormal" id="vue-checkout">
    <div class="container">
        <div class="row">
            <div class="col-xl-8">
                <div class="hansa-title">
                    <h1>@lang('main.making')</h1>
                </div>
                <div class="checkout-left">
                    <div class="checkout-top">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="fiz-tab" data-toggle="tab" href="#fiz" role="tab"
                                   aria-controls="fiz" aria-selected="true">@lang('main.fiz')</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="ur-tab" data-toggle="tab" href="#ur" role="tab"
                                   aria-controls="ur" aria-selected="false">@lang('main.ur')</a>
                            </li>
                        </ul>
                    </div>

                    <div class="checkout-medium">


                        <div class="tab-content" id="myTabContent">

                            <div class="tab-pane fade show active" id="fiz" role="tabpanel"
                                 aria-labelledby="fiz-tab">
                                <form action="{{ route('submit') }}" method="POST" id="formsample">
                                    @csrf
                                    <input type="hidden" id="fiz" name="type_of_form" value="fiz">
                                    <div class="basket-content checkout-border">
                                        <div class="row">
                                            <div class="col-xl-6 col-md-6">
                                                <div class="product-name product-text">
                                                    <h2 order-title>@lang('main.order_info')</h2>
                                                    <h3 order-error style="color: red; display: none;">Проверьте
                                                        правильность заполнения полей</h3>
                                                </div>
                                                <div class="checkout-content">
                                                    <span class="checkout-content-req">
                                                        <span>*</span> Обязательно</span>
                                                    <input type="text" placeholder="@lang('main.name_order')"
                                                           name="name" validate>
                                                    <span class="checkout-content-req">
                                                        <span>*</span> Обязательно</span>
                                                    </span>

                                                    <input type="email" class="email" id="email"
                                                           placeholder="Example@gmail.com" name="emailfield" required
                                                           data-msg="Формат e-mail неправильный" validate
                                                           @change="checkTheEmail">
                                                    <label for="field" style="color: #D50032;">@{{ email_error
                                                        }}</label>
                                                    <span class="checkout-content-req">
                                                        <span>*</span> Обязательно</span>
                                                    <div class="checkout-num phoe_bottom">
                                                        <input type="text" id="phone1" placeholder="XXX XXX XX XX"
                                                               name="phone" validate maxlength="13">
                                                        <span>+7</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkout-pay">
                                        <div class="basket-content checkout-border">
                                            <div class="product-name product-text">
                                                <h2>@lang('main.price')</h2>
                                            </div>
                                            <div class="pay-method">
                                                <label class="catalog_label">
                                                    <h2>@lang('main.cash_order')</h2>
                                                    <input type="radio" checked="checked" name="payment_type"
                                                           value="Наличные">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="catalog_label">
                                                    <h2>@lang('main.card_order')</h2>
                                                    <input id="cardChecker" id="card" type="radio" checked="checked"
                                                           name="payment_type"
                                                           value="Картой на сайте">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="catalog_label">
                                                    <h2>@lang('main.nocash_order')</h2>
                                                    <input class="cashless_payments" type="radio" checked="checked"
                                                           name="payment_type"
                                                           value="Безналичный расчет">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkout-pay">
                                        <div class="basket-content checkout-border">
                                            <div class="product-name product-text">
                                                <h2>@lang('main.delivery_order')</h2>
                                            </div>
                                            <div class="pay-method">
                                                <label class="catalog_label">
                                                    <h2>@lang('main.self_order')</h2>
                                                    <input type="radio" checked="checked" value="Самовывоз"
                                                           name="delivery_type">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="catalog_label">
                                                    <h2>@lang('main.notself_order')</h2>
                                                    <input type="radio" checked="checked" value="Доставка"
                                                           name="delivery_type">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-6 col-md-6">
                                            <div class="basket-content ">
                                                <div class="product-name product-text">
                                                    <h2>@lang('main.address_order')</h2>
                                                </div>
                                                <div class="checkout-select">
                                                    <span class="checkout-content-req">
                                                        <span>*</span> Обязательно</span>
                                                    <div class="select_sconte">
                                                        <select name="country_city" id="">
                                                            @foreach($cities as $one)
                                                            <option value="{{$one->city}}">{{$one->city}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="checkout-content">
                                                    <span class="checkout-content-req">
                                                        <span>*</span> Обязательно</span>
                                                    </span>
                                                    <input maxlength="50" type="text" placeholder="@lang('main.street')"
                                                           name="address" validate>
                                                    <div class="checkout-address checkout-address__normalSize">
                                                        <span class="checkout-content-req">
                                                            <span>*</span> Обязательно</span>
                                                        </span>
                                                        <input id="house" class="checkout-address__normalSize-input"
                                                               type="text" placeholder="@lang('main.house')"
                                                               name="house" validate>
                                                        <input id="floor" class="checkout-address__normalSize-input"
                                                               type="text" placeholder="@lang('main.flat')" name="flat"
                                                               validate-optional>

                                                        <input id="apartment" class="checkout-address__normalSize-input"
                                                               type="text" placeholder="@lang('main.apartment')"
                                                               name="apartment" validate-optional>
                                                    </div>

                                                </div>
                                                <textarea cols="30" rows="10"
                                                          placeholder="@lang('main.comment_order')"></textarea>
                                                <div class="checkout-btn" validate-btn>
                                                    <input type="hidden" name="user_id"
                                                           value="{{\Illuminate\Support\Facades\Auth::user() ? \Illuminate\Support\Facades\Auth::user()->id:null}}">
                                                    <button id="submit" type="submit">
                                                        @lang('main.submit_order')
                                                    </button>
                                                    <span class="checkout-content-req"
                                                          style="text-align: center; margin-top: 5px; display: block; margin-left: auto; margin-right: auto;">
                                                        <span>*</span>Все поля обязательны для заполнения</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>


                            <div class="tab-pane fade" id="ur" role="tabpanel" aria-labelledby="ur-tab">
                                <form action="{{ route('submit') }}" method="POST" id="formsample">
                                    @csrf
                                    <input type="hidden" id="ur" name="type_of_form" value="ur">
                                    <div class="basket-content checkout-border">
                                        <div class="row">
                                            <div class="col-xl-5 col-md-6">
                                                <div class="product-name product-text">
                                                    <h2 order-company-title>@lang('main.order_info')</h2>
                                                    <h3 order-company-error style="color: red; display: none;">Проверьте
                                                        правильность заполнения полей</h3>
                                                </div>
                                                <div class="checkout-content">
                                                    <span class="checkout-content-req">
                                                        <span>*</span> Обязательно</span>
                                                    <input type="text" placeholder="@lang('main.comp_order')"
                                                           name="company_name" validate-company>
                                                    <span class="checkout-content-req">
                                                            <span>*</span> Обязательно</span>

                                                    <input type="email" class="email" id="email2"
                                                           placeholder="Example@gmail.com" name="emailfield" required
                                                           data-msg="Формат e-mail неправильный"
                                                           @change="checkTheEmail2">
                                                    <label for="field" style="color: #D50032">@{{ email_error2
                                                        }}</label>
                                                    <span class="checkout-content-req">
                                                        <span>*</span> Обязательно</span>
                                                    <div class="checkout-num phoe_bottom">
                                                        <input type="text" id="phone2" placeholder="XXX XXX XX XX"
                                                               name="phone" validate-company maxlength="13">

                                                        <span>+7</span>
                                                    </div>
                                                    <span class="checkout-content-req">
                                                        <span>*</span> Обязательно</span>
                                                    <input type="text" placeholder="@lang('main.legal_address')"
                                                           name="legal_address" validate-company>
                                                    <span class="checkout-content-req">
                                                            <span>*</span> Обязательно</span>
                                                    <input type="text" placeholder="Банк" name="bank" validate-company>
                                                    <span class="checkout-content-req">
                                                        <span>*</span> Обязательно</span>
                                                    <input id="iic" type="text" placeholder="@lang('main.iic_order')"
                                                           name="iic" validate-company>
                                                    <span class="checkout-content-req">
                                                        <span>*</span> Обязательно</span>
                                                    <input id="bin" type="text" placeholder="БИН" name="bin"
                                                           validate-company>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkout-pay">
                                        <div class="basket-content checkout-border">
                                            <div class="product-name product-text">
                                                <h2>@lang('main.vat')</h2>
                                            </div>
                                            <div class="pay-method">
                                                <label class="catalog_label">
                                                    <h2>@lang('main.vat_yes')</h2>
                                                    <input type="radio" checked="checked" name="vat_payer" value="Да">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="catalog_label">
                                                    <h2>@lang('main.vat_no')</h2>
                                                    <input type="radio" checked="checked" name="vat_payer" value="Нет">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkout-pay">
                                        <div class="basket-content checkout-border">
                                            <div class="product-name product-text">
                                                <h2>@lang('main.ways')</h2>
                                            </div>
                                            <div class="pay-method">
                                                <label class="catalog_label">
                                                    <h2>@lang('main.cash_order')</h2>
                                                    <input type="radio" checked="checked" name="payment_type"
                                                           value="Наличные">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="catalog_label">
                                                    <h2>@lang('main.nocash_order')</h2>
                                                    <input class="cashless_payments" type="radio" checked="checked"
                                                           name="payment_type"
                                                           value="Безналичный расчет">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="checkout-pay">
                                        <div class="basket-content checkout-border">
                                            <div class="product-name product-text">
                                                <h2>@lang('main.delivery_order')</h2>
                                            </div>
                                            <div class="pay-method">
                                                <label class="catalog_label">
                                                    <h2>@lang('main.self_order')</h2>
                                                    <input type="radio" checked="checked" value="Самовывоз"
                                                           name="delivery_type">
                                                    <span class="checkmark"></span>
                                                </label>
                                                <label class="catalog_label">
                                                    <h2>@lang('main.notself_order')</h2>
                                                    <input type="radio" value="Доставка" name="delivery_type">
                                                    <span class="checkmark"></span>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-5 col-md-6">
                                            <div class="basket-content ">
                                                <div class="product-name product-text">
                                                    <h2>@lang('main.address_order')</h2>
                                                </div>
                                                <div class="checkout-select checkout-seleect2">
                                                    <div class="select_sconte">
                                                        <select class="select_city" id="select_city" name="country_city"
                                                                id="">
                                                            @foreach($cities as $one)
                                                            <option @if($loop->index == 0) selected
                                                                @endif value="{{ $one->city }}">{{ $one->city }}
                                                            </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-12">
                                            <div class="checkout-pay">
                                                <div class="basket-content checkout-border">
                                                    @foreach($cities as $one)
                                                    @foreach($one->show as $addressed)
                                                    <div
                                                        class="pay-method client__wrapper__map__geo city_id-{{ $one->city }}">
                                                        <label class="catalog_label">
                                                            <h2>{{ $addressed->address }}<br>
                                                                Пн-Пт 9:00-20:00, Сб-Вс 10:00-18:00
                                                            </h2>
                                                            <input type="radio"
                                                                   name="address">
                                                            <span class="checkmark"></span>
                                                        </label>
                                                    </div>
                                                    @endforeach
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xl-5">
                                            <div class="basket-content">
                                                <div class="checkout-btn" validate-company-btn>
                                                    <button>@lang('main.submit_order')</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="hansa-title hansa-title__margins">
                    <h1>@lang('main.my_orders_card')</h1>
                </div>
                <div style="display: none;" :class="'checkout-right vue-load '+load">
                    <a :href="'/product/about/' + product.id" v-for="product in products" :key="product.id">
                        <div class="checkout-item">
                            <div class="row">
                                <div class="col-xl-3 col-4 col-md-2">
                                    <div class="checkout-image">
                                        <img :src="`/storage/${JSON.parse(product.image)[0]}`" alt="">
                                    </div>
                                </div>
                                <div class="col-xl-9 col-8">
                                    <div class="order-inner">
                                        <h1>@{{ product['product_name'] }} @{{ product['colour']['color']}}</h1>
                                        <div class="order-content">
                                            @if(!\Illuminate\Support\Facades\Auth::check())
<!--                                            <p v-if="product.discount_price">-->
<!--                                                @{{ parseInt(product['discount_price']).toLocaleString('ru') }} ₸</p>-->
<!--                                            <p v-else>@{{ parseInt(product['price']).toLocaleString('ru') }} ₸</p>-->
                                            @endif
                                            <p v-if="product.discount_price" style=" white-space: pre; ">@{{product['quantity']}}  x  @{{ parseInt(product['discount_price']).toLocaleString('ru') }} ₸</p>
                                            <p style=" white-space: pre; " v-else> @{{product['quantity']}}  x  @{{ parseInt(product['price']).toLocaleString('ru') }} ₸</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div style="display: none;" :class="'checkout-right-bottom vue-load '+load">
                    <div class="basket-price-inner price-num">
                        <div class="price-left" v-if="totalQuantity">
                            <p>
                                @{{ totalQuantity }}
                                @lang('main.prods_promo')</p>
                        </div>
                        <div class="price-right" v-if="totalSum">
                            <span>@{{ (0 + totalSum).toLocaleString('ru') }} ₸</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .client__wrapper__map__geo {
        display: none !important;
    }

    .client__wrapper__map__geo.active {
        display: flex !important;
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://unpkg.com/imask"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
<script type='text/javascript'
        src="https://rawgit.com/RobinHerbots/jquery.inputmask/3.x/dist/jquery.inputmask.bundle.js"></script>
{{-- {{--
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script> --}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/inputmask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/3.3.4/inputmask/jquery.inputmask.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>

<script>

    $(document).on("keyup", ".search_input", function (e) {
        if ((e.keyCode == 13)) {
            location.href = "https://hansa.kz/catalogue?product_name=" + $(this).val();
        }
    });

    $('[validate-company-btn]').on('click', (e) => {
        let errorsList = [];
        $('[validate-company]').toArray().forEach(elem => {
            if (elem.value == null || elem.value.trim() === '') {
                elem.style.borderColor = 'red';
                $('[order-company-title]')[0].scrollIntoView();
                $('[order-company-error]')[0].style.display = 'block';
                errorsList.push(elem);
                e.preventDefault();
            } else {
                elem.style.borderColor = '';
                if (errorsList.includes(elem)) {
                    errorsList.splice(errorsList.indexOf(elem), 1);
                }
            }
        });

        if (!errorsList.length) {
            e.preventDefault();
            if (document.querySelectorAll('.cashless_payments')[1].checked) {
                window.location.href = '/paymentcard';
            } else {
                orderReady()
            }
        }
    });
</script>
<script>
    let selectMain = document.getElementById('select_city')
    let mainEnter = selectMain.value
    selectMain.addEventListener('change', (event) => {
        let activeTabs = document.querySelectorAll(`.client__wrapper__map__geo.active`)
        for (let i = 0; i < activeTabs.length; i++) {
            activeTabs[i].classList.remove('active')

        }
        let mainTab = document.getElementsByClassName(`city_id-${event.target.value}`)
        for (let i = 0; i < mainTab.length; i++) {
            mainTab[i].classList.add('active')

        }

    });

    document.addEventListener("DOMContentLoaded", function () {
        let mainTab = document.getElementsByClassName(`city_id-${selectMain.value}`)
        for (let i = 0; i < mainTab.length; i++) {
            mainTab[i].classList.add('active')

        }
    });

    const vue = new Vue({
        el: "#vue-checkout",
        data: {
            products: null,
            totalQuantity: null,
            totalSum: null,
            email_error: "",
            email_error2: "",
            load: "vue-load-active",
        },
        beforeMount() {
            storageItems = JSON.parse(localStorage.getItem('products'));
            this.products = storageItems.cart;
            this.totalSum = storageItems.totalSum;
            this.totalQuantity = storageItems.totalQuantity;
        },
        methods: {
            checkTheEmail(e) {
                const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(e.target.value);
                if (!re) {
                    this.email_error = e.target.getAttribute("data-msg");
                } else {
                    this.email_error = "";
                }
            },
            checkTheEmail2(e) {
                const re = /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(e.target.value);
                if (!re) {
                    this.email_error2 = e.target.getAttribute("data-msg");
                } else {
                    this.email_error2 = "";
                }
            }
        }

    })
</script>

<style>
    .vue-load {
        display: none !important;
    }

    .vue-load-active {
        display: block !important;
    }
</style>
@extends('layouts.footer')

