@include('layouts.header')
<section class="personal-data account_top">
    <div class="container">
        <div class="personal-data__content">
            <div class="left-side">
                <h1>@lang('main.personal_page')</h1>
                <div class="sidebar">
                    <ul class="sidebar__menu">
                        <li class="sidebar__item"><img src="/assets/img/user.svg" alt=""><a
                                href="{{ route('personal-data') }}"
                                class="sidebar__link active"> @lang('main.page_personal_info')</a></li>
                        <li class="sidebar__item"><img src="/assets/img/img2.svg" alt=""><a
                                href="{{ route('order-history') }}" class="sidebar__link"> @lang('main.order_history')</a>
                        </li>
                        <li class="sidebar__item"><img src="/assets/img/credit-card.svg" alt=""><a
                                href="{{ route('transactions') }}" class="sidebar__link"> @lang('main.trans')</a></li>
                        <li class="sidebar__item"><img src="/assets/img/lock.svg" alt=""><a
                                href="{{ route('change-password') }}" class="sidebar__link"> @lang('main.change')</a></li>
                        <button class="sidebar__btn">@lang('main.ask')</button>
                        {{--                        <a href="{{ route('logout') }}"--}}
                        {{--                           onclick="event.preventDefault();--}}
                        {{--                                                     document.getElementById('logout-form').submit();">--}}
                        {{--                                                        Logout--}}
                        {{--                        </a>--}}

                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                            <button class="sidebar__btn" style="margin-top: 10%"> @lang('main.exit')</button>
                            @csrf
                        </form>
                    </ul>
                </div>
            </div>

            <div class="right-side">
                <div class="title">
<!--                    <h2>@lang('main.personal_info')</h2>-->
                    <a style="margin-left: auto" href="{{ route('clients_shop') }}" target="_blank">
                        <button class="primary-btn">@lang('main.price_for_clients')</button>
                    </a>
                </div>

                <form action="{{ route('update') }}" class="form" method="post">
                    <div class="right-side__content">
                        @csrf
                        <div class="form">
                            <div class="form-item">
                                <label for="name">@lang('main.your_name')</label>
                                <input type="text" class="name" id="name"
                                       value="{{ $user->name }}" name="name">
                            </div>

                            <div class="form-item">
                                <label for="">@lang('main.your_surname')</label>
                                <input type="text" name="surname"
                                       value="{{ auth()->check() ? auth()->user()->surname : ''}}">
                            </div>

                            <div class="form-item">
                                <label for="">@lang('main.comp_name')</label>
                                <input type="text" name="legal_entity"
                                       value="{{ auth()->check() ? auth()->user()->legal_entity : ''}}">
                            </div>
                        </div>
                        <div class="form form2">
                            <div class="form-item">
                                <label for="">@lang('main.phone')</label>
                                <input type="text" name="phone"
                                       value="{{ auth()->check() ? auth()->user()->phone : ''}}">
                            </div>
                            <div class="form-item">
                                <label for="">Эл. почта</label>
                                <input type="text" name="email"
                                       value="{{ auth()->check() ? auth()->user()->email : ''}}">
                            </div>
                        </div>

                        <div class="form form3">
                            <div class="form-item">
                                <label for="">@lang('main.legal_address')</label>
                                <input type="text" name="legal_address"
                                       value="{{ auth()->check() ? auth()->user()->legal_address : ''}}">
                            </div>
                            <div class="form-item bank_input">
                                <label for="">Банк</label>
                                <input type="text" name="bank" class="bank_input__val"
                                       value="{{ auth()->check() ? auth()->user()->bank : ''}}">
                                <div class="bank_dropdown">
                                    @foreach ($bankso as $bank)
                                        <div class="bank_dropdown__item">{{ $bank -> bank }}</div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-item">
                                <label for="">ИИК</label>
                                <input type="text" name="iic" value="{{ auth()->check() ? auth()->user()->iic : ''}}">
                            </div>
                            <div class="form-item">
                                <label for="">БИН</label>
                                <input type="text" name="bin" value="{{ auth()->check() ? auth()->user()->bin : ''}}">
                            </div>
                            <button class="recommendation_product__descr__item_btn_submit" type="submit"
                                    style="margin-top: 5%; margin-left: 55%;">
                                <span>@lang('main.save')</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<div class="modal fade cabinet_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog partners_modal__wrap modal-sm">
        <div class="modal-content">
            <div class="partners_modal__block">
                <div class="partners_modal__block__title">
                    <h3>@lang('main.ask')</h3>
                    <img class="cabinet_exit" src="/assets/img/x.png" alt="">
                </div>
                <div class="partners_modal__block__form">
                    <form method="POST" action="{{ route('sendQuestion') }}">
                        <input type="hidden" name="user_id"
                               value="{{\Illuminate\Support\Facades\Auth::user() ? \Illuminate\Support\Facades\Auth::user()->id:null}}">
                        @csrf
                        <textarea name="question"></textarea>
                        <button type="submit">@lang('main.send')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')
