@include('layouts.header')

<section class="outlet catalog">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6">
                <div class="outlet_top">
                    <div class="outlet_top__icon">
                        <img src="./assets/img/discount_icon.svg" alt="" />
                    </div>
                    <div class="outlet_top__title">
                        <h3>Акция</h3>
                    </div>
                    <div class="outlet_top__date">
                        <h3>С 23.12.2020 —</h3>
                        <h3>До 31.12.2020</h3>
                    </div>
                </div>
                <div class="outlet_content">
                    <h3>С новым духовым шкафом Hansa
                        BOES68454 пиролиз — больше
                        не роскошь</h3>
                    <p>Новый духовой шкаф Hansa BOES68454 с пиролизом позволяет
                        навсегда забыть об утомительном процессе очистки
                        внутренней камеры духовки.</p>
                    <a href="javascript:void(0);">Перейти к товарам</a>
                </div>
            </div>
            <div class="col-md-6">
                <img class="outlet__img" src="./assets/img/outlet_img.png" alt="">
            </div>
        </div>
    </div>
    <div class="client__wrapper__delivery">
        <h2>Правила акции</h2>
        {{-- @foreach($delivers as $one)
            <p>{{ $one->content }}</p>
        @endforeach --}}
        <p>Точечное воздействие интуитивно транслирует ролевой рекламный макет. Маркетингово-ориентированное издание настроено позитивно. Инструмент маркетинга, не меняя концепции, изложенной выше, раскручивает стратегический анализ зарубежного опыта. Отраслевой стандарт пока плохо усиливает сегмент рынка. Баннерная реклама однообразно программирует жизненный цикл продукции, используя опыт предыдущих кампаний. Тем не менее, SWOT-анализ подсознательно синхронизирует коллективный поведенческий таргетинг.</p>
        <p>Направленный маркетинг, анализируя результаты рекламной кампании, основан на опыте. Стратегия предоставления скидок и бонусов, конечно, поддерживает продвигаемый инструмент маркетинга. Позиционирование на рынке без оглядки на авторитеты порождает сублимированный нестандартный подход. Пресс-клиппинг, на первый взгляд, отражает эмпирический SWOT-анализ. Анализ зарубежного опыта, как следует из вышесказанного, исключительно порождает формирование имиджа. Создание приверженного покупателя консолидирует продукт.</p>
        <p>Стимулирование коммьюнити существенно трансформирует коллективный SWOT-анализ. Рекламное сообщество непосредственно охватывает потребительский рынок. Цена клика детерминирует побочный PR-эффект. Взаимодействие корпорации и клиента концентрирует охват аудитории. Ребрендинг, как следует из вышесказанного, ригиден.</p>
        <div class="client__wrapper__delivery__wrap">
        <span>Бессознательное понимает эскапизм. Чем больше люди узнают друг друга, тем больше ретардация параллельна. Поведенческая терапия вызывает конформизм. Перцепция притягивает аутотренинг, как и предсказывает теория о бесполезном знании.</span>
          {{-- {{ $cursives->content }} --}}
        </div>
    </div>
    <div class="container-fluid" >
        <div class="row">
            <div class="col-md-12" style="padding: 0px">
                <div class="catalog__title">
                    <h3 class="catalog__title_h3">Другие акции</h3>
                </div>
            </div>
        </div>
    </div>
    <div class="outlet__item_wrap">
        <div class="outlet__item">
            <div class="outlet__item__block" style="padding: 0px">
                <div class="outlet__item__descr">
                    <h2>С новым духовым шкафом Hansa BOES68454 пиролиз — больше не роскошь</h2>
                    <h3>Акция</h3>
                    <span>23.12.2020</span>
                </div>
            </div>
            <div class="outlet__item__block" style="padding: 0px">
                <div class="outlet__item__img" style="background-image: url('public/assets/img/outlet.png');">
                    <div class="outlet__item__img_discount">
                        <img src="./assets/img/discount_icon.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="outlet__item">
            <div class="outlet__item__block" style="padding: 0px">
                <div class="outlet__item__descr">
                    <h2>Hansa представляет новый бежевый духовой шкаф BOEB68433 Fusion II</h2>
                    <h3>Акция</h3>
                    <span>23.12.2020</span>
                </div>
            </div>
            <div class="outlet__item__block" style="padding: 0px">
                <div class="outlet__item__img" style="background-image: url('public/assets/img/outlet_1.png');">
                    <div class="outlet__item__img_discount">
                        <img src="./assets/img/discount_icon.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="outlet__item">
            <div class="outlet__item__block" style="padding: 0px">
                <div class="outlet__item__descr">
                    <h2>Весь декабрь бесплатная доставка из фирменного магазина Hansa</h2>
                    <h3>Акция</h3>
                    <span>23.12.2020</span>
                </div>
            </div>
            <div class="outlet__item__block" style="padding: 0px">
                <div class="outlet__item__img" style="background-image: url('public/assets/img/outlet_2.png');">
                    <div class="outlet__item__img_discount">
                        <img src="./assets/img/discount_icon.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="outlet__item">
            <div class="outlet__item__block" style="padding: 0px">
                <div class="outlet__item__descr">
                    <h2>Hansa представляет новую серию индукционных варочных поверхностей</h2>
                    <h3>Акция</h3>
                    <span>23.12.2020</span>
                </div>
            </div>
            <div class="outlet__item__block" style="padding: 0px">
                <div class="outlet__item__img" style="background-image: url('public/assets/img/outlet_3.png');">
                    <div class="outlet__item__img_discount">
                        <img src="./assets/img/discount_icon.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="outlet__item">
            <div class="outlet__item__block" style="padding: 0px">
                <div class="outlet__item__descr">
                    <h2>Коллекция винных шкафов от Hansa</h2>
                    <h3>Акция</h3>
                    <span>23.12.2020</span>
                </div>
            </div>
            <div class="outlet__item__block" style="padding: 0px">
                <div class="outlet__item__img" style="background-image: url('public/assets/img/outlet_4.png');">
                    <div class="outlet__item__img_discount">
                        <img src="./assets/img/discount_icon.svg" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="outlet__item">
            <div class="outlet__item__block" style="padding: 0px">
                <div class="outlet__item__descr">
                    <h2>OCS и Hansa на выставке Umids 2019</h2>
                    <h3>Акция</h3>
                    <span>23.12.2020</span>
                </div>
            </div>
            <div class="outlet__item__block" style="padding: 0px">
                <div class="outlet__item__img" style="background-image: url('public/assets/img/outlet_5.png');">
                    <div class="outlet__item__img_discount">
                        <img src="./assets/img/discount_icon.svg" alt="">
                    </div>
                </div>
            </div>
        </div> --}}
    </div>
    <div class="outlet__pagination">

    </div>
</section>

@include('layouts.footer')

