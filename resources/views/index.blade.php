@include('layouts.header')
<main>
    <div id="home_slider" class="home_slider_wrap wow fadeIn" data-wow-delay="2s">
        <div class="home_slider">
            <div class="slider__padding container">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="home_slider__descr">
                            <h2 class="wow fadeInLeft" data-wow-duration="3s" data-wow-delay="2s">
                                @lang('main.slider_title') </h2>
                            <h3 class="wow fadeInLeft" data-wow-duration="3s" data-wow-delay="2s">
                                @lang('main.slider_content')</h3>
                            <a href="/catalogue/categories/5" class="wow fadeInLeft" data-wow-duration="3s"
                               data-wow-delay="2s">@lang('main.more_info')</a>
                        </div>
                    </div>
                    <div class="col-lg-7 home_slider_descr__oval">

                        <!-- <div class="home_slider__oval wow fadeInRight"  style="background:none!important;" data-wow-duration="3s" data-wow-delay="2s">
                        <div class="img-slider"  id="hansa_slide" style=""></div>
                        </div> -->

                        <!-- <div class="hover14">

                        <div class="home_slider__oval ">
                        <figure>
                        <img class="img-slider"  src="/assets/img/slider_bg.png" alt="">
                        </figure>
                        </div>

                        </div> -->
                        <div class="home_slider__oval ">
                            <img class="img-slider" src="/assets/img/slider_bg.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="home_slider">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5">
                        <div class="home_slider__descr">
                            <h2>@lang('main.slider_title') </h2>
                            <h3>@lang('main.slider_content')</h3>
                            <a href="/catalogue/categories/5">@lang('main.more_info')</a>
                        </div>
                    </div>
                    <div class="col-lg-7 home_slider_descr__oval">


                        <div class="home_slider__oval ">
                            <img class="img-slider" src="/assets/img/slider_bg.png" alt="">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <script type="text/javascript">
        var animData = {
            wrapper: document.getElementById('hansa_slide'),
            renderer: 'svg',
            loop: true,
            autoplay: true,
            path: '/dist/js/slide_p.json'

        };
        var anim = bodymovin.loadAnimation(animData);
    </script>

    <div class="our_tehnical container">
        <div class="our_tehnical__title">
            <h3>@lang('main.techs')</h3>
        </div>
        <div class="our_tehnical__links">
            <div class="our_tehnical__links_item__row">
                @foreach ($categories as $category)
                <a href="{{ route('shopCategory', $category->id) }}" class="our_tehnical__links_item">

                    <div class="our_tehnical__links_item__wrap wow fadeIn" data-wow-offset="200">
                        <div class="our_tehnical__links_item__number">
                            <span>{{ count($category->products) }}</span>
                        </div>
                        <div class="image-container">
                            <img class="our_tehnical__links_item__img"
                                 src="{{ asset('storage/' . $category->image) }}" alt="">
                            <img class="our_tehnical__links_item__img_active"
                                 src="{{ asset('storage/' . $category->image) }}" alt="">
                        </div>
                        <div class="our_tehnical__links_item__descr">
                            <span>{{ $category->title }}</span>
                            <img src="/assets/img/arrow_left.svg" alt="">
                        </div>
                    </div>
                    <div class="hover_img"></div>
                </a>
                @endforeach
                {{-- <a href="javascript:void(0);" class="our_tehnical__links_item"> --}}
                    {{--
                    <div class="our_tehnical__links_item__wrap"> --}}
                        {{--
                        <div class="our_tehnical__links_item__number"> --}}
                            {{-- <span>20</span> --}}
                            {{-- </div>
                        --}}
                        {{-- <img class="our_tehnical__links_item__img" src="./assets/img/oven.svg" alt=""> --}}
                        {{-- <img class="our_tehnical__links_item__img_active" src="./assets/img/oven_active.svg"
                                  alt=""> --}}
                        {{--
                        <div class="our_tehnical__links_item__descr"> --}}
                            {{-- <span>Духовые шкафы</span> --}}
                            {{-- <img src="./assets/img/arrow_left.svg" alt=""> --}}
                            {{-- </div>
                        --}}
                        {{-- </div>
                    --}}
                    {{--
                    <div class="hover_img"></div>
                    --}}
                    {{-- </a> --}}
                {{-- <a href="javascript:void(0);" class="our_tehnical__links_item"> --}}
                    {{--
                    <div class="our_tehnical__links_item__wrap"> --}}
                        {{--
                        <div class="our_tehnical__links_item__number"> --}}
                            {{-- <span>20</span> --}}
                            {{-- </div>
                        --}}
                        {{-- <img class="our_tehnical__links_item__img" src="./assets/img/oven.svg" alt=""> --}}
                        {{-- <img class="our_tehnical__links_item__img_active" src="./assets/img/oven_active.svg"
                                  alt=""> --}}
                        {{--
                        <div class="our_tehnical__links_item__descr"> --}}
                            {{-- <span>Духовые шкафы</span> --}}
                            {{-- <img src="./assets/img/arrow_left.svg" alt=""> --}}
                            {{-- </div>
                        --}}
                        {{-- </div>
                    --}}
                    {{--
                    <div class="hover_img"></div>
                    --}}
                    {{-- </a> --}}
                {{-- <a href="javascript:void(0);" class="our_tehnical__links_item"> --}}
                    {{--
                    <div class="our_tehnical__links_item__wrap"> --}}
                        {{--
                        <div class="our_tehnical__links_item__number"> --}}
                            {{-- <span>20</span> --}}
                            {{-- </div>
                        --}}
                        {{-- <img class="our_tehnical__links_item__img" src="./assets/img/oven.svg" alt=""> --}}
                        {{-- <img class="our_tehnical__links_item__img_active" src="./assets/img/oven_active.svg"
                                  alt=""> --}}
                        {{--
                        <div class="our_tehnical__links_item__descr"> --}}
                            {{-- <span>Духовые шкафы</span> --}}
                            {{-- <img src="./assets/img/arrow_left.svg" alt=""> --}}
                            {{-- </div>
                        --}}
                        {{-- </div>
                    --}}
                    {{--
                    <div class="hover_img"></div>
                    --}}
                    {{-- </a> --}}
                {{-- <a href="javascript:void(0);" class="our_tehnical__links_item"> --}}
                    {{--
                    <div class="our_tehnical__links_item__wrap"> --}}
                        {{--
                        <div class="our_tehnical__links_item__number"> --}}
                            {{-- <span>20</span> --}}
                            {{-- </div>
                        --}}
                        {{-- <img class="our_tehnical__links_item__img" src="./assets/img/oven.svg" alt=""> --}}
                        {{-- <img class="our_tehnical__links_item__img_active" src="./assets/img/oven_active.svg"
                                  alt=""> --}}
                        {{--
                        <div class="our_tehnical__links_item__descr"> --}}
                            {{-- <span>Духовые шкафы</span> --}}
                            {{-- <img src="./assets/img/arrow_left.svg" alt=""> --}}
                            {{-- </div>
                        --}}
                        {{-- </div>
                    --}}
                    {{--
                    <div class="hover_img"></div>
                    --}}
                    {{-- </a> --}}
                {{-- <a href="javascript:void(0);" class="our_tehnical__links_item"> --}}
                    {{--
                    <div class="our_tehnical__links_item__wrap"> --}}
                        {{--
                        <div class="our_tehnical__links_item__number"> --}}
                            {{-- <span>20</span> --}}
                            {{-- </div>
                        --}}
                        {{-- <img class="our_tehnical__links_item__img" src="./assets/img/oven.svg" alt=""> --}}
                        {{-- <img class="our_tehnical__links_item__img_active" src="./assets/img/oven_active.svg"
                                  alt=""> --}}
                        {{--
                        <div class="our_tehnical__links_item__descr"> --}}
                            {{-- <span>Духовые шкафы</span> --}}
                            {{-- <img src="./assets/img/arrow_left.svg" alt=""> --}}
                            {{-- </div>
                        --}}
                        {{-- </div>
                    --}}
                    {{--
                    <div class="hover_img"></div>
                    --}}
                    {{-- </a> --}}
                {{-- <a href="javascript:void(0);" class="our_tehnical__links_item"> --}}
                    {{--
                    <div class="our_tehnical__links_item__wrap"> --}}
                        {{--
                        <div class="our_tehnical__links_item__number"> --}}
                            {{-- <span>20</span> --}}
                            {{-- </div>
                        --}}
                        {{-- <img class="our_tehnical__links_item__img" src="./assets/img/oven.svg" alt=""> --}}
                        {{-- <img class="our_tehnical__links_item__img_active" src="./assets/img/oven_active.svg"
                                  alt=""> --}}
                        {{--
                        <div class="our_tehnical__links_item__descr"> --}}
                            {{-- <span>Духовые шкафы</span> --}}
                            {{-- <img src="./assets/img/arrow_left.svg" alt=""> --}}
                            {{-- </div>
                        --}}
                        {{-- </div>
                    --}}
                    {{--
                    <div class="hover_img"></div>
                    --}}
                    {{-- </a> --}}
                {{-- <a href="javascript:void(0);" class="our_tehnical__links_item"> --}}
                    {{--
                    <div class="our_tehnical__links_item__wrap"> --}}
                        {{--
                        <div class="our_tehnical__links_item__number"> --}}
                            {{-- <span>20</span> --}}
                            {{-- </div>
                        --}}
                        {{-- <img class="our_tehnical__links_item__img" src="./assets/img/oven.svg" alt=""> --}}
                        {{-- <img class="our_tehnical__links_item__img_active" src="./assets/img/oven_active.svg"
                                  alt=""> --}}
                        {{--
                        <div class="our_tehnical__links_item__descr"> --}}
                            {{-- <span>Духовые шкафы</span> --}}
                            {{-- <img src="./assets/img/arrow_left.svg" alt=""> --}}
                            {{-- </div>
                        --}}
                        {{-- </div>
                    --}}
                    {{--
                    <div class="hover_img"></div>
                    --}}
                    {{-- </a> --}}
            </div>
        </div>
    </div>

    <div class="discount wow fadeInUp" data-wow-offset="200">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="discount__descr">
                        <h3>@lang('main.sale')</h3>
                        <h2>@lang('main.sale_percent')</h2>
                        <a href="/outlets/categories/1" class="discount__descr_h4">
                            <h4>@lang('main.sale_desc')</h4>
                        </a>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="discount__img">
                        <img src="./assets/img/slider_bg.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="recommendation_product">
        <div class="container">
            <div class="recommendation_product__title">
                <h3>@lang('main.recommend')</h3>
                <a class="see-all-link" href="{{ route('catalogue') }}">
                    <span>@lang('main.see_all')</span>
                    <img src="./assets/img/arrow_left.svg" alt="">
                </a>
            </div>

            <div id="recommend__products" class="recommendation_product__descr" style="justify-content: space-between;">
                @foreach ($products as $one)
                <a href="{{ route('product_page', $one['id']) }}">
                    <div class="recommendation_product__descr__item  wow slideInUp" data-wow-offset="200">
                        @if (isset($one->discount_price))
                        <div class="recommendation_product__descr__item_icon">
                            <img src="/assets/img/discount_icon.svg" alt="">
                        </div>
                        @endif
                        @if ($one->loan == 1)
                        <div class="recommendation_product__descr__item_icon_2">
                            <img src="/assets/img/ras.svg" alt="">
                        </div>
                        @endif
                        <div
                            class="recommendation_product__descr__item_img {{isset($one->discount_price)? '':'sale_border'}}"
                            style="max-width: 201px;">
                            @foreach (json_decode($one->image) as $image)
                            <img src="{{ asset('storage/' . $image) }}" alt="" style="height: 181px">
                            @break
                            @endforeach
                        </div>

                        <div class="recommendation_product__descr__item_text recommendation_product__normalHeight">
                            <h3>{{ $one->product_name }} {{ $one->colour->color }}</h3>
                            <div class="recommendation_product__descr__item_text__price">
                                @if (!\Illuminate\Support\Facades\Auth::check())
                                @if ($one->discount_price)
                                <h3 class="now_price">{{ number_format($one->discount_price, 0, '', ' ') }} ₸</h3>
                                <h3 class="old_price">{{ number_format($one->price, 0, '', ' ')}} ₸</h3>
                                @elseif(!$one->discount_price)
                                <h3 class="now_price">{{ number_format($one->price, 0, '', ' ') }} ₸</h3>
                                @endif
                                @elseif(\Illuminate\Support\Facades\Auth::check())
                                <h3 class="now_price">{{ number_format($one->partners_price, 0, '', ' ') }} ₸</h3>
                                @endif
                            </div>
                            <div class="recommendation_product__descr__item_text__status">
                                @if ($one->stock > 0)
                                <h3>@lang('main.in_stock')</h3>
                                @else
                                <h3>@lang('main.not_in_stock')</h3>
                                @endif
                            </div>
                        </div>
                        <div>
                            <a href="{{ route('add_cart', $one->id) }}">
                                <button onclick=" addOneFromCart(event, {{$one->id}})"
                                        class="recommendation_product__descr__item_btn" type="submit">
                                    <img class="cart_noactive" src="./assets/img/cart_red.svg" alt="">
                                    <img class="cart_active" src="./assets/img/cart_white.svg" alt="">
                                    <span>@lang('main.in_cart')</span>
                                </button>
                            </a>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>


    <div class="home_bottom wow slideInUp" data-wow-offset="200">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="home_bottom__img">
                        <img src="{{ asset('storage/' . $descriptions->image) }}" alt="">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="home_bottom__descr">
                        <h3>{{ $descriptions->title }}</h3>
                        <p>{!! $descriptions->description !!}</p>
                        <a href="/catalogue">@lang('main.more_info')</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<script>
    const vue = New
    Vue({
        el: "#recommend__products",
        methods: {
            addProductToBasket(event, id) {
                console.log(event, id);
                addOneFromCart(event, id)
            }
        }
    });
</script>


@include('layouts.footer')
