@include('layouts.header')
    <section class="paymentcard">
        <div class="container">
            <h1 order-payment-title class="paymentcard__title">Оплата картой</h1>
            <h3 order-payment-error style="color: red; display: none; font-size: 14px;">Проверьте правильность заполнения полей</h3>
            <div class="paymentcard__wrapper">
                <div id="paymentcard-form" class="paymentcard__inputs">
                    <input validate-payment id="number-card" placeholder="Hомер карты" pattern=".{15,16}" type="text" class="paymentcard__inputs--input">
                    <input validate-payment id="date-card" placeholder="Срок действия карты" type="text" class="paymentcard__inputs--input">
                    <input validate-payment id="cvv-card" placeholder="CVV(на обратной стороне карты)" pattern=".{3,4}" type="number"  class="paymentcard__inputs--input">
                    <input type="submit" class="paymentcard__inputs--submit" id="payment_submit" disabled validate-payment-btn value="Оплатить">
                </div>
            </div>
        </div>
    </section>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="inputmask.js"></script>
    <script>
        
    </script>
    <script>
        const elementSelect = (el) => {
            return document.querySelector(el)
        }
        document.querySelector('#number-card').addEventListener('keydown',function(){
            if(elementSelect("#number-card").value.length >= 19) 
                elementSelect("#payment_submit").removeAttribute('disabled');
            else 
                elementSelect("#payment_submit").setAttribute("disabled","disabled");
        })
        document.querySelector('cvv-card').addEventListener('keydown',function(){
            if(elementSelect("cvv-card").value.length >= 3) 
                elementSelect("#payment_submit").removeAttribute('disabled');
            else 
                elementSelect("#payment_submit").setAttribute("disabled","disabled");
        })
    </script>
@include('layouts.footer')
