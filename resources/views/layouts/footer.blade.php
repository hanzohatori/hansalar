<footer>
    <div class="container">
        <div class="row footer">
            <div class="col-lg-2 col-md-12 footer__wrap">
                <div class="footer__logo">
                    <img src="/assets/img/logo_footer.svg" alt="">
                </div>
            </div>
            <div class="col-lg-7 col-md-8 footer__wrap">
                <div class="footer__descr">
                    <ul>
                        @foreach($categories as $key => $category)
                        @if($key % 2 == 0 )
                        <li><a href="{{ route('shopCategory',$category->id) }}">{{ $category->title }}</a></li>
                        @endif
                        @endforeach
                        {{--
                        <li><a href="javascript:void(0);">Отдельностоящие плиты</a></li>
                        --}}
                        {{--
                        <li><a href="javascript:void(0);">Посудомоечные машины</a></li>
                        --}}
                        {{--
                        <li><a href="javascript:void(0);">Варочные поверхности</a></li>
                        --}}
                    </ul>
                    <ul>
                        @foreach($categories as $key => $category)
                        @if($key % 2 != 0)
                        <li><a href="{{ route('shopCategory',$category->id) }}">{{ $category->title }}</a></li>
                        @endif
                        @endforeach
                        {{--
                        <li><a href="javascript:void(0);">Вытяжки</a></li>
                        --}}
                        {{--
                        <li><a href="javascript:void(0);">Микроволновые печи</a></li>
                        --}}
                        {{--
                        <li><a href="javascript:void(0);">Стиральные машины</a></li>
                        --}}
                        {{--
                        <li><a href="javascript:void(0);">Холодильники</a></li>
                        --}}
                    </ul>
                    <ul>
                        <li><a href="{{ route('client') }}">Клиентам</a></li>
                        <li><a href="{{ route('partner') }}">Партнерам</a></li>
                        <li><a href="{{ route('outlet') }}">Акция/Аутлет</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 footer__wrap">
                <div class="footer__soc">
                    <ul>
                        <li>
                            @foreach($contacts as $contact)
                            <!--                            {{ $contact->phone }}-->
                            <a href="tel:+77089681134">
                                <img src="/assets/img/phone_white.svg" alt="">
                                <span>+7 708 968 11 34</span>
                            </a>
                            @endforeach
                        </li>
                        <li>
                            @foreach($contacts as $contact)
                            <a href="mailTo:test@email.com">
                                <img src="/assets/img/message_white.svg" alt="">
                                <span>{{ $contact->mail }}</span>
                            </a>
                            @endforeach
                        </li>
                        <li>
                            @foreach($contacts as $contact)
                            <a href="javascript:void" style="cursor: auto;">
                                <img src="/assets/img/geo_white.svg" alt="">
                                <span>{{ $contact->address }}</span>
                            </a>
                            @endforeach
                        </li>

                        <li class="footer__soc_list">
                            @foreach($contacts as $contact)
                            <a href="{{ $contact->Instagram }}">
                                <img src="/assets/img/instagram.svg" alt="">
                            </a>

                            <a href="{{ $contact->Vk }}">
                                <img src="/assets/img/vk.svg" alt="">
                            </a>

                            <a href="{{ $contact->Facebook }}">
                                <img src="/assets/img/facebook.svg" alt="">
                            </a>
                            @endforeach
                        </li>
                    </ul>
                    <div class="footer__company">
                        <span class="footer__company--item">Hansa © 2020</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<style>
    .recommendation_product__descr__item_img {

    }


    .sale_border {
        border: 1px solid #b9b9b9 !important;
    }

    .checkout .checkout-left .checkout-medium .checkout-content .checkout-num::before {
        height: 100% !important;
    }

    .client__wrapper__payment__item p, .partners__descr__item p, .client__wrapper__delivery p, .client__wrapper__map p, .client__wrapper__return p, .partners__descr__text p {
        font-size: 12px !important;
        line-height: 120% !important;
    }

    header .header_main__cart_block__bottom_btn, header .header_main__cart_block__bottom_result__price h3, header .header_main__cart_block__bottom_result__price span, .client__wrapper__delivery__wrap span, header .header_main__cart_block__item__descr_content .count, header .header_main__cart_block__item__descr_content .price, .client__wrapper__return__stage span, header .header_main__cart_block__item__descr h3 {
        font-size: 12px !important;
        line-height: 120% !important;
    }

    .client__wrapper__payment__item h3, .header_main__cart_block__title_h3, .partners__descr__text span, .partners__descr__item h3, .client__wrapper__return__stage h3, .partners__descr__text h3 {
        font-size: 14px !important;
        line-height: 120% !important;
    }

    .client__wrapper__payment h2, .client__wrapper__map h2, .client__wrapper__return h2 {
        font-size: 16px !important;
        line-height: 120% !important;
    }

    .catalog__title h3 {
        margin-left: 0 !important;
        font-size: 23px !important;
    }

    .catalog__title_h3 {
        font-size: 23px !important;
    }

    .catalog__title span {
        margin-right: 10px !important;
    }

    .partners.catalog {
        margin-left: 0px !important;
        margin-right: 0px !important;
    }


    header .header_main__cart_block {
        width: 320px !important;
    }

    .catalog .categories_filter__links__list li a h3 {
        line-height: 120% !important;
    }

    .basket {
        padding-top: 70px !important;
    }

    .catalog__title_filter .filter__pricies--item h3 {
        font-size: 14px !important;
    }

    .tab_content.product_inner__descr__content_tabs__content__character h2 {
        padding-bottom: 0 !important;
    }

    header .header_main__cart_block__item__img img {
        width: 40px !important;
        height: 40px !important;
        object-fit: contain !important;
    }

    ul.product_inner__descr__content_info__item_first {
        width: 50%;
        min-width: 50%;
    }

    .client__wrapper__map__select::before {
        top: 50% !important;
        transform: translateY(-50%) !important;
        background-size: contain !important;
        background-position: center !important;
    }


    .checkout .checkout-left .checkout-medium .checkout-content .checkout-num span {
        top: calc(50%) !important;
        transform: translateY(-50%) !important;
        background-size: contain !important;
        background-position: center !important;
        font-size: 16px;
        padding-bottom: 0px;
        left: 13px;
    }


    header .header_top__find img {
        top: 50% !important;
        transform: translateY(-50%) !important;
    }


    .cart-product {
        padding-bottom: 28px;
    }

    .checkout .checkout-left .checkout-medium .basket-content .checkout-select::before {
        content: '';
        background-image: url(../../assets/img/arrow.png);
        background-repeat: no-repeat;
        width: 1.38889vw;
        height: 0.69444vw;
        display: block;
        position: absolute;
        top: 1.52778vw;
        right: 1.38889vw;
        display: none !important;
    }

    .phoe_bottom {
        margin-bottom: 1.04167vw;
        position: relative;
    }

    .phoe_bottom:before {
        height: 100% !important;
    }

    .phoe_bottom input {
        margin-bottom: 0 !important;
    }

    .select_sconte {
        position: relative;
    }

    .select_sconte::before {
        content: '';
        background-image: url(../../assets/img/arrow.png);
        background-repeat: no-repeat;
        width: 15px;
        height: 15px;
        display: block;
        position: absolute;
        top: 50%;
        transform: translateY(-50%);
        right: 1.38889vw;
        background-size: contain;
        background-position: center center;
    }

    .our_tehnical__links_item .our_tehnical__links_item__img {
        transition: 0.3s;
        transform: scale(1);
    }

    .our_tehnical__links_item:hover .our_tehnical__links_item__img {
        display: block !important;
        transform: scale(1.1);
    }

    .our_tehnical__links_item:hover .our_tehnical__links_item__img_active {
        display: none !important;
    }

    .checkout .checkout-right .order-inner .order-content p {
        font-size: 14px !important;
    }

    .checkout .checkout-left .checkout-top ul .nav-item .nav-link {
        font-size: 14px !important;
        line-height: 120% !important;
        margin-right: 14px;
    }

    .checkout .checkout-left .checkout-top ul .nav-item {
        width: auto !important;
        height: auto !important;
    }

    .checkout .checkout-right .checkout-image img {
        width: 50px !important;
        height: 50px !important;
    }

    .checkout .checkout-right .checkout-image {
        width: 90px !important;
        height: 90px !important;
    }

    /*@media (min-width: 767px) {*/
    .basket-content .discount_price {
        margin-top: 0;
    }

    .checkout .checkout-left .checkout-pay .pay-method .catalog_label {
        min-width: auto !important;
    }

    .checkout .checkout-right-bottom .basket-price-inner .price-left p {
        font-size: 14px !important;
    }

    .checkout .checkout-left .checkout-medium .basket-content select {
        height: 47px !important;
    }

    .checkout .checkout-right .order-inner {
        width: 85% !important;
        margin-left: auto !important;
    }

    .checkout .checkout-right-bottom .basket-price-inner .price-right span {
        font-size: 14px !important;
    }

    .checkout .checkout-left .checkout-top ul {
        height: auto !important;
        padding: 24px 0 !important;
    }

    .image-container img {
        object-fit: contain !important;
    }

    .cart-product > div {
        display: flex;
        align-items: center;
    }

    .checkout .checkout-left .checkout-medium .checkout-content input {
        padding-bottom: 0 !important;
    }

    .product_inner__descr__content_info__item ul li {
        height: 35px !important;
        justify-content: center;
    }

    .left_text .product_inner__descr__content_info__item ul li {
        justify-content: flex-start !important;
    }

    .basket .basket-content .product-delete {
        margin-top: 0 !important;
    }

    .basket .basket-content .product-name {
        height: auto !important;
        margin-top: 0 !important;
    }

    .basket .basket-content .product-name h2 {
        margin-bottom: auto !important;
    }

    .basket .basket-content .product-text-1 h2 {
        min-width: auto !important;
    }

    .basket .basket-content .product-text-1 {
        margin-left: auto !important;
        margin-right: auto !important;
    }

    .basket-content .cart-product {

    }

    .basket-content .product-name.product-text.product-count {
        margin-top: 0;
    }

    @media (min-width: 1000px) {
        /*.basket .basket-content .product-name {*/
        /*    display: flex;*/
        /*    align-items: center;*/
        /*}*/
        .product-name.product-text.product-price.product-with-discount {
            left: 18px;
            position: relative;
        }

        .catalog .categories_filter__links__list li a {
            min-height: 46px !important;
        }
        .catalog .categories_filter__links__list li a span{
            padding-top: 5px !important;
        }

    }

    @media (max-width: 767px) {
        .checkout .checkout-left .checkout-pay .pay-method .catalog_label h2 {
            width: 10% !important;
            flex-grow: 2;
        }

        .basket .basket-content .product-count {
            position: relative !important;
            right: 20px !important;
        }

        .product-name.product-text.product-price.product-with-discount {
            position: relative;
            right: -24px;
        }

        .catalog__title span {

        }

        .our_tehnical__links_item__number span {
            font-size: 14px !important;
        }

        .our_tehnical__links_item__number {
            height: auto !important;
            padding: 3px 0 !important;
        }

        .cart-product > div {
            align-self: normal !important;;
            padding: 10px 0 !important;
        }

        #vue-cart .product-name h3 {
            margin-top: 0 !important;
        }

        .catalog h3.catalog__title_h3 {
            width: 100%;
        }

        .cart .product-name .discount_price {
            margin-top: 4.4vw !important;
        }

        .select_sconte::before {
            right: 15px !important;
        }

        header .header_top__find img {
            top: 50% !important;
            transform: translateY(-50%) !important;
        }

        .catalog__title_h3 {
            font-size: 14px !important;
        }

        .partners__title {
            flex-direction: row !important;
            justify-content: space-between;
            align-items: center;
        }

        .catalog__title {
            flex-wrap: wrap;
        }

        .catalog__title h3 {
            margin-left: 0 !important;
            font-size: 14px !important;
        }

        .checkout .checkout-left .checkout-medium .checkout-content .checkout-num span {
            top: calc(50%) !important;
            font-size: 16px !important;
        }

        header .header_main__lang a {
            padding: 9px 0px !important;
            margin: 0 10px !important;
        }

        .catalog__title a {
            margin-bottom: 10px !important;
        }

        .catalog__title span {
            margin-bottom: 0px !important;
        }

        .our_tehnical__links_item__number span {
            line-height: 100%;
        }

        header .header_main__lang {
            margin-left: -10px !important;
        }

        .splide__arrow--prev {
            left: 0 !important;
        }

        .splide__arrow--next {
            right: 0 !important;
        }

        .partners .row {
            margin-right: 0 !important;
            margin-left: 0 !important;
        }

        .partners__descr__item {
            padding-left: 0 !important;
        }

        .partners__title .partners__title_btn {
            margin-left: 0px !important;
            margin-top: 0px !important;
        }


        .partners .partners__form_wrap .catalog__title {
            margin-top: 40px !important;
        }

        .catalog__title_h3 + span {
            margin-bottom: 7px !important;
        }
    }
</style>
<script src="https://unpkg.com/imask"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.9.0/slick/slick.min.js"></script>
<script src="{{asset('/dist/js/main.js')}}"></script>
</body>
</html>
