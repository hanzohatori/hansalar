<!DOCTYPE html>
<html id="html" lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
    <link rel="stylesheet" href="{{ asset('/dist/css/main.css') }}">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>
    <link rel="stylesheet" href="{{ asset('/dist/css/splide.min.css') }}">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/> -->
    {{--
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.9.0/slick/slick.css"/>
    --}}
    {{--
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/gh/kenwheeler/slick@1.9.0/slick/slick-theme.css"/>
    --}}
    <script src="https://cdn.jsdelivr.net/npm/vue@2/dist/vue.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script src="{{ asset('/dist/js/lottie.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="{{ asset('/dist/css/animate.css') }}"/>
    <script src="{{ asset('/dist/js/wow.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.21.1/axios.min.js"
            integrity="sha512-bZS47S7sPOxkjU/4Bt0zrhEtWx0y0CRkhEp8IckzK+ltifIIE9EMIMTuT/mEzoIMewUINruDBIR/jJnbguonqQ=="
            crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <style>
        .header_main__cart {
            opacity: 0;
        }

        .header_main__cart.active {
            opacity: 1;
        }
    </style>
    <script>
        new WOW().init();
    </script>
    <title>Hansa</title>
</head>
<body>
<div id="main_loader" class="main_loader">
    <div id="hansa_loader"></div>
</div>

<script>
    document.getElementById('main_loader').style.display = "none";
    if (document.location.pathname === "/" || document.location.pathname === "/page") {
        document.getElementById('main_loader').style.display = "block";
        setTimeout(() => {
            document.getElementById('main_loader').style.opacity = 0;
        }, 3000)
        setTimeout(() => {
            document.getElementById('main_loader').style.display = "none";
        }, 3300)
    }

</script>

<script type="text/javascript">
    var animData = {
        wrapper: document.getElementById('hansa_loader'),
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: '/dist/js/hansa_loader_transparent.json'

    };
    var anim = bodymovin.loadAnimation(animData);

</script>
<section class="modal-categories mobile-only hidden">
    <div class="modal-background"></div>
    <div class="modal-main">
        <h1 class="modal-title">Категории</h1>
        <ul>
            @foreach($categories as $category)
            <li><a href="{{ route('shopCategory',$category->id) }}">{{ $category->title }}</a></li>
            @endforeach
        </ul>
    </div>
</section>
<header>
    <div class="header-top-main">
        <div class="header_top container">
            <div class="header_main__lang" style="margin-left: 0px!important;">
                <a id="kzlang" style="padding: 9px 0px!important; margin: 0 5px !important;"
                   href="{{ route('locale', 'kz') }}" @if( Config::get('app.locale') =='kz')
                class="header_main__lang_active" @endif>Каз</a>
<!--                {{ session('locale') }}-->
                <a id="rulang" style="padding: 9px 0px!important; margin: 0 5px !important;"
                   href="{{ route('locale', 'ru') }}" @if( Config::get('app.locale') == 'ru')
                class="header_main__lang_active" @endif>Рус</a>
            </div>
            <div class="header_top__find">
                <img src="/assets/img/find.svg" alt="">
                <input class="search_input" type="search" name="product_name">

            </div>
        </div>
    </div>
    <div class="header_main container">
        <a href="{{ route('index') }}" class="header_main__logo">
            <!-- <img src="/assets/img/logo.svg" alt="" class="wow fadeInDown" data-wow-duration="2s" data-wow-delay="2s"> -->
            <div id="logo_hansa"></div>
        </a>

        <script type="text/javascript">
            var animData = {
                wrapper: document.getElementById('logo_hansa'),
                renderer: 'svg',
                loop: true,
                autoplay: true,
                path: '/dist/js/logo.json'

            };
            var anim = bodymovin.loadAnimation(animData);
        </script>
        <div class="header_main__menu desktop-only">
            <a href="{{ route('catalogue') }}" class="header_main__menu_link">
                <img src="/assets/img/menu.svg" alt="">
                <span>Каталог</span>
            </a>
            <div class="catalog__links">
                <div class="catalog__links_item">
                    <ul>
                        @foreach($categories as $category)

                        <li><span></span><a
                                href="{{ route('shopCategory',$category->id) }}">{{ $category->title }}</a></li>
                        @if($category->id == 5)
                        @break
                        @endif
                        @endforeach
                    </ul>
                    <ul>
                        @foreach($categories as $category)
                        @if($category->id > 5)
                        <li><span></span><a
                                href="{{ route('shopCategory',$category->id) }}">{{ $category->title }}</a></li>
                        @endif
                        @endforeach

                    </ul>
                </div>
            </div>
        </div>
        <div class="header_main__menu mobile-only" id="open-categories">
            <a href="{{ route('catalogue') }}" class="header_main__menu_link">
                <img src="/assets/img/menu.svg" alt="">
                <span>Каталог</span>
            </a>
        </div>
        <div class="header_top__geo">
            <div class="cityname__wrap">
                <span>@lang('main.city'):</span>
                <h3 class="cityname">{{ Session::get('city') ? : 'Алматы' }}</h3>
            </div>
            <div class="cityname_list"
                 style="max-height: 350px ; flex-direction: column; flex-wrap: wrap; min-width: {{170*ceil(count($banks)/9)}}">
                @foreach ($banks as $city)
                <a href="{{ route('city', $city->id) }}">
                    <div class="cityname_list__item point">
                        {{ $city->city }}
                        <input type="hidden" value="{{ $city->city }}" name="city">
                        {{--/city/$city->id--}}
                        {{--/city/2--}}
                    </div>
                </a>
                {{--Str::slug('city')--}}

                @endforeach
            </div>

            {{--
            <div class="cityname_list">
                <div class="cityname_list__item point">Алматы</div>
                <div class="cityname_list__item point">Астана</div>
                <div class="cityname_list__item point">Павлодар</div>
            </div>
            --}}
        </div>
        {{--
        <div class="header_main__phone">
            <div class="header_main__phone_img">
                <img src="./assets/img/phone.svg" alt="">
            </div>
        </div>
        --}}
        <a href="{{ route('outlet') }}" class="header_main__discount">
            <img src="/assets/img/discount.svg" alt="">
            <span>Акция/Аутлет</span>
        </a>
        <a href="{{ route('client') }}" class="header_main__links">
            <span>@lang('main.clients')</span>
        </a>
        <div class="header_main__links">
            <a href="{{ route('personal-data') }}" style="display:flex; align-items:center; text-decoration: none;">
                <img src="/assets/img/user.svg" alt="">
            </a>
            <div style="display: flex; flex-direction: column; padding-left: 5px;">
                <a href="{{ route('personal-data') }}" style="display:flex; text-decoration: none;">
                    <span style="padding-bottom: 0px;">@lang('main.partners')</span>
                </a>
                @if(\Illuminate\Support\Facades\Auth::check())
                <form action="{{ route('logout') }}" method="POST" id="logout-form">
                    @csrf
                    {{-- <a href="{{ url('logout') }}" --}}
                            {{-- style="display: flex; color: #ccc; font-size: 13px; text-transform: capitalize;">@lang('main.exit')</a>--}}
                    <a style="display: flex; color: #ccc; font-size: 13px; text-decoration: none; text-transform: capitalize;"
                       href="{{ route('logout') }}"
                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                        @lang('main.exit')
                    </a>

                </form>
                @endif
            </div>
        </div>
        <div class="header_main__cart" id="cart-header">
            <a href="{{ route('cart') }}" class="header_main__cart_flex">
                <div class="header_main__cart_img">
                    <img src="/assets/img/cart.svg" alt="">
                    <h5 v-if="totalQuantity">
                        @{{ totalQuantity }}
                    </h5>
                </div>
                <div class="header_main__cart_descr">
                    <h3>@lang('main.cart')</h3>
                    <span v-if="totalSum">@{{ parseInt(totalSum).toLocaleString('ru') }} ₸</span>
                </div>
            </a>
            <div class="header_main__cart_block" v-if="cart && cart.length > 0">
                <div class="header_main__cart_block__title">
                    <h3 class="header_main__cart_block__title_h3">@lang('main.your_cart')</h3>
                </div>
                <a v-for="product in cart" :href="`/product/about/${product.id}`">
                    <div class="header_main__cart_block__item">
                        <div class="header_main__cart_block__item__img">
                            <img v-bind:src="`/storage/${JSON.parse(product.image)[0]}`" alt="">

                        </div>
                        <div class="header_main__cart_block__item__descr">

                            <h3>@{{ product.product_name }} @{{product.colour.color}}</h3>
                            <div class="header_main__cart_block__item__descr_content">
                                <span class="count">@{{ product.quantity }} шт</span>
                                @if(\Illuminate\Support\Facades\Auth::check())
                                <span
                                    class="price">@{{ parseInt(product.partners_price).toLocaleString('ru') }} ₸</span>
                                @endif
                                @if(!\Illuminate\Support\Facades\Auth::check())
                                <span class="price"
                                      v-if="product.discount_price">@{{ parseInt(product.discount_price).toLocaleString('ru') }} ₸</span>
                                <span class="price" v-else>@{{ parseInt(product.price).toLocaleString('ru') }} ₸</span>
                                @endif
                            </div>
                        </div>
                    </div>
                </a>
                <div class="header_main__cart_block__bottom">
                    <div class="header_main__cart_block__bottom_result">

                        <div class="header_main__cart_block__bottom_result__product">
                            <div class="header_main__cart_block__bottom_result__price">
                                <h3>На сумму:&nbsp;</h3><span>@{{ parseInt(totalSum).toLocaleString('ru') }} ₸</span>
                            </div>

                        </div>
                        <a href="{{ route('order') }}" class="header_main__cart_block__bottom_btn">@lang('main.submit_order')</a>
                        {{--
                        <form action="{{ route('order') }}" method="POST" ref="submitProductForm">
                            @csrf
                            <input type="hidden" name="product" :value="submitProduct">
                            <input class="header_main__cart_block__bottom_btn" type="submit" v-on:click="submitForm"
                                   value="{{ trans('main.submit_order') }}">
                        </form>
                        --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="burger-menu">
            <a href="#" class="burger-menu-btn">
                <span class="burger-menu-lines"></span>
            </a>
        </div>
        <input type="hidden" name="category_id" id="category_id" value="{{isset($id) ? $id :''}}">
        <input type="hidden" name="user_id"
               id="user_id"
               value="{{\Illuminate\Support\Facades\Auth::user() ? \Illuminate\Support\Facades\Auth::user()->id:null}}">
</header>
<div class="nav-panel-mobil">
    <div class="container">
        <nav class="navbar-expand-lg navbar-light">
            <ul class="navbar-nav navbar-mobile">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('index')}}">@lang('main.main')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('outlet') }}">Акция/Аутлет</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('client') }}">@lang('main.clients')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('partner') }}">@lang('main.partners')</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-link-lang nav-link-active" href="javascript:void(0);"
                       class="header_top__find_lang__active">ru</a>
                    <a class="nav-link nav-link-lang" href="javascript:void(0);">kz</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('cart') }}">@lang('main.cart')</a>
                </li>
            </ul>
        </nav>
    </div>
</div>

<script>
    const vueCart = new Vue({
        el: '#cart-header',
        data: {
            cart: null,
            totalSum: null,
            totalQuantity: null,
            currentProduct: null,
            products: {
                cart: [],
                promocode: null,
                totalQuantity: 0,
                totalSum: 0
            },
            submitProduct: null
        },
        beforeMount() {

            /*axios.get('/cart/get').then(res => {
                this.cart = res.data.cart;
                this.totalSum = res.data.totalSum;
                this.totalQuantity = res.data.totalQuantity;
            })*/
            if (JSON.parse(localStorage.getItem('products'))) {
                this.products = JSON.parse(localStorage.getItem('products'));
                if (this.products) {
                    this.cart = this.products.cart;
                    this.totalSum = this.products.totalSum;
                    this.totalQuantity = this.products.totalQuantity;
                }
            } else {
                if (this.products) {
                    this.cart = this.products.cart;
                    this.totalSum = this.products.totalSum;
                    this.totalQuantity = this.products.totalQuantity;
                }
            }

            setInterval(function () {
                document.querySelector(".header_main__cart").classList.add("active");
                console.log("Update");
            }, 1000);

        },
        methods: {
            submitForm() {
                this.submitProduct = localStorage.getItem('products');
                this.$refs.submitProductForm.submit();
            },
            renderCart() {
                /* axios.get('/cart/get').then(res => {
                     if(res.data.cart && res.data.cart.length === 0) {
                       this.cart = res.data.cart;
                     } else {
                       this.cart = res.data.cart;
                     }
                     this.totalSum = res.data.totalSum;
                     this.totalQuantity = res.data.totalQuantity;
                 }); */
                this.products = JSON.parse(localStorage.getItem('products'));
                if (this.products.cart && this.products.cart.length === 0) {
                    this.cart = this.products.cart;
                } else {
                    this.cart = this.products.cart;
                }

                this.totalSum = this.products.totalSum;
                this.totalQuantity = this.products.totalQuantity;


            },

            addOneFromCart(event, id) {
                event.preventDefault();
                /*axios.get('cart/add/' + id).then(res => {
                    this.cart = res.data.cart;
                    this.totalQuantity = res.data.totalQuantity;
                    this.totalSum = res.data.totalSum;
                    renderCart();
                }); */

                axios.get(`/cart/add/products/${id}`).then(res => { // ?count=this.countths


                    this.currentProduct = JSON.parse(res.data.current_product);
                    this.currentProduct.quantity = 0;

                    if (this.products.cart.some(el => el.id === this.currentProduct.id)) {
                        let currentElement = {};
                        this.products.cart.forEach(el => {
                            if (el.id === this.currentProduct.id) {
                                currentElement = el;
                            }
                        });
                        let idCart = this.products.cart.indexOf(currentElement);
                        this.products.cart[idCart].quantity++;
                        this.products.totalQuantity++;
                        let discount_price = this.currentProduct.discount_price;
                        discount_price ? this.products.totalSum += Number(this.currentProduct.discount_price) :
                            this.products.totalSum += Number(this.currentProduct.price);

                        let products_c = this.products;
                        localStorage.setItem('products', JSON.stringify(products_c));
                        renderCart();
                    } else {
                        this.products.cart.push(this.currentProduct);
                        let idCart = this.products.cart.indexOf(this.currentProduct)
                        this.products.cart[idCart].quantity += 1;
                        this.products.totalQuantity += 1;
                        let discount_price = this.currentProduct.discount_price;
                        discount_price ? this.products.totalSum += Number(this.currentProduct.discount_price) :
                            this.products.totalSum += Number(this.currentProduct.price);

                        let products_c = this.products;
                        localStorage.setItem('products', JSON.stringify(products_c));
                        renderCart();
                    }
                });

            },
        }
    })

    function renderCart() {
        vueCart.renderCart();
    };

    function addOneFromCart(event, id) {
        vueCart.addOneFromCart(event, id);
    };

</script>

@yield('content')

