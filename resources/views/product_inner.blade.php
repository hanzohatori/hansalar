@include('layouts.header')
<section class="product_inner catalog left_text" id="product-vue">
    <div class="container">
        <!-- <div class="breadscrumb">
      <ul>
        <li><a href="javascript:void(0);">Главная</a></li>
        <li><a>&nbsp;/&nbsp;</a></li>
        <li><a href="javascript:void(0);">Карточка товара</a></li>
      </ul>
    </div> -->
    <div class="catalog__title">
        <a href="/catalogue/categories/{{$products['shop_category_id']}}" class="catalog__title_h3">{{$categories[$products['shop_category_id'] - 1]['title']}}</a> <span>></span>
        <h3 class="catalog__title_h3">{{ $products->product_name }}</h3>
    </div>
    <div class="product_inner__descr">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="product_inner__descr_img">
                        <div class="product_inner__descr_img__item splide" id="splide-primary">
                            <div class="splide__track">
                                <div class="splide__list">
                                    @foreach(json_decode($products->image) as $image)
                                        <div class="slider-image-container splide__slide">
                                            <img src="{{ asset('storage/'.$image) }}"
                                            alt="" class="slider-inner-image">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="product_inner__descr_img__item_nav" id="splide-nav">
                            <div class="splide__track">
                                <div class="splide__list">
                                    @foreach(json_decode($products->image) as $image)
                                        <div class="product_inner__descr_img__item_nav__item splide__slide">
                                            <img src="{{ asset('storage/'.$image) }}" alt="">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-12 col-sm-12">
                    <div class="product_inner__descr__content">
                        <div class="product_inner__descr__content_title">
                            <h3>{!! $products->product_name !!}</h3>
                        </div>
                        <p>{!! $products->description !!}</p>
                        <div class="product_inner__descr__content_info">
                            <div class="product_inner__descr__content_info__item">
                                <ul class="product_inner__descr__content_info__item_first">
                                    <li>К/н:</li>
                                    <li>Категория:</li>
                                    <li>@lang('main.stock'):</li>
                                    <li>Гарантия:</li>
                                </ul>
                                <ul>
                                    <li>{!! $products->knumber !!}</li>
                                    <li style="text-decoration: underline;">{!! $products->category->title !!}</li>
                                    @if($products->stock > 0)

                                        <li style="text-decoration: underline; color: #6A983C;">@foreach($products->cities as $one) {!! $one->city !!} @endforeach</li>

                                    @else
                                        <li style="text-decoration: underline; color: #6A983C;">@lang('main.not_in_stock')</li>
                                    @endif
                                    <li>{!! $products->guarantee !!}</li>
                                </ul>
                            </div>
                            <div class="product_inner__descr__content_info__item">
                                <ul class="product_inner__descr__content_info__item_first">
                                    <li>@lang('main.year'):</li>
                                    <li>@lang('main.volume'):</li>
                                    <li>@lang('main.delivery'):</li>
                                    <li>@lang('main.origin'):</li>
                                </ul>
                                <ul>
                                    <li>{!! $products->year !!}</li>
                                    <li>{!! $products->dimension !!}</li>
                                    <li>{{ $products->delivery }}</li>
                                    <li>{!! $products->origin !!}</li>
                                </ul>
                            </div>

                        </div>
                        <div class="product_inner__descr__content_count">
                            @if(!\Illuminate\Support\Facades\Auth::check())
                                    <h3 class="now_price"
                                        v-if="product && product.discount_price"
                                        style="color: rgba(213, 0, 50, 1)"> {!! number_format($products->discount_price, 0, '', ' ') !!} ₸</h3>

                                    <div v-if="product && product.discount_price" class="product_inner__descr__content_count__price">
                                        <h3 class="old_price"> {!! number_format($products->price, 0, '', ' ') !!} ₸</h3>
                                    </div>
                                    <h3 v-if="product != null && !(product && product.discount_price)" class="now_price" style="color: rgba(213, 0, 50, 1)">{!!  number_format($products->price, 0, '', ' ') !!}
                                        ₸</h3>
                            @elseif(\Illuminate\Support\Facades\Auth::check())
                            <h3 class="discount__price--item">{{ number_format($products->price, 0, '', ' ')}} ₸</h3>
                                <h3 class="now_price"
                                    style="color: rgba(213, 0, 50, 1)"> {!!  number_format($products->partners_price, 0, '', ' ') !!} ₸</h3>
                            @endif
                            <div class="product_inner__descr__content_count__price">

                            </div>
                            @php $basket = session()->get('cart'); @endphp

                                <div class="product_inner__descr__content_count__select">
                                    <div class="product_inner__descr__content_count__select__counter">
                                        <a href="#" v-on:click="count <= 1 ? count = 1 : count--; $event.preventDefault();"
                                           class="product_inner__descr__content_count__select__counter_btns minus">-</a>
                                        <span>
                                        @{{count}}
                                    </span>
                                        <a href="#" v-on:click="$event.preventDefault(); count++;"
                                           class="product_inner__descr__content_count__select__counter_btns plus">+</a>
                                    </div>
                                </div>


                                <a href="#" v-on:click="addProduct($event, product.id)"
                                    class="product_inner__descr__content_count__btn">
                                    <img src="/assets/img/cart_white.svg" alt="">
                                    <span>@lang('main.in_cart')</span>
                                </a>

                        </div>
                        <div class="product_inner__descr__content_favorites">
                            <a class="product_inner__descr__content_favorites__item">
                                {{--                                <img src="/assets/img/favorite.svg" alt="">--}}
                                @if($products->discount_price)
                                    <span style="color: rgba(213, 0, 50, 1)">@lang('main.prod_loan')</span>
                                @endif
                            </a>
                            <a class="product_inner__descr__content_favorites__item">
                                {{--                                <img src="/assets/img/share.svg" alt="">--}}
                                @if($products->loan)
                                    <span style="color: rgba(213, 0, 50, 1)">Доступно в рассрочку</span>
                                @endif
                            </a>
                        </div>
                        <div class="product_inner__descr__content_tabs">
                            <div class="product_inner__descr__content_tabs__head">
                                <a class="tab_link product_inner__descr__content_tabs__head_active"
                                   href="javascript:void(0);">
                                    @lang('main.about')
                                </a>
                                <a class="tab_link" href="javascript:void(0);">
                                    @lang('main.reviews')
                                    {{--                                    <span>52</span>--}}
                                </a>
                                <a class="tab_link" href="javascript:void(0);">
                                    @lang('main.characteristics')
                                </a>
                            </div>
                            <div class="product_inner__descr__content_tabs__content">
                                <div class="tab_content product_inner__descr__content_tabs__content__about"
                                     style="display: block">
                                    <h3>{!! $products->about_product_title !!} </h3>
                                    <p>
                                    {!! strip_tags($products->about_product_description) !!}
                                    </p>
                                    {{--                                                                        <h3>Стабильная температура</h3>--}}
                                    {{--                                                                        <p>Благодаря стабильной и равномерной температуре по всей духовке Ваша хрупкая--}}
                                    {{--                                                                            выпечка и суфле будут готовиться идеально.</p>--}}
                                </div>
                                <div class="tab_content product_inner__descr__content_tabs__content__about"
                                     style="display: none">
                                    <h3>{!! $products->review !!} </h3>
                                    {{--                                    <p>{!! strip_tags($products->about_product_description) !!} </p>--}}
                                    {{--                                                                        <h3>Стабильная температура</h3>--}}
                                    {{--                                                                        <p>Благодаря стабильной и равномерной температуре по всей духовке Ваша хрупкая--}}
                                    {{--                                                                            выпечка и суфле будут готовиться идеально.</p>--}}
                                </div>
                                <div class="tab_content product_inner__descr__content_tabs__content__character"
                                     style="display: none">
                                    <h2>@lang('main.characteristics') {!! $products->category->title !!}
                                        {!! $products->product_name !!} {!! $products->control !!} </h2>
<!--                                    <div class="product_inner__descr__content_tabs__content__character__table">-->
<!--                                    </div>-->
                                    <div class="product_inner__descr__content_tabs__content__character__table gray">
                                        <span>@lang('main.inner_volume')</span><span>{!! $products->volume !!}</span>
                                    </div>
                                    <div class="product_inner__descr__content_tabs__content__character__table">
                                        <span>@lang('main.control_type')</span><span>{!! $products->control !!}</span>
                                    </div>
                                    <div class="product_inner__descr__content_tabs__content__character__table gray">
                                        <span>@lang('main.switch_type')</span><span>{!! $products->switch_type !!}</span>
                                    </div>
                                    <div class="product_inner__descr__content_tabs__content__character__table">
                                        <span>@lang('main.prog_quant')</span><span>{!! $products->prog_quantity !!}</span>
                                    </div>
                                    <div class="product_inner__descr__content_tabs__content__character__table gray">
                                        <span>@lang('main.origin_color')</span><span>{!! $products->colour->color !!}</span>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="product_inner__bottom">
        @if(!empty($products->similar))
        <div class="product_inner__bottom__title">
            <h3>@lang('main.alike')</h3>
            <a href="{{ route('catalogue') }}">@lang('main.more_alike') <img src="/assets/img/product_right.svg"
                                                                             alt=""></a>
        </div>
        @endif
        <div class="product_inner__bottom__descr">
            <div class="recommendation_product__descr" style="justify-content: space-between;">
                @foreach($products->similar as $one)
                    <a href="{{ route('product_page',$one->id) }}">
                        <div class="recommendation_product__descr__item">
                            @if(isset($one->discount_price))
                                <div class="recommendation_product__descr__item_icon">
                                    <img src="/assets/img/discount_icon.svg" alt="">
                                </div>
                            @endif

                            <div class="recommendation_product__descr__item_img {{isset($one->discount_price)? '':'sale_border'}}" style="max-width: 201px;">
                                @foreach(json_decode($one->image) as $image)
                                    <img src="{{ asset('storage/'.$image) }}" alt=""
                                         style="height: 180px; object-fit: contain;">
                                    @break
                                @endforeach
                            </div>
                            <div class="recommendation_product__descr__item_text recommendation_product__normalHeight">
                                {{--                            <span>СП-00032943</span>--}}
                                <h3>{{ $one->product_name }} {{ $one->colour->color }}</h3>
                                <div class="recommendation_product__descr__item_text__price">
                                    @if(!\Illuminate\Support\Facades\Auth::check())
                                        @if($one->discount_price)
                                            <h3 class="now_price">{{ number_format($one->discount_price, 0, '', ' ')}} ₸</h3>
                                            <h3 class="old_price">{{ number_format($one->price, 0, '', ' ')}} ₸</h3>
                                        @elseif(!$one->discount_price)
                                            <h3 class="now_price">{{ number_format($one->price, 0, '', ' ')}} ₸</h3>
                                        @endif
                                    @elseif(\Illuminate\Support\Facades\Auth::check())
                                        <h3 class="now_price">{{number_format($one->partners_price, 0, '', ' ')}} ₸</h3>
                                    @endif
                                </div>
                                <div class="recommendation_product__descr__item_text__status">
                                    @if($one->stock > 0)
                                        <h3>@lang('main.in_stock')</h3>
                                    @else
                                        <h3>@lang('main.not_in_stock')</h3>
                                    @endif
                                </div>
                            </div>
                            <div>
                                <a href="{{ route('add_cart', $one->id) }}">
                                    <button onclick=" addOneFromCart(event, {{$one->id}})" class="recommendation_product__descr__item_btn" type="submit">
                                        <span>@lang('main.in_cart')</span>
                                    </button>
                                </a>
                            </div>
                        </div>
                    </a>
                @endforeach
            </div>
        </div>
    </div>
    </div>
</section>
<script>
  const productVue = new Vue({
    el: '#product-vue',
    data: {
      product: null,
      cart: null,
      count: 1,
      currentProduct: null,
      products: {
        cart: [],
        promocode: null,
        totalQuantity: 0,
        totalSum: 0
      }
    },
    beforeMount() {
        axios.get('/product/info/' + {{$products->id}}).then(res => {
            this.product = res.data.product;
        });
        /*axios.get('/cart/get/').then(res => {
            this.cart = res.data.cart;
            console.log(this.cart);
        });*/

        if(!localStorage.getItem('products')){
            localStorage.setItem('products', JSON.stringify(this.products));
        } else {
            this.products = JSON.parse(localStorage.getItem('products'));
        }

    },

    computed: {
        cartProduct() {
            if(this.cart && this.cart.filter(el => el.id == {{$products->id}}).length) {
                return this.cart.filter(el => el.id == {{$products->id}})[0];
            }
        }
    },
    methods: {

        addProduct(event, id) {
            event.preventDefault();
            this.beforePushProducts();
            axios.get(`/cart/add/products/${id}` ).then(res => { // ?count=this.countths
                this.currentProduct = JSON.parse(res.data.current_product);
                this.currentProduct.quantity = 0;
                if(this.products.cart.some(el => el.id === this.currentProduct.id)){
                    let currentElement = {};
                    this.products.cart.forEach(el => {
                        if(el.id === this.currentProduct.id){
                            currentElement = el;
                        }
                    });
                    let idCart = this.products.cart.indexOf(currentElement);
                    this.products.cart[idCart].quantity += this.count;
                    this.products.totalQuantity += this.count;
                    let discount_price = this.currentProduct.discount_price;
                    discount_price ?  this.products.totalSum += this.currentProduct.discount_price * this.count :
                    this.products.totalSum += this.currentProduct.price * this.count;
                } else {
                    this.products.cart.push(this.currentProduct);
                    let idCart = this.products.cart.indexOf(this.currentProduct);
                    this.products.cart[idCart].quantity += this.count;
                    this.products.totalQuantity += this.count;
                    let discount_price = this.currentProduct.discount_price;
                    discount_price ?  this.products.totalSum += this.currentProduct.discount_price * this.count :
                    this.products.totalSum += this.currentProduct.price * this.count;
                }
                let products_c = this.products;
                console.log(this.products);
                localStorage.setItem('products', JSON.stringify(products_c));
                renderCart();
            });
        },
        beforePushProducts(){
            this.products = localStorage.getItem('products') ? JSON.parse(localStorage.getItem('products')) : this.products;
        },
        removeOne(event, id) {
            event.preventDefault();
            /*axios.get('/cart/remove/' + id).then(res => {
                this.cart = res.data.cart;
                renderCart();
            }) */
            axios.get(`/cart/add/products/${id}` ).then(res => { // ?count=this.countths
                this.currentProduct = JSON.parse(res.data.current_product);
                this.currentProduct.quantity = 0;
                if(this.products.cart.some(el => el.id === this.currentProduct.id)){
                    let currentElement = {};
                    this.products.cart.forEach(el => {
                        if(el.id === this.currentProduct.id){
                            currentElement = el;
                        }
                    });
                    let idCart = this.products.cart.indexOf(currentElement);
                    this.products.cart[idCart].quantity --;
                    this.products.totalQuantity --;
                    let discount_price = this.currentProduct.discount_price;
                    discount_price ?  this.products.totalSum -= this.currentProduct.discount_price :
                    this.products.totalSum -= this.currentProduct.price;

                    let products_c = this.products;
                    localStorage.setItem('products', JSON.stringify(products_c));
                    renderCart();
                }
            });
        },
        addOne(event, id) {
            event.preventDefault();
            axios.get('/cart/add/' + id).then(res => {
                this.cart = res.data.cart;
                this.totalQuantity = res.data.totalQuantity;
                this.totalSum = res.data.totalSum;
                renderCart();
            })
        },
    },
  })
</script>
@include('layouts.footer')
