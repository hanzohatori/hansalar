@include('layouts.header')
<section class="personal-data account_top">
    <div class="container">
        <div class="personal-data__content">
            <div class="left-side">
                <h1>@lang('main.change')</h1>
                <div class="sidebar">
                    <ul class="sidebar__menu">
                        <li class="sidebar__item"><img src="/assets/img/user.svg" alt=""><a
                                href="{{ route('personal-data') }}"
                                class="sidebar__link active"> @lang('main.page_personal_info')</a></li>
                        <li class="sidebar__item"><img src="/assets/img/img2.svg" alt=""><a
                                href="{{ route('order-history') }}" class="sidebar__link"> @lang('main.order_history')</a>
                        </li>
                        <li class="sidebar__item"><img src="/assets/img/credit-card.svg" alt=""><a
                                href="{{ route('transactions') }}" class="sidebar__link"> @lang('main.trans')</a></li>
                        <li class="sidebar__item"><img src="/assets/img/lock.svg" alt=""><a
                                href="{{ route('change-password') }}" class="sidebar__link"> @lang('main.change')</a></li>
                        <button class="sidebar__btn">@lang('main.ask')</button>
                        {{--                        <a href="{{ route('logout') }}"--}}
                        {{--                           onclick="event.preventDefault();--}}
                        {{--                                                     document.getElementById('logout-form').submit();">--}}
                        {{--                                                        Logout--}}
                        {{--                        </a>--}}

                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                            <button class="sidebar__btn" style="margin-top: 10%"> @lang('main.exit')</button>
                            @csrf
                        </form>
                    </ul>
                </div>
            </div>

            <div class="right-side">
                <div class="title">
<!--                    <h2>@lang('main.change')</h2>-->
                    <a style="margin-left: auto" href="{{ route('clients_shop') }}" target="_blank">
                        <button class="primary-btn for-clients-btn">@lang('main.price_for_clients')</button>
                    </a>
                </div>

                <div class="right-side__content">
                    <form action="{{ route('reset') }}" class="form password-form" method="post">
                        @csrf
                        <div class="form-item">

                            <label for="name">@lang('main.old_change')</label>
                            <input class="password-input" type="password" name="old_password">
                            <img class="password-form-icon" src="/assets/img/password-form-icon.svg" alt="">
                        </div>

                        <div class="form-item">

                            <label for="">@lang('main.new_change')</label>
                            <input class="password-input" type="password" name="password">
                            <img class="password-form-icon" src="/assets/img/password-form-icon.svg" alt="">
                        </div>

                        <div class="form-item">

                            <label for="">@lang('main.submit_change')</label>
                            <input class="password-input" type="password" name="password_confirmation">
                            <img class="password-form-icon" src="/assets/img/password-form-icon.svg" alt="">
                        </div>
                        <button class="primary-btn change-password-btn">@lang('main.change_change')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade cabinet_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog partners_modal__wrap modal-sm">
        <div class="modal-content">
            <div class="partners_modal__block">
                <div class="partners_modal__block__title">
                    <h3>@lang('main.ask')</h3>
                    <img class="cabinet_exit" src="/assets/img/x.png" alt="">
                </div>
                <div class="partners_modal__block__form">
                    <form method="POST" action="{{ route('sendQuestion') }}">
                        <input type="hidden" name="user_id"
                               value="{{\Illuminate\Support\Facades\Auth::user() ? \Illuminate\Support\Facades\Auth::user()->id:null}}">
                        @csrf
                        <textarea name="question"></textarea>
                        <button type="submit">@lang('main.send')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@include('layouts.footer')
