<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('/dist/css/main.css') }}">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>
    <title>Hansa</title>
</head>

<header>
    <div class="header_top container_wrapper">
        <div class="header_main__lang">

        </div>
        <div class="header_top__find">

        </div>
    </div>
    <div class="header_main container_wrapper">
        <a href="" class="header_main__logo">
            <img src="/assets/img/logo.svg" alt="">
        </a>

        <div class="header_top__geo">

        </div>
        <!--  <div class="header_main__phone">
           <div class="header_main__phone_img">
             <img src="./assets/img/phone.svg" alt="">
           </div>
         </div> -->

        <div class="header_main__cart">


        </div>
    </div>
    <div class="burger-menu">
        <a href="#" class="burger-menu-btn">
            <span class="burger-menu-lines"></span>
        </a>
    </div>
</header>
<div class="nav-panel-mobil">
    <div class="container">
        <nav class="navbar-expand-lg navbar-light">
            <ul class="navbar-nav navbar-mobile">
                <li class="nav-item">
                    <a class="nav-link" href="/">Главная</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">Акция/Аутлет</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">Клиентам</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">Партнерам</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-link-lang nav-link-active" href="javascript:void(0);"
                       class="header_top__find_lang__active">ru</a>
                    <a class="nav-link nav-link-lang" href="javascript:void(0);">kz</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">Корзина</a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<section>
    <div class="home_slider__descr">
        <h1 style="text-align: center;margin: 20%"> САЙТ НАХОДИТСЯ В РАЗРАБОТКЕ </h1>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row footer">
            <div class="col-lg-2 col-md-4 footer__wrap">
                <div class="footer__logo">
                    <img src="/assets/img/logo_footer.svg" alt="">
                    <span>Hansa © 2020</span>
                </div>
            </div>
            <div class="col-lg-7 col-md-8 footer__wrap">
                <div class="footer__descr">
                    <ul>
{{--                        @foreach($categories as $category)--}}

{{--                            <li><a href="{{ route('shopCategory',$category->id) }}">{{ $category->title }}</a></li>--}}
{{--                            @if($category->id == 4)--}}
{{--                                @break--}}
{{--                            @endif--}}
{{--                        @endforeach--}}
                        {{--            <li><a href="javascript:void(0);">Отдельностоящие плиты</a></li>--}}
                        {{--            <li><a href="javascript:void(0);">Посудомоечные машины</a></li>--}}
                        {{--            <li><a href="javascript:void(0);">Варочные поверхности</a></li>--}}
                    </ul>
                    <ul>
{{--                        @foreach($categories as $category)--}}
{{--                            @if($category->id > 4)--}}
{{--                                <li><a href="{{ route('shopCategory',$category->id) }}">{{ $category->title }}</a></li>--}}
{{--                            @endif--}}
{{--                        @endforeach--}}
                        {{--            <li><a href="javascript:void(0);">Вытяжки</a></li>--}}
                        {{--            <li><a href="javascript:void(0);">Микроволновые печи</a></li>--}}
                        {{--            <li><a href="javascript:void(0);">Стиральные машины</a></li>--}}
                        {{--            <li><a href="javascript:void(0);">Холодильники</a></li>--}}
                    </ul>
                    <ul>

                    </ul>
                </div>
            </div>

        </div>
    </div>
</footer>
<script src="{{asset('/dist/js/main.js')}}"></script>
</html>
