<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('/dist/css/main.css') }}">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.3.1/css/ion.rangeSlider.min.css"/>
    <title>Hansa</title>
</head>

<header>
    <div class="header_top container_wrapper">
        <div class="header_main__lang">
            <a href="javascript:void(0);">Каз</a>
            <a href="javascript:void(0);" class="header_main__lang_active">Рус</a>
        </div>
        <div class="header_top__find">
            <img src="/assets/img/find.svg" alt="">
            {{--            <form action="">--}}
            {{--                <input type="search" name="product_name">--}}
            {{--            </form>--}}
        </div>
    </div>
    <div class="header_main container_wrapper">
        <a href="" class="header_main__logo">
            <img src="/assets/img/logo.svg" alt="">
        </a>
        <div class="header_main__menu">
            <a href="" class="header_main__menu_link">
                <img src="/assets/img/menu.svg" alt="">
                <span>Каталог</span>
            </a>
            <div class="catalog__links">
                <div class="catalog__links_item">
                    {{--                    <ul>--}}
                    {{--                        @foreach($categories as $category)--}}

                    {{--                            <li><span></span><a href="{{ route('shopCategory',$category->id) }}">{{ $category->title }}</a></li>--}}
                    {{--                            @if($category->id == 4)--}}
                    {{--                                @break--}}
                    {{--                            @endif--}}
                    {{--                        @endforeach--}}
                    {{--                    </ul>--}}
                    {{--                    <ul>--}}
                    {{--                        @foreach($categories as $category)--}}
                    {{--                            @if($category->id > 4)--}}
                    {{--                                <li><span></span><a href="{{ route('shopCategory',$category->id) }}">{{ $category->title }}</a></li>--}}
                    {{--                            @endif--}}
                    {{--                        @endforeach--}}

                    {{--                    </ul>--}}
                </div>
            </div>
        </div>
        <div class="header_top__geo">
            <img src="/assets/img/geo.svg" alt="">
            <span>Ваш город:</span>
            <h3>Алматы</h3>
        </div>
        <!--  <div class="header_main__phone">
           <div class="header_main__phone_img">
             <img src="./assets/img/phone.svg" alt="">
           </div>
         </div> -->
        <a href="" class="header_main__discount">
            <img src="/assets/img/discount.svg" alt="">
            <span>Акция/Аутлет</span>
        </a>
        <a href="" class="header_main__links">
            <span>Клиентам</span>
        </a>
        <a href="" class="header_main__links">
            <img src="/assets/img/user.svg" alt="">
            <span>Партнерам</span>
        </a>
        <div class="header_main__cart">
            <a href="" class="header_main__cart_flex">
                <div class="header_main__cart_img">
                    <img src="/assets/img/cart.svg" alt="">
                    {{--                    @if(isset($cartNew))--}}
                    {{--                        <h5>--}}
                    {{--                            {{ count($cartNew) }}--}}
                    {{--                        </h5>--}}
                    {{--                    @endif--}}
                </div>
                <div class="header_main__cart_descr">
                    <h3>Корзина</h3>
                    {{--                    @if(isset($cartNew))--}}
                    {{--                        --}}{{--                                                <span>{{ $cartNew->price }} ₸</span>--}}
                    {{--                    @endif--}}
                </div>
            </a>
            {{--            @if(isset($cartNew))--}}
            {{--                --}}{{--                                        @dd($cartNew)--}}
            {{--                <div class="header_main__cart_block">--}}
            {{--                    <div class="header_main__cart_block__title">--}}
            {{--                        <h3 class="header_main__cart_block__title_h3">Ваша корзина</h3>--}}
            {{--                        <!-- <a href="javascript:void(0);" class="header_main__cart_block__title_img">--}}
            {{--                          <img src="./assets/img/cart_exit.svg" alt="">--}}
            {{--                        </a> -->--}}
            {{--                    </div>--}}
            {{--                    @foreach($cartNew as $one)--}}
            {{--                        <div class="header_main__cart_block__item">--}}
            {{--                            <div class="header_main__cart_block__item__img">--}}
            {{--                                @foreach(json_decode($one->image) as $image)--}}
            {{--                                    <img src="{{ asset('storage/'.$image) }}" alt="">--}}
            {{--                                    @break--}}
            {{--                                @endforeach--}}

            {{--                            </div>--}}
            {{--                            <div class="header_main__cart_block__item__descr">--}}

            {{--                                <h3>{{ $one->product_name }} {{$one->color}}</h3>--}}
            {{--                                <div class="header_main__cart_block__item__descr_content">--}}
            {{--                                    <span class="count">{{ $one->quantity }} шт</span>--}}
            {{--                                    @if(isset($one->discount_price))--}}
            {{--                                        <span class="price">{{ $one->discount_price }} ₸</span>--}}
            {{--                                    @else--}}
            {{--                                        <span class="price">{{ $one->price }} ₸</span>--}}
            {{--                                    @endif--}}
            {{--                                </div>--}}

            {{--                            </div>--}}

            {{--                        </div>--}}
            {{--                    @endforeach--}}
            {{--                    --}}{{--                    @foreach($cartNew as $one)--}}
            {{--                    <div class="header_main__cart_block__bottom">--}}
            {{--                        <div class="header_main__cart_block__bottom_result">--}}

            {{--                            <div class="header_main__cart_block__bottom_result__product">--}}
            {{--                                --}}{{--                                    <h3>Всего товаров:&nbsp;</h3><span>{{ $cartNew->quantity }}</span>--}}
            {{--                            </div>--}}
            {{--                            <div class="header_main__cart_block__bottom_result__price">--}}
            {{--                                <h3>На сумму:&nbsp;</h3><span>{{ $totalSum }} ₸</span>--}}
            {{--                            </div>--}}

            {{--                        </div>--}}
            {{--                        <a href="{{ route('order') }}" class="header_main__cart_block__bottom_btn">--}}
            {{--                            Оформить заказ--}}
            {{--                        </a>--}}
            {{--                    </div>--}}
            {{--                    --}}{{--                    @endforeach--}}
            {{--                </div>--}}
            {{--            @endif--}}
        </div>
    </div>
    <div class="burger-menu">
        <a href="#" class="burger-menu-btn">
            <span class="burger-menu-lines"></span>
        </a>
    </div>
</header>
<div class="nav-panel-mobil">
    <div class="container">
        <nav class="navbar-expand-lg navbar-light">
            <ul class="navbar-nav navbar-mobile">
                <li class="nav-item">
                    <a class="nav-link" href="/">Главная</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">Акция/Аутлет</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">Клиентам</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">Партнерам</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link nav-link-lang nav-link-active" href="javascript:void(0);"
                       class="header_top__find_lang__active">ru</a>
                    <a class="nav-link nav-link-lang" href="javascript:void(0);">kz</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="">Корзина</a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<section>

    <div class="container" style="margin-top: 126px; margin-bottom: 126px">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Reset Password') }}</div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Send Password Reset Link') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer>
    <div class="container">
        <div class="row footer">
            <div class="col-lg-2 col-md-12 footer__wrap">
                <div class="footer__logo">
                    <img src="/assets/img/logo_footer.svg" alt="">
                    <span>Hansa © 2020</span>
                </div>
            </div>
            <div class="col-lg-7 col-md-8 footer__wrap">
                <div class="footer__descr">
                    <ul>
                        {{--                        @foreach($categories as $category)--}}

                        {{--                            <li><a href="{{ route('shopCategory',$category->id) }}">{{ $category->title }}</a></li>--}}
                        {{--                            @if($category->id == 4)--}}
                        {{--                                @break--}}
                        {{--                            @endif--}}
                        {{--                        @endforeach--}}
                        {{--            <li><a href="javascript:void(0);">Отдельностоящие плиты</a></li>--}}
                        {{--            <li><a href="javascript:void(0);">Посудомоечные машины</a></li>--}}
                        {{--            <li><a href="javascript:void(0);">Варочные поверхности</a></li>--}}
                    </ul>
                    <ul>
                        {{--                        @foreach($categories as $category)--}}
                        {{--                            @if($category->id > 4)--}}
                        {{--                                <li><a href="{{ route('shopCategory',$category->id) }}">{{ $category->title }}</a></li>--}}
                        {{--                            @endif--}}
                        {{--                        @endforeach--}}
                        {{--            <li><a href="javascript:void(0);">Вытяжки</a></li>--}}
                        {{--            <li><a href="javascript:void(0);">Микроволновые печи</a></li>--}}
                        {{--            <li><a href="javascript:void(0);">Стиральные машины</a></li>--}}
                        {{--            <li><a href="javascript:void(0);">Холодильники</a></li>--}}
                    </ul>
                    <ul>
                        <li><a href="javascript:void(0);">Клиентам</a></li>
                        <li><a href="javascript:void(0);">Партнерам</a></li>
                        <li><a href="javascript:void(0);">Акция/Аутлет</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 footer__wrap">
                <div class="footer__soc">
                    <ul>
                        <li>
                            <a href="javascript:void(0);">
                                <img src="/assets/img/phone_white.svg" alt="">
                                {{--                                @foreach($contacts as $contact)--}}
                                {{--                                    <span>{{$contact->phone}}</span>--}}
                                {{--                                @endforeach--}}
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <img src="/assets/img/message_white.svg" alt="">
                                {{--                                @foreach($contacts as $contact)--}}
                                {{--                                    <span>{{$contact->mail}}</span>--}}
                                {{--                                @endforeach--}}
                            </a>
                        </li>
                        <li>
                            <a href="javascript:void(0);">
                                <img src="/assets/img/geo_white.svg" alt="">
                                {{--                                @foreach($contacts as $contact)--}}
                                {{--                                    <span>{{$contact->address}}</span>--}}
                                {{--                                @endforeach--}}
                            </a>
                        </li>

                        <li class="footer__soc_list">
                            {{--                            @foreach($contacts as $contact)--}}
                            {{--                                <a href="{{$contact->instagram}}">--}}
                            {{--                                    <img src="/assets/img/instagram.svg" alt="">--}}
                            {{--                                </a>--}}

                            {{--                                <a href="{{$contact->instagram}}">--}}
                            {{--                                    <img src="/assets/img/vk.svg" alt="">--}}
                            {{--                                </a>--}}

                            {{--                                <a href="{{$contact->instagram}}">--}}
                            {{--                                    <img src="/assets/img/facebook.svg" alt="">--}}
                            {{--                                </a>--}}
                            {{--                            @endforeach--}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>
<script src="{{asset('/dist/js/main.js')}}"></script>
</html>


