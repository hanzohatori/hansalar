@include('layouts.header')
<section class="partners catalog">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xl-8 " style="padding: 0;">
                <div class="partners__title">
                    <div class="catalog__title">
                        <h3 class="catalog__title_h3">@lang('main.partners')</h3>
                    </div>
                    <svg class="partners__arrow hide-mob" width="63" height="14" viewBox="0 0 63 14" fill="none"
                         xmlns="http://www.w3.org/2000/svg">
                        <path d="M56 13L62 7L56 1" stroke="#C8C9CC" stroke-width="2" stroke-linecap="round"
                              stroke-linejoin="round"/>
                        <line x1="-8.74228e-08" y1="7" x2="61" y2="6.99999" stroke="#C8C9CC" stroke-width="2"/>
                    </svg>
                    @if(!\Illuminate\Support\Facades\Auth::check())
                        <a class="partners__title_btn" href="javascript:void(0)">@lang('main.login')</a>
                    @else
                        <a class="partners__title_btn_no" href="{{ route('personal-data') }}">@lang('main.login')</a>
                    @endif
                </div>
                <div class="partners__descr">
                    <div class="partners__descr__title">
                        <h3>@lang('main.adven')</h3>
                    </div>
                    <div class="row">
                        @foreach( $advantages as $one )
                            <div class="col-md-4 partners__descr__item">
                                <h3>{{ $one->title }}</h3>
                                <p>{!! $one->content !!} </p>
                                <div class="partners__descr__item__line"></div>
                            </div>
                        @endforeach
                        {{--                        <div class="col-md-4 partners__descr__item">--}}
                        {{--                            <h3>В условиях электромагнитных помех</h3>--}}
                        {{--                            <p>Очевидно, что газ вторично радиоактивен. Атом</p>--}}
                        {{--                            <div class="partners__descr__item__line"></div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-4 partners__descr__item">--}}
                        {{--                            <h3>В условиях электромагнитных помех</h3>--}}
                        {{--                            <p>Очевидно, что газ вторично радиоактивен. Атом</p>--}}
                        {{--                            <div class="partners__descr__item__line"></div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-4 partners__descr__item">--}}
                        {{--                            <h3>В условиях электромагнитных помех</h3>--}}
                        {{--                            <p>Очевидно, что газ вторично радиоактивен. Атом</p>--}}
                        {{--                            <div class="partners__descr__item__line"></div>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-4 partners__descr__item">--}}
                        {{--                            <h3>В условиях электромагнитных помех</h3>--}}
                        {{--                            <p>Очевидно, что газ вторично радиоактивен. Атом</p>--}}
                        {{--                            <div class="partners__descr__item__line"></div>--}}
                        {{--                        </div>--}}
                    </div>
                </div>
                <div class="partners__descr__text">
                    <h3>@lang('main.description')</h3>
                    <p>
                        {!! $description->description !!}
                    </p>
                    {{--                    <p>В литературе неоднократно описано, как тело когерентно заряжает плазменный гамма-квант. Мишень--}}
                    {{--                        выталкивает солитон. Жидкость расщепляет гамма-квант. Галактика представляет собой--}}
                    {{--                        внутримолекулярный осциллятор. Неустойчивость, как известно, быстро разивается, если тело--}}
                    {{--                        ненаблюдаемо отклоняет изотопный атом.</p>--}}
                    {{--                    <p>Фотон перманентно отталкивает короткоживущий погранслой. Квантовое состояние пространственно--}}
                    {{--                        притягивает межядерный электрон даже в случае сильных локальных возмущений среды. Гетерогенная--}}
                    {{--                        структура зеркально выталкивает тангенциальный экситон.</p>--}}
                </div>
                <div class="partners__descr__text">
                    <h3>@lang('main.steps')</h3>
                    <div class="row">
                        @foreach($steps as $one)
                            <div class="col-md-4">
                                <span>{{ $one->number }}</span>
                                <p>{{ $one->content }}</p>
                            </div>
                        @endforeach
                        {{--                        <div class="col-md-4">--}}
                        {{--                            <span>02</span>--}}
                        {{--                            <p>Очевидно, что газ вторично радиоактивен. Атом</p>--}}
                        {{--                        </div>--}}
                        {{--                        <div class="col-md-4">--}}
                        {{--                            <span>03</span>--}}
                        {{--                            <p>Очевидно, что газ вторично радиоактивен. Атом</p>--}}
                        {{--                        </div>--}}
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="partners__descr__text_bottom">
                                <p>{!! $cursives->content !!}</p>
                                {{--                                <p>Гидродинамический удар ускоряет осциллятор. Туманность расщепляет атом. Вещество--}}
                                {{--                                    тормозит резонатор. Гравитирующая сфера волнообразна.</p>--}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xl-4 partners__form_wrap">
                <div class="catalog__title">
                    <h3 class="catalog__title_h3">@lang('main.become')</h3>
                </div>
                <div class="partners__form">
                    <form action="{{ route('request') }}" method="post">
                        @csrf
                        <input type="text" placeholder="@lang('main.full_name')" name="name">
                        <input type="text" placeholder="@lang('main.comp_name')" name="legal_entity">
                        <input type="text" placeholder="Сегмент" name="segment">
                        <input id="partners_phone" type="text" placeholder="@lang('main.phone')" name="phone">
                        <input type="text" placeholder="Эл. почта" name="email">
                        <button class="partners__for--btn" type="submit">@lang('main.send_request')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade partners_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog partners_modal__wrap modal-sm">
        <div class="modal-content">
            <div class="partners_modal__block">
                <div class="partners_modal__block__title">
                    <h3>Вход</h3>
                    <img class="partners_exit" src="/assets/img/x.png" alt="">
                </div>
                <div class="partners_modal__block__form">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <input name="email" type="text"
                               class="account__personal-date-input @error('email') is-invalid @enderror"
                               value="{{ old('email') }}" placeholder="Логин" required autocomplete="email" autofocus/>
                        <input name="password" type="password" placeholder="Пароль"
                               class="account__personal-date-input"/>
                        <button type="submit">@lang('main.enter')</button>
                    </form>
                    {{--                    <a class="partners__form" href="{{ route('password.request') }}" style="font-size: small; color: grey; margin-left: 30%">--}}
                    {{--                        Забыли пароль?--}}
                    {{--                    </a>--}}
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')
