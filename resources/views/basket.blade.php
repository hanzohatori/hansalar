@include('layouts.header')
<section class="basket">
    <div class="container">
        <div class="row hiddenBlock" id="vue-cart">
            <div class="col-xl-8">
                <div class="hansa-title">
                    <h1>@lang('main.my_orders_card')</h1>
                </div>
                <div class="basket-wrap-left">
                    <div class="row">
                        <div class="col-xl-5 col-md-5">
                            <div class="hansa-subtitle">
                                <p>@lang('main.prods')</p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-2">
                            <div class="hansa-subtitle">
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-2">
                            <div class="hansa-subtitle">
                                <p>@lang('main.quant')</p>
                            </div>
                        </div>
                        <div class="col-xl-2 col-md-2">
                            <div class="hansa-subtitle">
                                <p>@lang('main.price')</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="basket-content basket-prod">
                    <div class="cart">
                        <div class="cart-product row" v-for="product in cart">
                            <div class="col-xl-2 col-4 col-md-2">
                                <div class="product-image">
                                    <a v-bind:href="`/product/about/${product.id}`">
                                        <img v-bind:src="`/storage/${JSON.parse(product.image)[0]}`" alt="">
                                    </a>
                                </div>
                            </div>
                            <div class="col-xl-3 col-6 col-md-3">
                                <a v-bind:href="`/product/about/${product.id}`">
                                    <div class="product-name product-text product-text-1">
                                        <h2>@{{ product.product_name }}</h2>
                                    </div>
                                </a>
                            </div>
                            <div class="col-xl-2 col-3 col-md-2">
                                <div class="product-name product-text">
                                </div>
                            </div>

                            <div class="col-xl-2 col-3 col-md-2">
                                <div class="product-name product-text product-count">
                                    <button :disabled="disableBtn" class="btn-minus" style="text-decoration: none"
                                            href="#" v-if="product.quantity > 1"
                                            v-on:click="removeOne($event, product.id)">
                                        <img class="minus" src="./assets/img/minus.svg" alt="">
                                    </button>

                                    <button :disabled="disableBtn" class="btn-minus" style="text-decoration: none"
                                            href="#" v-else v-on:click="$event.preventDefault();">
                                        <img class="minus" src="./assets/img/minus.svg" alt="">
                                    </button>

                                    <h3>@{{ product.quantity }}</h3>
                                    <button class="btn-plus" style="text-decoration: none" href="#"
                                            v-on:click="addOne($event, product.id)">
                                        <img class="plus" src="./assets/img/plus.svg" alt="">
                                    </button>
                                </div>
                            </div>
                            <div class="col-xl-2 col-4 col-md-2">
                                <div class="product-name product-text product-price product-with-discount">
                                    @if (\Illuminate\Support\Facades\Auth::check())
                                    <div class="price-now">
                                        <h3 class="discount__price--item">
                                            @{{ parseInt(product.price * product.quantity).toLocaleString('ru') }} ₸
                                        </h3>
                                        <h3 class="now_price">
                                            @{{ parseInt(product.partners_price * product.quantity).toLocaleString('ru')
                                            }} ₸
                                        </h3>
                                    </div>
                                    @else
                                    <h3 v-if="product.discount_price" class="discount_price">
                                        @{{ parseInt(product.discount_price * product.quantity).toLocaleString('ru') }}
                                        ₸</h3>
                                    <h3 class="discount_price" v-else>
                                        @{{parseInt((product.price * product.quantity)).toLocaleString('ru') }} ₸
                                    </h3>
                                    @endif
                                </div>
                            </div>
                            <div class="col-xl-1 col-2 col-md-1">
                                <div class="product-delete">
                                    <a href="#" v-on:click="deleteProduct($event, product.id)">
                                        <img src="/assets/img/x.svg" alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-xl-4">
                <div v class="basket-right">
                    <div class="hansa-title">
                        <h1>@lang('main.overall_promo')</h1>
                    </div>
                    <div class="basket-right-wrapper">
                        @if (\Illuminate\Support\Facades\Auth::check())
                        <form action="{{ route('cart') }}" type="get">
                            <input type="text" value="{{ isset($promocode) ? $promocode->promocode : '' }}"
                                   placeholder="Промокод" name="promocode">
                            <button type="submit" class="checkout-btn chechOuts" style="margin-top: 3%;width: 100%;padding: 0;border-radius: 100px !important;">
                                <a style="border-radius: 22px !important;">@lang('main.use_promo')</a>
                            </button>
                        </form>
                        @endif

                        <style>
                            .chechOuts:hover{

                            }
                        </style>
                        <div class="basket-price-inner price-num">
                            <div class="price-left">
                                <p>@{{ totalQuantity }} @lang('main.prods_promo')</p>
                            </div>
                            <div class="price-right">
                                <span>@{{ parseInt(totalSum) > 0 ? parseInt(totalSum).toLocaleString('ru') : 0 }} ₸</span>
                            </div>
                        </div>
                        @if (isset($promocode))
                        <div class="basket-price-inner price-num">
                            <div class="price-left">
                                <p> @lang('main.with_sale') -@{{ promocode.percent }}%</p>
                            </div>
                            <div class="price-right">
                                        <span>@{{ (parseInt(totalSum - (promocode.product.partners_price * promocode.percent / 100))) > 0 ? (parseInt(totalSum - (promocode.product.partners_price * promocode.percent / 100))).toLocaleString('ru') : 0 }}
                                            ₸</span>
                            </div>
                        </div>
                        @endif
                        <div class="basket-price-inner">
                            <div class="price-left">
                                <p>@lang('main.payment_promo')</p>
                            </div>
                            <div class="price-right total-price">
                                @if (isset($promocode))
                                <span>@{{  parseInt((totalSum - (promocode.product.partners_price * $promocode.percent / 100))) > 0 ? parseInt((totalSum - (promocode.product.partners_price * $promocode.percent / 100))).toLocaleString('ru') : 0 }}
                                            ₸</span>
                                @else
                                <span>@{{  parseInt(totalSum) > 0? parseInt(totalSum).toLocaleString('ru') : 0 }} ₸</span>
                                @endif
                            </div>
                        </div>
                        <div v-if="totalQuantity > 0" class="checkout-btn">
                            <a href="{{ route('order') }}" class="order__submit">@lang('main.submit_order')</a>
                            {{--
                            <form action="{{ route('order') }}" method="post" ref="submitProductForm">
                                @csrf
                                <input type="hidden" name="product" :value="submitProduct">
                                <input class="order__submit" type="submit" v-on:click="submitForm"
                                       value="{{ trans('main.submit_order') }}">
                            </form>
                            --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<style>
    .hiddenBlock {
        display: none;
    }

    .hiddenBlock.active {
        display: flex;
    }
</style>
<script>
    setTimeout(function () {
        document.querySelector(".hiddenBlock").classList = 'row hiddenBlock active';
    }, 300)

    const vue = new Vue({
        el: '#vue-cart',
        data: {
            cart: null,
            totalQuantity: null,
            totalSum: null,
            disableBtn: false,
            submitProduct: null
        },
        beforeMount() {
            /*axios.get('cart/get').then(res => {
                this.cart = res.data.cart;
                this.totalQuantity = res.data.totalQuantity;
                this.totalSum = res.data.totalSum;
                renderCart();
            }) */

            this.products = JSON.parse(localStorage.getItem('products'));
            if (this.products) {
                this.cart = this.products.cart;
                this.totalSum = this.products.totalSum;
                this.totalQuantity = this.products.totalQuantity;
                renderCart();
            }


        },
        methods: {
            submitForm() {
                this.submitProduct = localStorage.getItem('products');
                this.$refs.submitProductForm.submit();

            },
            removeOne(event, id) {
                event.preventDefault();
                this.disableBtn = true;
                /*axios.get('cart/remove/' + id).then(res => {
                    this.cart = res.data.cart;
                    this.totalQuantity = res.data.totalQuantity;
                    this.totalSum = res.data.totalSum;
                    renderCart();
                    this.disableBtn = false;
                }); */

                axios.get(`/cart/add/products/${id}`).then(res => { // ?count=this.countths


                    this.currentProduct = JSON.parse(res.data.current_product);
                    this.currentProduct.quantity = 0;
                    if (this.products.cart.some(el => el.id === this.currentProduct.id)) {
                        let currentElement = {};
                        this.products.cart.forEach(el => {
                            if (el.id === this.currentProduct.id) {
                                currentElement = el;
                            }
                        });
                        let idCart = this.products.cart.indexOf(currentElement);
                        this.products.cart[idCart].quantity--;
                        this.products.totalQuantity--;
                        let discount_price = this.currentProduct.discount_price;
                        discount_price ? this.products.totalSum -= this.currentProduct.discount_price :
                            this.products.totalSum -= this.currentProduct.price;

                        let products_c = this.products;
                        localStorage.setItem('products', JSON.stringify(products_c));
                        if (this.products) {
                            this.cart = this.products.cart;
                            this.totalSum = this.products.totalSum;
                            this.totalQuantity = this.products.totalQuantity;
                        }
                        renderCart();
                        this.disableBtn = false;
                    }
                });
            },
            addOne(event, id) {
                event.preventDefault();
                /*axios.get('cart/add/' + id).then(res => {
                    this.cart = res.data.cart;
                    this.totalQuantity = res.data.totalQuantity;
                    this.totalSum = res.data.totalSum;
                    renderCart();
                }); */

                axios.get(`/cart/add/products/${id}`).then(res => { // ?count=this.countths
                    this.currentProduct = JSON.parse(res.data.current_product);
                    this.currentProduct.quantity = 0;
                    if (this.products.cart.some(el => el.id === this.currentProduct.id)) {
                        let currentElement = {};
                        this.products.cart.forEach(el => {
                            if (el.id === this.currentProduct.id) {
                                currentElement = el;
                            }
                        });
                        let idCart = this.products.cart.indexOf(currentElement);
                        this.products.cart[idCart].quantity++;
                        this.products.totalQuantity++;
                        let discount_price = this.currentProduct.discount_price;
                        discount_price ? this.products.totalSum += Number(this.currentProduct
                                .discount_price) :
                            this.products.totalSum += Number(this.currentProduct.price);

                        let products_c = this.products;
                        localStorage.setItem('products', JSON.stringify(products_c));
                        if (this.products) {
                            this.cart = this.products.cart;
                            this.totalSum = this.products.totalSum;
                            this.totalQuantity = this.products.totalQuantity;
                        }
                        renderCart();
                    }
                });

            },
            deleteProduct(event, id) {
                event.preventDefault();
                /*axios.get('cart/delete/' + id).then(res => {
                    this.cart = res.data.cart;
                    this.totalQuantity = res.data.totalQuantity;
                    this.totalSum = res.data.totalSum;
                    renderCart();
                }); */
                axios.get(`/cart/add/products/${id}`).then(res => { // ?count=this.countths
                    this.currentProduct = JSON.parse(res.data.current_product);
                    this.currentProduct.quantity = 0;
                    if (this.products.cart.some(el => el.id === this.currentProduct.id)) {
                        let currentElement = {};
                        this.products.cart.forEach(el => {
                            if (el.id === this.currentProduct.id) {
                                currentElement = el;
                            }
                        });
                        let idCart = this.products.cart.indexOf(currentElement);
                        let discount_price = this.currentProduct.discount_price;
                        discount_price ? this.products.totalSum -= this.currentProduct.discount_price *
                            this.products.cart[idCart].quantity :
                            this.products.totalSum -= this.currentProduct.price * this.products.cart[
                                idCart].quantity;
                        this.products.totalQuantity -= this.products.cart[idCart].quantity;

                        this.products.cart.splice(idCart, 1);
                        let products_c = this.products;
                        localStorage.setItem('products', JSON.stringify(products_c));
                        if (this.products) {
                            this.cart = this.products.cart;
                            this.totalSum = this.products.totalSum;
                            this.totalQuantity = this.products.totalQuantity;
                        }
                        renderCart();
                    }
                });

            }
        },
    })
</script>
@include('layouts.footer')
