@include('layouts.header')
<section class="personal-data account_top">
    <div class="container">
        <div class="personal-data__content">
            <div class="left-side">
                <h1>@lang('main.order_history')</h1>

                <div class="sidebar">
                    <ul class="sidebar__menu">
                        <li class="sidebar__item"><img src="/assets/img/user.svg" alt=""><a
                                href="{{ route('personal-data') }}"
                                class="sidebar__link active"> @lang('main.page_personal_info')</a></li>
                        <li class="sidebar__item"><img src="/assets/img/img2.svg" alt=""><a
                                href="{{ route('order-history') }}" class="sidebar__link"> @lang('main.order_history')</a>
                        </li>
                        <li class="sidebar__item"><img src="/assets/img/credit-card.svg" alt=""><a
                                href="{{ route('transactions') }}" class="sidebar__link"> @lang('main.trans')</a></li>
                        <li class="sidebar__item"><img src="/assets/img/lock.svg" alt=""><a
                                href="{{ route('change-password') }}" class="sidebar__link"> @lang('main.change')</a></li>
                        <button class="sidebar__btn">@lang('main.ask')</button>
                        {{--                        <a href="{{ route('logout') }}"--}}
                        {{--                           onclick="event.preventDefault();--}}
                        {{--                                                     document.getElementById('logout-form').submit();">--}}
                        {{--                                                        Logout--}}
                        {{--                        </a>--}}

                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                            <button class="sidebar__btn" style="margin-top: 10%"> @lang('main.exit')</button>
                            @csrf
                        </form>
                    </ul>
                </div>
            </div>

            <div class="right-side">
                <div class="title">
<!--                    <h2>@lang('main.order_history')</h2>-->
                    <a style="margin-left: auto" href="{{ route('clients_shop') }}" target="_blank">
                        <button class="primary-btn">@lang('main.price_for_clients')</button>
                    </a>
                </div>

                <div class="right-side__content order-history-content">
                    @if(isset($order))
                        @foreach($order as $tito)
                            <div class="box">
                                <div class="box-item">
                                    <p class="box-item-title">№</p>
                                    <p>{{ $tito->id }}</p>
                                </div>

                                <div class="box-item">
                                    <p class="box-item-title">@lang('main.time_orderh')</p>
                                    <p>{{ $tito->created_at }}</p>
                                </div>

                                <div class="box-item">
                                    <p class="box-item-title">@lang('main.list_orderh')</p>
                                    <div class="sructure">
                                        @foreach($tito->order_details as $one)
                                            <a href="{{ route('product_page',$one['id']) }}">
                                                <p> {{ $one->product_name }}</p>
                                            </a>
                                            {{--                                    <p>2. Холодильная камера Hansa FM1337.3RAA Красный</p>--}}
                                        @endforeach
                                        <a href="">
                                            {{--                                        Показать все--}}
                                        </a>
                                    </div>
                                </div>

                                <div class="box-item">
                                    <p class="box-item-title">@lang('main.price_orderh')</p>
                                    <p class="price">{{ $tito->total_price }} ₸</p>
                                </div>

                                <div class="box-item last-item">
                                    <p class="box-item-title">Статус</p>
                                    <p class="status">{{ $tito->delivery_status }}</p>
                                </div>
                            </div>
                        @endforeach
                    @endif

                    {{--                <div class="box">--}}
                    {{--                    <div class="box-item">--}}
                    {{--                    <p class="box-item-title hidden">№</p>--}}
                    {{--                    <p>540</p>--}}
                    {{--                    </div>--}}

                    {{--                    <div class="box-item">--}}
                    {{--                    <p class="box-item-title hidden">Дата заказа</p>--}}
                    {{--                    <p>06.01.2021</p>--}}
                    {{--                    </div>--}}

                    {{--                    <div class="box-item">--}}
                    {{--                    <p class="box-item-title hidden">Состав заказа</p>--}}
                    {{--                    <div class="sructure">--}}
                    {{--                        <p>1. Холодильная камера Hansa FM1337.3RAA Красный</p>--}}
                    {{--                        <p>2. Холодильная камера Hansa FM1337.3RAA Красный</p>--}}
                    {{--                        <a href="">Показать все</a>--}}

                    {{--                    </div>--}}
                    {{--                    </div>--}}

                    {{--                    <div class="box-item">--}}
                    {{--                    <p class="box-item-title hidden">Стоимость</p>--}}
                    {{--                    <p class="price">25 990 ₸</p>--}}
                    {{--                    </div>--}}

                    {{--                    <div class="box-item last-item">--}}
                    {{--                    <p class="box-item-title hidden">Статус</p>--}}
                    {{--                    <p class="status">Обрабатывается</p>--}}
                    {{--                    </div>--}}
                    {{--                </div>--}}

                    {{--                <div class="box last-box">--}}
                    {{--                    <div class="box-item">--}}
                    {{--                    <p class="box-item-title hidden">№</p>--}}
                    {{--                    <p>540</p>--}}
                    {{--                    </div>--}}

                    {{--                    <div class="box-item">--}}
                    {{--                    <p class="box-item-title hidden">Дата заказа</p>--}}
                    {{--                    <p>06.01.2021</p>--}}
                    {{--                    </div>--}}

                    {{--                    <div class="box-item">--}}
                    {{--                    <p class="box-item-title hidden">Состав заказа</p>--}}
                    {{--                    <div class="structure">--}}

                    {{--                        <p>1. Холодильная камера Hansa FM1337.3RAA Красный</p>--}}
                    {{--                        <p>2. Холодильная камера Hansa FM1337.3RAA Красный</p>--}}
                    {{--                        <p>3. Холодильная камера Hansa FM1337.3RAA Красный</p>--}}
                    {{--                    </div>--}}
                    {{--                    </div>--}}

                    {{--                    <div class="box-item">--}}
                    {{--                    <p class="box-item-title hidden">Стоимость</p>--}}
                    {{--                    <p class="price">25 990 ₸</p>--}}
                    {{--                    </div>--}}

                    {{--                    <div class="box-item last-item">--}}
                    {{--                    <p class="box-item-title hidden">Статус</p>--}}
                    {{--                    <p class="status status-completed">Заказ выполнен</p>--}}
                    {{--                    </div>--}}
                    {{--                </div>--}}

                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade cabinet_modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true">
    <div class="modal-dialog partners_modal__wrap modal-sm">
        <div class="modal-content">
            <div class="partners_modal__block">
                <div class="partners_modal__block__title">
                    <h3>@lang('main.ask')</h3>
                    <img class="cabinet_exit" src="/assets/img/x.png" alt="">
                </div>
                <div class="partners_modal__block__form">
                    <form method="POST" action="{{ route('sendQuestion') }}">
                        <input type="hidden" name="user_id"
                               value="{{\Illuminate\Support\Facades\Auth::user() ? \Illuminate\Support\Facades\Auth::user()->id:null}}">
                        @csrf
                        <textarea name="question"></textarea>
                        <button type="submit">@lang('main.send')</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.footer')
