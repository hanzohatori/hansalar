const path = require('path');

module.exports = {
    mode: "development",
    entry: ['./public/src/js/main.js'],
    output: {
        filename: 'main.js',
        path: path.join(__dirname, './dist/js')
    },
    module: {
        rules: [
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
        ]
    },
};
