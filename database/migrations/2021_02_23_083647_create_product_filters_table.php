<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Domains\Contracts\ProductFilterContract;

class CreateProductFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ProductFilterContract::TABLE, function (Blueprint $table) {
            $table->id();
            $table->bigInteger(ProductFilterContract::PRODUCT_ID);
            $table->bigInteger(ProductFilterContract::FILTER_ID);
            $table->string(ProductFilterContract::TITLE)->nullable();
            $table->boolean(ProductFilterContract::IS_SHOW)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ProductFilterContract::TABLE);
    }
}
