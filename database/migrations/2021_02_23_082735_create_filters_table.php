<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Domains\Contracts\FilterContract;

class CreateFiltersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(FilterContract::TABLE, function (Blueprint $table) {
            $table->id();
            $table->bigInteger(FilterContract::CATEGORY_ID);
            $table->string(FilterContract::SLUG)->nullable();
            $table->string(FilterContract::TITLE)->nullable();
            $table->boolean(FilterContract::IS_SHOW)->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(FilterContract::TABLE);
    }
}
