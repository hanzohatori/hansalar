window.$ = window.jQuery = require("jquery");

require('popper.js');
require('bootstrap');
require('slick-slider');
require('ion-rangeslider');
require('jquery-mask-plugin');
require('@splidejs/splide');
const {default: Splide} = require("@splidejs/splide");
const Swal = require('sweetalert2')
const Vue = require('vue/dist/vue.common.dev')
// function getLocation() {
//     if (navigator.geolocation) {
//       navigator.geolocation.watchPosition(showPosition);
//     } else {
//       x.innerHTML = "Geolocation is not supported by this browser.";
//     }
// }

// function showPosition(position) {
//     let cityname = document.querySelector('.cityname')
//     fetch('https://geolocation-db.com/json/09068b10-55fe-11eb-8939-299a0c3ab5e5', {
//         method: 'GET'
//     })
//     .then(response => response.json())
//     .then(res => cityname.innerHTML = res.city)
// }

// document.addEventListener("DOMContentLoaded", function(event) {
//     getLocation()
// });

let cityname = document.querySelector('.cityname')
let cityname_list__item = document.querySelectorAll('.cityname_list__item')
let bank_input = document.querySelector('.bank_input__val')
let bank_dropdown__item = document.querySelectorAll('.bank_dropdown__item')

// if (cityname_list__item) {
//     cityname_list__item.forEach(item => {
//         item.addEventListener('click', () => {
//             cityname.innerText = item.textContent
//         })
//     })
// }

if (bank_dropdown__item) {
    bank_dropdown__item.forEach(item => {
        item.addEventListener('click', () => {
            bank_input.value = item.textContent
        })
    })
}

// Jquery codes
$(".catergories_filter__range_input").ionRangeSlider({
    type: "double",
    skin: "round",
    min: $(".categories_filter__range").data("min"),
    max: $(".categories_filter__range").data("max"),
    hide_from_to: true,
    hide_min_max: true,
    onChange: function (data) {
        let from = document.querySelector('.priceto')
        let to = document.querySelector('.pricefrom')
        from.value = data.from
        to.value = data.to
    }
});

$('.home_slider_wrap').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    variableWidth: true,
    prevArrow: '<img class="left_arrow" src="./assets/img/left_arrow_img.svg" />',
    nextArrow: '<img class="right_arrow" src="./assets/img/left_arrow_img.svg" />',
    responsive: [
        {
            breakpoint: 1024,
            settings: {
                arrow: false,
            }
        },
    ]
});

let x = window.matchMedia("(max-width: 1024px)");
let y = document.querySelector('.filter__wrap_collapse')
if (y) {
    if (x.matches) {
        y.classList.add('collapse')
    } else {
        y.classList.remove('collapse')
    }
}

let partnersBtn = document.querySelector('.partners__title_btn');
let partnersExit = document.querySelector('.partners_exit')
if (partnersBtn) {
    partnersBtn.addEventListener('click', () => {
        $('.partners_modal').modal('show')
    })
}
if (partnersExit) {
    partnersExit.addEventListener('click', () => {
        $('.partners_modal').modal('hide')
    })
}
let sidebar__btn = document.querySelector('.sidebar__btn')
let cabinet_exit = document.querySelector('.cabinet_exit')
if (sidebar__btn) {
    sidebar__btn.addEventListener('click', () => {
        $('.cabinet_modal').modal('show')
    })
}
if (cabinet_exit) {
    cabinet_exit.addEventListener('click', () => {
        $('.cabinet_modal').modal('hide')
    })
}


// $('.product_inner__descr_img__item').slick({
//   slidesToShow: 1,
//   slidesToScroll: 1,
//   arrows: true,
//   prevArrow: '<img class="prevArrow_product_inner" src="/assets/img/product_inner_arrow.svg" alt="">',
//   nextArrow: '<img class="nextArrow_product_inner" src="/assets/img/product_inner_arrow.svg" alt="">',
//   fade: true,
//   asNavFor: '.product_inner__descr_img__item_nav'
// });
// $('.product_inner__descr_img__item_nav').slick({
//   slidesToShow: 3,
//   slidesToScroll: 1,
//   asNavFor: '.product_inner__descr_img__item',
//   dots: true,
//   focusOnSelect: true
// });


$(document).ready(function () {
    $("#phone").mask("999 999 99 99");
    //$("#phone1").mask("999 999 99 99");
    //$("#phone2").mask("999 999 99 99");
    //$(".checkout-num__phone").mask("999 999 99 99");
    $('.burger-menu-btn').click(function (e) {
        e.preventDefault();
        $(this).toggleClass('burger-menu-lines-active');
        $('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
        $('body').toggleClass('body-overflow');
    });
})


if (document.querySelectorAll('.catalog__btn--up').length != 0) {
    window.addEventListener('scroll', function () {
        if (pageYOffset >= 550) {
            document.getElementById('btnup').style.display = 'block';
        } else {
            document.getElementById('btnup').style.display = 'none';
        }
    });
}


//let phone1 = document.getElementById('phone1');
if (document.querySelectorAll('#phone1').length != 0) {
    IMask(
        document.getElementById('phone1'),
        {
            mask: '000 000 00 00'
        }
    );

    IMask(
        document.getElementById('phone2'),
        {
            mask: '000 000 00 00'
        }
    );

    IMask(
        document.getElementById('iic'),
        {
            mask: '{KZ}******************'
        }
    );

    IMask(
        document.getElementById('bin'),
        {
            mask: '000000000000'
        }
    );

    IMask(
        document.getElementById('floor'),
        {
            mask: '00'
        }
    );

    IMask(
        document.getElementById('apartment'),
        {
            mask: '0000'
        }
    );

    IMask(
        document.getElementById('house'),
        {
            mask: '********'
        }
    );
}
if (document.querySelectorAll('#partners_phone').length != 0) {
    IMask(
        document.getElementById('partners_phone'),
        {
            mask: '000 000 00 00'
        }
    );
}

if (document.querySelectorAll('#paymentcard-form').length != 0) {
    IMask(
        document.getElementById('number-card'),
        {
            mask: '0000 0000 0000 0000',
            min: 16,
            max: 16
        }
    );

    IMask(
        document.getElementById('date-card'),
        {
            mask: '00{/}00'
        }
    );

    IMask(
        document.getElementById('cvv-card'),
        {
            mask: '000',
            min: 3,
            max: 3
        }
    );
}


/*$(document).ready(function(){
    $(".checkout-num__phone").mask("999 999 99 99");
} */

const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true
})

$(window).on('load', function (e) {
    $('a.product_inner__descr__content_count__btn').on('click', async (e) => {
        e.preventDefault();
        await fetch(e.currentTarget.href);
        Toast.fire({
            icon: 'success',
            title: 'Товар добавлен в корзину'
        });
        //renderCart();
    });
    $('button.recommendation_product__descr__item_btn').on('click', async (e) => {
        e.preventDefault();
        await fetch(e.currentTarget.parentElement.href);
        Toast.fire({
            icon: 'success',
            title: 'Товар добавлен в корзину'
        });
        //renderCart();
    });
    $('[validate-btn]').on('click', (e) => {
        let errorsList = [];
        $('[validate-optional]').toArray().forEach(elem => {
            if (elem.value == null || elem.value.trim() === '') {
                elem.value = '-';
            }
        });
        $('[validate]').toArray().forEach(elem => {
            if (elem.value == null || elem.value.trim() === '') {
                elem.style.borderColor = 'red';
                $('[order-title]')[0].scrollIntoView();
                $('[order-error]')[0].style.display = 'block';
                errorsList.push(elem);
                e.preventDefault();
            } else {
                elem.style.borderColor = '';
                if (errorsList.includes(elem)) {
                    errorsList.splice(errorsList.indexOf(elem), 1);
                }
            }
        });
        if (!errorsList.length) {
            e.preventDefault();
            if (document.querySelector('#cardChecker').checked ||
                (document.querySelectorAll('.cashless_payments')[0].checked)) {
                window.location.href = '/paymentcard';
            } else {
                orderReady();
            }
        }
    });

    $('[validate-company-btn]').on('click', (e) => {
        let errorsList = [];
        $('[validate-company]').toArray().forEach(elem => {
            if (elem.value == null || elem.value.trim() === '') {
                elem.style.borderColor = 'red';
                $('[order-company-title]')[0].scrollIntoView();
                $('[order-company-error]')[0].style.display = 'block';
                errorsList.push(elem);
                e.preventDefault();
            } else {
                elem.style.borderColor = '';
                if (errorsList.includes(elem)) {
                    errorsList.splice(errorsList.indexOf(elem), 1);
                }
            }
        });

        if (!errorsList.length) {
            e.preventDefault();
            if (document.querySelectorAll('.cashless_payments')[1].checked) {
                window.location.href = '/paymentcard';
            } else {
                orderReady()
            }
        }
    });

    $('[validate-payment-btn]').on('click', (e) => {
        let errorsList = [];
        $('[validate-payment]').toArray().forEach(elem => {
            if (elem.value == null || elem.value.trim() === '') {
                elem.style.borderColor = 'red';
                $('[order-payment-title]')[0].scrollIntoView();
                $('[order-payment-error]')[0].style.display = 'block';
                errorsList.push(elem);
                e.preventDefault();
            } else {
                elem.style.borderColor = '';
                if (errorsList.includes(elem)) {
                    errorsList.splice(errorsList.indexOf(elem), 1);
                }
            }
        });

        if (!errorsList.length) {
            e.preventDefault();
            orderReady()
        }
    });
})


let language = document.getElementById('html').getAttribute('lang');

let modalVerifyContent = ['Ваш заказ оформлен! В ближайшее время с вами свяжется менеджер.', 'Спасибо!']

if (language == "kz") {
    modalVerifyContent = ['Сіздің тапсырысыңыз аяқталды! Жақында менеджер сізбен байланысады.', 'Рахмет!']
}

function orderReady() {
    const csrfToken = document.querySelector('meta[name="csrf-token"]').content;
    Swal.fire({
        icon: 'success',
        text: modalVerifyContent[0],
        timer: 5000,
        titleText: modalVerifyContent[1],
        buttonsStyling: false,
        customClass: {
            confirmButton: 'checkout_btn__defStyle',
        },
        onClose: () => {
            fetch('/clear', {method: 'POST', headers: {'X-CSRF-Token': csrfToken}}).then(res => {
                window.location.href = '/page';
                localStorage.clear();
            })
        }
    })
}


// JS codes
let plus = document.querySelector('.plus');
let minus = document.querySelector('.minus');
let counter = document.querySelector('.counter');
let count = 1;
if (counter) {
    plus.addEventListener('click', () => {
        counter.innerHTML = count++;
    })
    minus.addEventListener('click', () => {
        counter.innerHTML > 1 ? counter.innerHTML = --count : false;
    })
}

let links = $(".tab_link")
let prev_link = 0
let tabContent = $(".tab_content")
for (let x = 0; x < links.length; x++) {
    links[x].addEventListener("click", () => {
        links[prev_link].classList.remove("product_inner__descr__content_tabs__head_active")
        links[x].classList.add("product_inner__descr__content_tabs__head_active")
        tabContent[prev_link].style.display = "none"
        tabContent[x].style.display = "block"
        prev_link = x
    })
}

const passwordFormIcon = document.querySelectorAll('.password-form-icon');

if (passwordFormIcon) {
    passwordFormIcon.forEach(el => el.addEventListener('click', function () {
        const input = this.parentElement.querySelector('.password-input');
        const type = input.getAttribute('type') === 'password' ? 'text' : 'password';
        input.setAttribute('type', type);
    }));
}
let categoryElement = document.getElementById('category_id')
if (categoryElement) {
    var category_id = categoryElement.value
}
let price_desc = document.getElementById('price_desc')
let price_asc = document.getElementById('price_asc')
let is_new = document.getElementById('is_new')
let is_popular = document.getElementById('is_popular')
let search_input = document.querySelector('.search_input')
let filterBody = {}
let filter = new Object()
let choice_city = document.getElementById('choice_city');
let select_city = document.getElementById('select_city');

let url = `/api/catalog${category_id !== '' ? `?category=${category_id}` : ''}`;

let changeSelectCity = () => {
    let selid = select_city.options.selectedIndex;

    if (select_city && select_city.options[selid].text.toLowerCase().trim() === "алматы") {
        choice_city.src = "https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d46526.80471576545!2d76.85962167806059!3d43.21105620942479!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1z0LzQuNC60YDQvtGA0LDQudC-0L0g0JDQu9C80LDQs9GD0LvRjCwg0JDQu9C80LDRgtGLINCe0YDQsdC40YLQsA!5e0!3m2!1sru!2skz!4v1624704130444!5m2!1sru!2skz";
    } else if (select_city && (select_city.options[selid].text.toLowerCase().trim() === "астана" ||
        select_city.options[selid].text.toLowerCase().trim() === "нурсултан")) {
        choice_city.src = "https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10007.569942701159!2d71.4340302!3d51.1657715!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4245812c18014321%3A0x385cf77f10db2182!2z0L_RgNC-0YHQv9C10LrRgiDQkNCx0LDRjywg0J3Rg9GALdCh0YPQu9GC0LDQvSAwMjAwMDA!5e0!3m2!1sru!2skz!4v1624704208498!5m2!1sru!2skz";
    }
}

if (select_city) {
    changeSelectCity();
    select_city.addEventListener('change', changeSelectCity);
}


if (price_asc) {
    price_asc.addEventListener('click', () => {
        price_desc.checked = false
        price_asc.checked = true
        is_new.checked = false
        is_popular.checked = false

    })
}
if (price_desc) {
    price_desc.addEventListener('click', () => {
        price_desc.checked = true
        price_asc.checked = false
        is_new.checked = false
        is_popular.checked = false
    })
}
if (is_new) {
    is_new.addEventListener('click', () => {
        price_desc.checked = false
        price_asc.checked = false
        is_new.checked = true
        is_popular.checked = false
    })
}
if (is_popular) {
    is_popular.addEventListener('click', () => {
        price_desc.checked = false
        price_asc.checked = false
        is_new.checked = false
        is_popular.checked = true
    })
}

search_input.addEventListener('keypress', (e) => {
    if (e.key == 'Enter') {
        let url = window.location.pathname
        if (url == '/catalogue') {
            search(search_input.value)
            e.preventDefault()
        } else {
            filterBody.product_name = search_input.value
            let query = new URLSearchParams(filterBody).toString()
            window.location.replace(`/catalogue?${query}`)
        }

    }
})
document.addEventListener('DOMContentLoaded', async function () {

    function getParameterByName(name, url = window.location.href) {
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    if (window.location.pathname == '/catalogue') {
        let response = await fetch(url, {
            method: "POST",
            body: JSON.stringify(filterBody)
        })
        let body = await response.json()
        let params = getParameterByName('product_name');
        if (params) {
            search(params)
        } else {

            renderHtmlProducts(body.products.data)
        }
    }
})

function renderHtmlProducts(products) {
    let userData = document.getElementById('user_data')
    let mainPanel = document.getElementById('main_product_panel')
    if (mainPanel) {
        mainPanel.innerHTML = ''
        products.forEach(product => {
            mainPanel.innerHTML += `
                    <div class="recommendation_product__descr__item" style="flex: 0 0 30%;">
                                <a href="/product/about/${product.id}">
                    ${userData.value === '' && product.discount_price !== null ? "<div class='recommendation_product__descr__item_icon'> <img src='/assets/img/discount_icon.svg' alt=''></div>" : ''}


                    ${userData.value === '' && product.loan === "1" ? '<div class="recommendation_product__descr__item_icon_2"><img src="/assets/img/ras.svg" alt=""></div>' : ''}

                    <div class="recommendation_product__descr__item_img" style="max-width: 201px;">
                    <img src="/storage/${product.image[0]}" alt="" style="height: 181px">
                    </div>
                    <div class="recommendation_product__descr__item_text">

                    <h3>${product.product_name} ${product.colour.color}</h3>
                    <div class="recommendation_product__descr__item_text__price">

                    ${userData.value === '' && product.discount_price !== null ? `<h3 class="now_price">${product.discount_price} ₸</h3><h3 class="old_price">${product.price} ₸</h3>` : ''}
                    ${userData.value === '' && product.discount_price === null ? ` <h3 class="now_price">${product.price} ₸</h3>` : ''}
                    ${userData.value !== '' ? `<h3 class="now_price">${product.partners_price}</h3>` : ''}


                    </div>
                    <div class="recommendation_product__descr__item_text__status">
                        ${product.stock > 0 ? `<h3>В наличии</h3>` : `<h3>Нет в наличии</h3>`}
                    </div>
                </div>
            </a>

            <div>
                <a class="recommendation_product__descr__item_btn"
                   href="#">
                                        <img class="cart_noactive" src="/assets/img/cart_red.svg" alt="">
                                        <img class="cart_active" src="/assets/img/cart_white.svg" alt="">
                                        <span>В корзину</span>
                                    </a>
                                </div>
                            </div>

`
        })

    }

}

async function search(val) {

    filterBody.search = val;
    let query = new URLSearchParams(filterBody).toString()
    if (url.includes('?') && filterBody.hasOwnProperty('search')) {
        url += `&${query}`
    } else {
        url += `?${query}`
    }
    let response = await fetch(url, {
        method: "POST",
        body: JSON.stringify(filterBody)
    })
    let body = await response.json();
    renderHtmlProducts(body.products.data)
}

let categoriest_catalogue = document.querySelectorAll('.categoriest_catalogue')
let filter_wrap = document.querySelector('.filter_wrap')
if (window.location.pathname == '/catalogue' || window.location.pathname == '/outlets') {
    filter_wrap.style.background = '#f2f2f2'
    filter_wrap.addEventListener('click', () => {
        Swal.fire({
            icon: 'error',
            text: 'Выберите категорию',
            toast: true,
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
            position: 'top-end',
        })
        window.scrollTo(0, 0)
    })
    categoriest_catalogue.forEach(item => {
        let a = item.querySelectorAll('.catalogue_input')
        a.forEach(i => {
            i.disabled = true
        })
        if (item.getAttribute('data-count') > 2) {
            item.style.display = 'none'
        }
    })
}

document.addEventListener('DOMContentLoaded', (event) => {
    const openCategoriesModal = document.querySelector('#open-categories');
    const modal = document.querySelector('.modal-categories');
    const modalBackground = document.querySelector('.modal-background');
    openCategoriesModal.addEventListener('click', (openModal) => {
        openModal.preventDefault();
        modal.classList.remove('hidden')
    })
    modalBackground.addEventListener('click', (modalClick) => {
        if (modalClick.target.className === 'modal-background') {
            modal.classList.add('hidden')
        }
    })
})

let secondarySplide = new Splide('#splide-nav', {
    focus: 'center',
    pagination: false,
    rewind: true,
    isNavigation: true,
    breakpoints: {
        480: {
            fixedWidth: '12rem',
        },
    }
}).mount();

let primarySplide = new Splide('#splide-primary', {
    type: 'loop',
    pagination: false,
    arrows: false,
}).sync(secondarySplide).mount();

