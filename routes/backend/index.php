<?php

use Illuminate\Support\Facades\Route;

/* ------------------------------------------------- BACKEND ROUTES  ------------------------------------------------- */

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

/* ------------------------------------------------- BACKEND ROUTES  ------------------------------------------------- */
