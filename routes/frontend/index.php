<?php

use Illuminate\Support\Facades\Route;
use Spatie\Sitemap\SitemapGenerator;

/* ------------------------------------------------- FRONTEND ROUTES  ------------------------------------------------- */

Route::get('/', 'ProductController@blocker')->name('blocker');

Route::middleware(['set_locale'])->group(function () {
    Route::group(['middleware' => 'shared.components'], function () {

        // ...
        Route::group(['middleware' => 'auth'], function () {
            Route::get('/partners/change-password', 'ChangePasswordController@index')->name('change-password');
            Route::get('/partners/order-history', 'OrderHistoryController@index')->name('order-history');
            Route::get('/partners/personal-data', 'PersonalDataController@index')->name('personal-data');
            Route::post('/question', 'PersonalDataController@send')->name('sendQuestion');
            Route::post('/update', 'PersonalDataController@updateUser')->name('update');
            Route::post('/reset', 'ChangePasswordController@reset')->name('reset');
            Route::get('/partners/transactions', 'TransactionsController@index')->name('transactions');
        });

        // page
        Route::get('/page', 'ProductController@index')->name('index');

        // ...
        Route::get('/home', 'HomeController@index')->name('home');

        // products
        Route::get('/product/about/{id}', 'ProductController@about')->name('product_page'); // done
        Route::post('/search', 'ProductController@search')->name('search'); // done
        Route::get('/product/info/{id}', 'ProductController@info')->name('product_info');

        // cart
        Route::get('/cart', 'CartController@viewCart')->name('cart');
        Route::get('/cart/add/{product}', 'CartController@addCart')->name('add_cart');
        Route::get('/cart/remove/{product}', 'CartController@removeCart')->name('remove_cart');
        Route::get('/cart/delete/{product}', 'CartController@deleteCart')->name('delete_cart');
        Route::get('/cart/get', 'CartController@getCart')->name('get_cart');
        Route::get('/cart/add/products/{product}', 'CartController@addManyCart')->name('add_items');

        // orders
        Route::get('/order', 'OrderController@orderPage')->name('order');
        Route::post('/submit', 'OrderController@orderSubmit')->name('submit');
        Route::post('/clear', 'OrderController@cartClear')->name('clear');
        // Route::get('order', 'OrderController@orderPage')->name('order');

        // partners
        Route::get('/partners', 'PartnerController@partner')->name('partner');
        Route::post('/partners/request', 'PartnerController@register')->name('request');

        // client
        Route::get('/clients', 'ClientController@client')->name('client');
        Route::get('/clients/products', function () {
            return redirect()->to("/catalogue");
        })->name('clients_shop');
//        Route::get('/clients/products', 'ClientController@clientsShop')->name('clients_shop');
        Route::get('/clients/products/categories', 'ClientController@filter')->name('clientFilter');
        Route::get('/clients/products/categories/{id}', 'ClientController@clientCategory')->name('clientCategory');


        // others
        Route::get('/paymentcard', 'PaymentcardController@showPage')->name('paymentcard');
        Route::get('/catalogue', 'CatalogController@catalogue')->name('catalogue');
        Route::get('/catalogue/categories/{id}', 'CatalogController@shopCategory')->name('shopCategory');
        Route::get('/outlets', 'OutletController@index')->name('outlet');
        Route::get('/outlets/categories/{id}', 'OutletController@outletCategory')->name('outletCategory');
        Route::get('/contacts', 'AdminController@contacts')->name('contacts');
        Route::get('/city/{city}', 'AdminController@city')->name('city');
    });

//    Route::get('/categories', 'ProductController@filter')->name('filter');

    //Sitemap generator
    Route::get('sitemap', function () {
        SitemapGenerator::create('https://hansa.kz')->writeToFile('sitemap.xml');
        return ('Карта сайта создана');
    });
});

Route::group(['prefix' => 'api'], function () {
    Route::post('catalog', 'CatalogController@mainApiCatalogue');
    Route::post('outlets', 'OutletController@mainApiCatalogue');
    Route::post('clients/products', 'CatalogController@mainApiCatalogue');
});

/* ------------------------------------------------- FRONTEND ROUTES  ------------------------------------------------- */
