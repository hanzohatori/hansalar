<?php

use Illuminate\Support\Facades\Route;

Route::get('/locale/{locale}', 'AdminController@locale')->name('locale');

include_route_files(__DIR__ . '/frontend/');
include_route_files(__DIR__ . '/backend/');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
